//
//  Message.swift
//  InBeauty
//
//  Created by Brian Nuñez Rios on 7/27/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

struct Chat : Decodable
{
    let id : Int
    let mensaje: String?
    let tipo_mensaje:String
    let created_at:String
    let de_user_id:String
    let para_user_id:String
    let chatcon :SenderChat
    var leido : Int
}

struct SenderChat : Decodable
{
    let id : String
    let name:String
    let email:String
    let foto_url:String?
    let soyseguidor:Int
    let mesigue:Int
}

class Message : NSObject,Decodable
{
    var tipo_mensaje : String = ""
    var created_at:String = ""
    var mensaje : String?
    var de_user_id:String = ""
    var para_user_id:String = ""
    var identificador:String?
    
    override init() {
        super.init()
    }
}

enum AddRecommend
{
    case inbox
    case service
}
