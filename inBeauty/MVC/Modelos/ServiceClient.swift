//
//  ServicioCliente.swift
//  InBeauty
//
//  Created by ADMIN on 14/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class ServiceClient: NSObject,Codable
{
    var satisfaccion : Int = 0
    var categoria_id : Int = 0
    var author_tag : String = ""
    var foto_principal : String = ""
    var style: String = ""
    var status : Bool = false
    var mensaje : String = "Type a message here..."
    var cliente_id : String = ""
    var formula : Formula?
    var recomendaciones : Recomendacion?
    var reminder : [Reminder] = []
    var componentes : [Component] = []
    var fotos : [Picture] = []
    
    override init() {
        super.init()
    }
    
    func imprimirData()
    {
//        print(satisfaccion)
//        print(status)
//        print(author_tag)
//        print(style)
//        print(categoria_id)
//        print(mensaje)
//        print(cliente_id)
//        print(formula.name)
//        print(formula?.descripcion_proceso)
//        print(formula?.productos)
//        print(recomendaciones?.dodont.count)
//        print(recomendaciones?.use.count)
//        print(reminder.count)
    }
}

class Formula : Codable
{
    var name : String = "Formula"
    var descripcion_proceso : String = "No descripcion"
    var productos : [ProductFormula] = []
}

struct ProductFormula : Codable {
    let id : String
    let producto_id : Int
    let numeroProducto: String?
    let nombreMarca:String?
    let cantidad: Int
    let medida : String
    let producto : Producto
}

class Recomendacion : Codable
{
    let id : String = ""
    var dodont : [DoDont] = []
    var use : [Use] = []
}

struct Reminder : Codable
{
    var mensaje : String
    var numero : Int?
    var tiempo : String
    let numero_restante : Int
    let tiempo_restante : String
}

struct StatusDoDont : Codable
{
    let status : String
    let data : [DoDont]
}

struct DoDont : Codable
{
    let id : String?
    let tipo : String
    let tarea : String
}

struct Use : Codable
{
    let id : String
    let producto_id : Int
    let cantidad : Int
    var medida : String
    let frecuencia : String?
    let producto : Producto
}

struct Component : Codable
{
    let perfil : String
    var url_componente : String
}

struct Picture : Codable
{
    let perfil : String
    var url_fotos : String
}
