//
//  OnBoard.swift
//  InBeauty
//
//  Created by ADMIN on 28/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

@objc class OnService: NSObject,Codable
{
    let id : String
    var satisfaccion : Int
    var mensaje : String?
    let fecha_recordatorio : String
    var foto_principal : String
    let updated_at:String
    let author_tag: String
    let style: String?
    let cliente_id:String
    let cliente : Client
    let profesional : Profesional
    var componentes : [Component]
    var fotos : [Picture]
    var status : Int
    let categoria_id:Int
    var recomendaciones : Recomendacion?
    var formula : Formula?
    var reminder : [Reminder]
    var categoria : Category
}

struct RecomendationClient
{
    let type:String
    let recomendation:Any
}

//    init(id:String,satisfaccion:Int,mensaje:String,fecha_recordatorio:String,author_tag:String,cliente_id:String,client:Client,componentes:[Component],fotos:[Picture],status:Int,categoria_id:Int,recomendaciones:Recomendacion?,formula:Formula?,reminder:[Reminder],categoria:Category) {
//
//        self.id = id
//        self.satisfaccion = satisfaccion
//        self.mensaje = mensaje
//        self.fecha_recordatorio = fecha_recordatorio
//        self.author_tag = author_tag
//        self.cliente_id = cliente_id
//        self.cliente = client
//        self.componentes = componentes
//        self.fotos = fotos
//        self.status = status
//        self.categoria_id = categoria_id
//        self.recomendaciones = recomendaciones
//        self.formula = formula
//        self.reminder = reminder
//        self.categoria = categoria
//    }
