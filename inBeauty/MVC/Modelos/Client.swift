//
//  Client.swift
//  InBeauty
//
//  Created by ADMIN on 28/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

struct StatusDiscovery : Decodable
{
    let status : String
    let data : [Discovery]
}

struct Discovery: Decodable
{
    let id: String
    let satisfaccion:Int
    let mensaje : String
    let fecha_recordatorio:String
    let profesional_id : String
    let author_tag:String
    let style: String?
    let cliente: Client
    let status : Int
    let profesional: Profesional
    let categoria : Category
    let fotos: [Picture]
}

struct StatusProfesional : Codable {
    let status : String
    let data : [Profesional]
}

struct Profesional:Codable
{
    let id : String
    let name : String
    let email: String
    let foto_url: String
    let foto:String?
    let location: String?
    let miscategorias : [Category]?
    let soyseguidor : Int
    let mesigue: Int
    let seguidores: Int
    let seguidos: Int
    let servicios: Int
}

struct Client: Codable
{
    let id : String
    let tipo : String?
    let name : String
    let email : String
    let foto_url : String
    let foto:String?
    let soyseguidor:Int
    let mesigue:Int
    var imgData : Data?
    let seguidores:Int
    let seguidos:Int
    let servicios:Int
}
