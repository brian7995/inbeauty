//
//  Marca.swift
//  InBeauty
//
//  Created by ADMIN on 13/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

struct Marca: Codable
{
    let id : Int
    let nombre : String
    let url_foto: String?
}

struct Categoria : Codable
{
    let id : Int
    let marca_id : Int
    let nombre : String
    let marca : Marca?
}

struct Producto : Codable
{
    let id : Int
    let categoria_id : Int
    let nombre : String
    var medida : String
    let numero : String
    let descripcion : String?
    let url_foto : String
    var cantidad : Int?
    var categoria : Categoria?
    var nombreCategoria : String? 
    
    func imgProducto() -> UIImage
    {
        return UIImage()
    }
}

struct NumeroProducto : Decodable
{
    let id : Int
    let numero : String
}

