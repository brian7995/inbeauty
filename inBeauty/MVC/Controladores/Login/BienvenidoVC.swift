//
//  BienvenidoVC.swift
//  InBeauty
//
//  Created by ADMIN on 22/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class BienvenidoVC: UIViewController {

    @IBOutlet var stackUserName: UIStackView!
    @IBOutlet var stackTitle: UIStackView!
    @IBOutlet var imgPerfil: UIImageView!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var areYouLabel: UILabel!
    @IBOutlet var styleProfessionalLabel: UILabel!
    @IBOutlet var yesImPro: UILabel!
    @IBOutlet var btnCerrar: UIButton!
    @IBOutlet var switchPro: UISwitch!
    @IBOutlet weak var subrayadoStyleProfesional: UIView!
    
    var isOn = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgPerfil.layoutIfNeeded()
        self.imgPerfil.clipsToBounds = true
        self.imgPerfil.layer.cornerRadius = self.imgPerfil.bounds.midX
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return (self.isOn) ? .lightContent : .default
    }

    @IBAction func mostrarTitulos(_ sender: UIButton)
    {
        //self.instanciarTitulos()
        //UITabBarController.init().set
    }
    
    @IBAction func volverTitulos(_ segue: UIStoryboardSegue)
    {
        //self.instanciarTitulos()
        //UITabBarController.init().set
    }
    
  
    @IBAction func yesImPro(sender: UISwitch) {
        
        self.isOn = sender.isOn
        self.setNeedsStatusBarAppearanceUpdate()
        
        let bgColor = (sender.isOn) ? #colorLiteral(red: 0.2421956956, green: 0.2385163009, blue: 0.329823941, alpha: 1)  : UIColor.white
        let colorWelcome = (sender.isOn) ? UIColor.white : UIColor.darkGray
        let colorQuestions =  (sender.isOn) ? UIColor.black : UIColor.darkGray
        
        let title = (sender.isOn) ? "Jump to Business" : "I'm Just a Client"
        
        //preferredStatusBarStyle.rawValue = (sender.isOn) ? 1 : 0
        
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = bgColor
            
            (self.stackTitle.subviews.first as! UIButton).setTitleColor(colorWelcome, for: .normal)
            (self.stackTitle.subviews.last)!.backgroundColor = colorWelcome
            self.subrayadoStyleProfesional.backgroundColor = colorWelcome
            
            self.stackUserName.subviews.forEach({ (view) in
                let label = view as! UILabel
                label.textColor = colorWelcome
            })
            
            self.areYouLabel.textColor = colorQuestions
            self.styleProfessionalLabel.textColor = colorQuestions
            self.yesImPro.textColor = colorQuestions
            
            self.btnCerrar.tintColor = colorWelcome
            self.btnConfirm.setTitle(title, for: .normal)
        }
    }
    
    func instanciarTitulos()
    {
        let vistaLlamadas = UIView(frame: CGRect(x: self.view.frame.midX - (self.view.bounds.width * 0.75 / 2), y: self.view.frame.midY - (self.view.bounds.height * 0.25/2), width: self.view.bounds.width * 0.75, height: self.view.bounds.height * 0.3))
        //vistaLlamadas.accessibilityIdentifier = "\(presentacion.id)"
        vistaLlamadas.alpha = 0
        vistaLlamadas.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        vistaLlamadas.tag = 177
        vistaLlamadas.backgroundColor = UIColor.white
        vistaLlamadas.layer.cornerRadius = vistaLlamadas.bounds.width / 14
        vistaLlamadas.layer.shadowRadius = 15
        vistaLlamadas.layer.shadowOpacity = 0.75
        
        let dimensionesBtnsLlamadas = vistaLlamadas.bounds.width * 0.3
        
        //BTNs Titulos
        
        let btn1Llamada = UIButton(type: .custom)
        btn1Llamada.layer.borderWidth = 1
        btn1Llamada.layer.borderColor = #colorLiteral(red: 0.5990038032, green: 0.5665634729, blue: 1, alpha: 1)
        btn1Llamada.tag = 1
        btn1Llamada.titleLabel?.adjustsFontSizeToFitWidth = true
        let sizeBtn1Font : CGFloat = (UIScreen.main.bounds.height == 568) ? 18 : 20
        btn1Llamada.titleLabel!.font = UIFont(name: "Courier", size: sizeBtn1Font)!
        btn1Llamada.titleLabel!.textAlignment = .center
        btn1Llamada.setTitle("hairstyler", for: .normal)
        btn1Llamada.setTitleColor(UIColor.darkGray, for: .normal)
        //btn1Llamada.sizeThatFits(CGSize(width: 150, height: 30))
        btn1Llamada.sizeToFit()
        btn1Llamada.titleEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        btn1Llamada.layer.cornerRadius = btn1Llamada.bounds.midY
        btn1Llamada.frame.origin = CGPoint(x: vistaLlamadas.bounds.midX - btn1Llamada.bounds.midX, y: vistaLlamadas.bounds.midY - 15)
        
        vistaLlamadas.addSubview(btn1Llamada)
        
        //BTN Ultima Llamada
        
        let btnUltLlamada = UIButton(frame: CGRect(x: vistaLlamadas.bounds.midX*1.175, y: vistaLlamadas.bounds.midY - dimensionesBtnsLlamadas/2, width: dimensionesBtnsLlamadas, height: dimensionesBtnsLlamadas))
        btnUltLlamada.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        btnUltLlamada.tag = 2
        btnUltLlamada.setTitle("Última Llamada", for: .normal)
        btnUltLlamada.layer.cornerRadius = dimensionesBtnsLlamadas / 12
        btnUltLlamada.titleLabel?.textAlignment = .center
        btnUltLlamada.titleLabel?.numberOfLines = 0
        //let sizeBtnUFont : CGFloat = (UIScreen.main.bounds.height == 568) ? 16 : 18
        //btnUltLlamada.titleLabel?.font = UIFont(name: "AvenirLTStd-Black", size: sizeBtnUFont)!
        //vistaLlamadas.addSubview(btnUltLlamada)
        
        //Llamada al WS
        
        //btn1Llamada.addTarget(self, action: #selector(activarCampana(btn:)), for: .touchUpInside)
        //btnUltLlamada.addTarget(self, action: #selector(activarCampana(btn:)), for: .touchUpInside)
        
        //BTN Salir
        
        let dimensionesBtnSalir = vistaLlamadas.bounds.width * 0.15
        let posXBtn = (self.view.frame.midX + vistaLlamadas.bounds.width/2) - dimensionesBtnSalir/2 //vistaLlamadas.bounds.width - (dimensionesBtnSalir/2)
        let posYBtn = (self.view.frame.midY - (vistaLlamadas.bounds.height/2))/1.05 // -dimensionesBtnSalir/1.5
        
        let btnSalir = UIButton(frame: CGRect(x: posXBtn, y: posYBtn, width: dimensionesBtnSalir, height: dimensionesBtnSalir))
        
        //posX cuando btnSalir es hijo de vistallamada = /*vistaLlamadas.bounds.width - (dimensionesBtnSalir/2)*/
        //posY cuando btnSalir es hijo de vistallamada = /*-dimensionesBtnSalir/1.5*/
        //vistaLlamadas.addSubview(btnSalir)
        btnSalir.tag = 178
        btnSalir.layer.cornerRadius = dimensionesBtnSalir / 2
        btnSalir.backgroundColor = UIColor.white
        btnSalir.setImage(#imageLiteral(resourceName: "cerrar"), for: .normal)
        btnSalir.imageEdgeInsets.top = 12
        btnSalir.imageEdgeInsets.bottom = 12
        btnSalir.imageEdgeInsets.right = 12
        btnSalir.imageEdgeInsets.left = 12
        btnSalir.alpha = 0
        btnSalir.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        //        btnSalir.layer.shadowRadius = 10
        //        btnSalir.layer.shadowOpacity = 0.85
        
        //btnSalir.addTarget(self, action: #selector(salirLlamadaObra(btn:)), for: .touchUpInside)
        
        self.view.addSubview(vistaLlamadas)
        self.view.addSubview(btnSalir)
        
        UIView.animate(withDuration: 0.1) {
            vistaLlamadas.alpha = 1
            vistaLlamadas.transform = CGAffineTransform(scaleX: 1, y: 1)
            //
            btnSalir.alpha = 1
            btnSalir.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
