//
//  RegistroVC.swift
//  InBeauty
//
//  Created by ADMIN on 17/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Cloudinary
import Firebase

class RegistroVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,CameraAndPhotos,CLUploaderDelegate{

    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var confirmpassTxt: UITextField!
    @IBOutlet weak var activityRegister: UIActivityIndicatorView!
    @IBOutlet var underlinedTxts: [UIView]!
    @IBOutlet weak var codeCountry: UILabel!
    @IBOutlet weak var viewCountries: UIView!
    @IBOutlet weak var countriesTV: UITableView!
    @IBOutlet weak var switchPro: UISwitch!
    @IBOutlet weak var scrollRegistro: UIScrollView!
    
    
    var dictTxt : [Int:UITextField] = [:]
    var countries : [Country] = []
    var currentCountry = Country()
    var uploaderCloud : CLUploader?
    var msjSuccess : String = ""
    var idNewUser : String = ""
    var tokenFBase : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        uploaderCloud = CLUploader(AppDelegate.cloud, delegate: self)
        currentCountry = Country(bandera: "🇺🇸", nombre: "Estados Unidos", codigo: "+01")
        self.countries = self.currentCountry.setupCountries()
        
        self.countriesTV.dataSource = self
        self.countriesTV.delegate = self
        self.countriesTV.reloadData()
        
        InstanceID.instanceID().instanceID { (instance, error) in
            
            if let error = error
            {
                print("Error a la hora de obtener el token de firebase \(error)")
            }else
            {
                self.tokenFBase = instance?.token ?? "No hay token"
                print("Token de Firebase : \(self.tokenFBase)")
            }
        }
        
        //self.activarOcultamientoTeclado()
        self.setupViews()
        //self.setupCountries()
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
        viewCountries.layer.cornerRadius = 20
        viewCountries.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewCountries.layer.shadowOpacity = 0.25
        viewCountries.layer.shadowRadius = 10
        
        dictTxt = [0:usernameTxt,1:emailTxt,2:phoneTxt,3:passTxt,4:confirmpassTxt]
        
        dictTxt.values.forEach { (txt) in
            txt.attributedPlaceholder = NSAttributedString(string: txt.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        }
        
        imgPhoto.clipsToBounds = true
        imgPhoto.layer.cornerRadius = imgPhoto.bounds.midX
        btnPhoto.layer.cornerRadius = btnPhoto.bounds.midX
        
        btnCreate.layer.cornerRadius = btnCreate.bounds.midY
        codeCountry.adjustsFontSizeToFitWidth = true
        
        usernameTxt.delegate = self
        emailTxt.delegate = self
        passTxt.delegate = self
        phoneTxt.delegate = self
        phoneTxt.addDoneCancelToolbar()
        confirmpassTxt.delegate = self
        
        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(openViewCountries))
        self.codeCountry.addGestureRecognizer(tapCountry)
        
        print(ChooseVC.tipo ?? "no hay tipo")
        
        if let tipo = ChooseVC.tipo
        {
            switchPro.isOn = tipo == "profesional"
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func volverAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Carrete de Fotos
    @IBAction func abrirFotos(_ btn: UIButton)
    {
        let dontShowTutorial = UserDefaults.standard.value(forKey: "tutorial") as? Bool ?? false
        
        if dontShowTutorial
        {
            self.showPickerControllerTypes()
        }else
        {
            self.performSegue(withIdentifier: "goTutorialPhotos", sender: self)
        }
    }
    
    func showPickerControllerTypes()
    {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default) { (action) in
            picker.sourceType = .camera
            picker.cameraDevice = .front
            picker.cameraViewTransform = CGAffineTransform(scaleX: -1, y: 1)
            self.showDetailViewController(picker, sender: nil)
        }
        
        let photoLibraryAction = UIAlertAction(title: "Open Photo Library", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            self.showDetailViewController(picker, sender: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openPickerControllerTypes() {
        self.showPickerControllerTypes()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imgPhoto.image = info[.originalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openViewCountries()
    {
        self.viewCountries.isHidden = false
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.75, options: .curveEaseOut, animations: {
            self.viewCountries.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (bool) in
        }
    }
    
    @IBAction func closeViewCountries(_ btn: UIButton)
    {
         self.viewCountries.isHidden = true
         self.viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
    }

    @IBAction func showTextField(_ btn: UIButton)
    {
        if let txt = dictTxt[btn.tag]
        {
            txt.isSecureTextEntry = !txt.isSecureTextEntry
        }
    }
    
    //MARK: TABLA PAISES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath)
        
        let country = self.countries[indexPath.row]
        
        let bandera = cell.viewWithTag(100) as! UILabel
        let codigo = cell.viewWithTag(101) as! UILabel
        let nombre = cell.viewWithTag(102) as! UILabel
        
        bandera.text = country.bandera
        codigo.text = country.codigo
        nombre.text = country.nombre
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentCountry = self.countries[indexPath.row]
        self.codeCountry.text = self.currentCountry.codigo
        self.codeCountry.textColor = UIColor.darkGray
        self.viewCountries.isHidden = true
        self.viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    //MARK: TYPE USER
    @IBAction func typeUserAction(_ sender: UISwitch)
    {
        ChooseVC.tipo = (sender.isOn) ? "profesional" : "cliente"
    }
    
    //MARK: CLUploader Delegate
    func uploaderSuccess(_ result: [AnyHashable : Any]!, context: Any!) {
        let publicId = result["public_id"] as? String ?? "noid"
        let version = result["version"] as? Int ?? -12
        let urlPhoto = AppDelegate.cloud?.url("v" + String(version) + "/" + publicId) ?? "nada"
        print("Exito en Uploader",urlPhoto,"context :",context)
        
        inBiServices.instancia.savePhotoProfile(id: self.idNewUser, urlPhoto: urlPhoto) { (respuesta) in
            //let body = respuesta as? [String:Any] ?? [:]
            //let exito = body["status"] as? String ?? ""
            self.btnCreate.isUserInteractionEnabled = true
            self.activityRegister.stopAnimating()
            self.mostrarAlerta(titulo: "inBi", msj: self.msjSuccess, acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })], bloqueCompletacion: nil)
        }
    }
    
    func uploaderError(_ result: String!, code: Int, context: Any!) {
        print("Error uploaded",result,code)
        self.activityRegister.stopAnimating()
        self.mostrarAlerta(titulo: "Inbi", msj: self.msjSuccess + "Failed to upload photo. Try again.", acciones: [UIAlertAction(title: "Ok", style: .cancel, handler: nil)], bloqueCompletacion: nil)
    }
    
    func uploaderProgress(_ bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int, context: Any!) {
        //let pro : CLongDouble = CLongDouble(totalBytesWritten/totalBytesExpectedToWrite)
        //print("Uploader Progress",bytesWritten,totalBytesWritten,totalBytesExpectedToWrite)
    }
    
    //MARK: Crear cuenta
    @IBAction func createAccount(_ btn: UIButton)
    {
        self.registerUser()
    }
    
    func registerUser()
    {
        guard dictTxt.values.reduce(true, {return $0 && (!($1.text!.isEmpty))}),emailTxt.text!.contains("@"),phoneTxt.text!.count >= 9,codeCountry.text! != "+00",passTxt.text! == confirmpassTxt.text!,imgPhoto.image != nil else {
            dictTxt.values.forEach { (txt) in
                
                if let bg = underlinedTxts.first(where: {return $0.tag == txt.tag})
                {
                    bg.backgroundColor = txt.text!.isEmpty ? UIColor.red : #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1)
                }
                
                if txt == emailTxt
                {
                    underlinedTxts[1].backgroundColor = txt.text!.contains("@") ? #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1) : UIColor.red
                }
                
                if txt == phoneTxt || codeCountry.text! == "+00"
                {
                    underlinedTxts[2].backgroundColor = txt.text!.count < 9 || codeCountry.text! == "+00" ? UIColor.red : #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1)
                }
            }
            
            if passTxt.text! != confirmpassTxt.text!
            {
                underlinedTxts[3].backgroundColor = UIColor.red
                underlinedTxts[4].backgroundColor = UIColor.red
            }
            
            if imgPhoto.image == nil
            {
                imgPhoto.layer.borderWidth = 2
                imgPhoto.layer.borderColor = UIColor.red.cgColor
            }
    
            return
        }
        
        self.activityRegister.startAnimating()
        self.btnCreate.isUserInteractionEnabled = false
        print("Ya podemos registrar", self.usernameTxt.text!,self.emailTxt.text!,self.passTxt.text!,self.phoneTxt.text!)
        
        self.imgPhoto.layer.borderWidth = 0
        self.underlinedTxts.forEach { (txt) in
            txt.backgroundColor = #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1)
        }
        
        let name = usernameTxt.text!
        let email = emailTxt.text!
        let cellPhone = codeCountry.text! + "_" + phoneTxt.text!
        let pass = passTxt.text!
        let confirmPass = confirmpassTxt.text!
        
        self.comprobarInternet { (conectado, msj) in
            if conectado
            {
                self.registerWS(name: name, email: email, phone: cellPhone, pass: pass, confirmPass: confirmPass)
            }else
            {
                self.mostrarAlerta(titulo: "inBi", msj: msj, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            }
        }
    }
    
    func registerWS(name:String,email:String,phone:String,pass:String,confirmPass:String)
    {
        inBiServices.instancia.register(username: name, email: email, phone: phone, password: pass, confirm: confirmPass, pais: self.currentCountry.nombre, urlPhoto: "", tokenFbase: self.tokenFBase) { (respuesta) in
            
            let body = respuesta as? [String:Any] ?? [:]
            let data = body["data"] as? [String:Any] ?? [:]
            //print(data)
            
            if let exito = body["status"] as? String,exito == "success"
            {
                self.msjSuccess = data["mensaje"] as? String ?? ""
                self.idNewUser = data["user_id"] as? String ?? ""
                //print(self.idNewUser)
                if let jpegImage = self.imgPhoto.image?.jpegData(compressionQuality: 0.5)
                {
                    print("Tenemos el jpg comprimido de la imagen")
                    let tipo = ChooseVC.tipo ?? "cliente"
                    let subFolder = (tipo == "profesional") ? "Professional" : "Client"
                    let idPhoto = self.emailTxt.text! + "-" + tipo
                    self.uploaderCloud?.upload(jpegImage, options: ["folder":"ProfilePictures/\(subFolder)/","public_id":idPhoto])
                }
            }else
            {
                let error = data["error"] as? String ?? "An error occurred while registering. Try again."
                self.mostrarAlerta(titulo: "inBi", msj: error, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            }
            self.activityRegister.stopAnimating()
        }
    }
    
    func login(email:String,password:String)
    {
        inBiServices.instancia.login(email: email, password: password, tokenFbase: self.tokenFBase) { (respuesta) in
            print(respuesta)
            guard let status = respuesta as? StatusUser,status.status == "success" else {
                let body = respuesta as? [String:Any] ?? [:]
                let data = body["data"] as? [String:Any] ?? [:]
                let error = data["error"] as? String ?? "There was an error. Please exit the app and re-enter with your new username and password."
                self.mostrarAlerta(titulo: "inBi", msj: error, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                self.activityRegister.stopAnimating()
                return
            }
            
            AppDelegate.user = status.data
            self.activityRegister.stopAnimating()
            
            if status.data.tipo == "profesional"
            {
                self.performSegue(withIdentifier: "goTitles", sender: self)
            }else
            {
                self.performSegue(withIdentifier: "goMenu", sender: self)
            }
        }
    }
    
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goTitles"
        {
            let titleVC = segue.destination as! TitleVC
            titleVC.img = self.imgPhoto.image
        }
        
        if segue.identifier == "goTutorialPhotos"
        {
            let tutoVC = segue.destination as! TutorialPhotoVC
            tutoVC.delegate = self
        }
    }
}

extension RegistroVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == phoneTxt
//        {
//            if string == "" && textField.text?.count == 9
//            {
//                textField.text?.removeLast()
//            }
//
//            return textField.text!.count < 9
//        }else
//        {
//            return true
//        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == phoneTxt
        {
            let esSE = UIScreen.main.bounds.height == 568
            
            if esSE
            {
                UIView.animate(withDuration: 0.25) {
                    self.scrollRegistro.contentOffset.y += 30
                }
            }
            
            passTxt.becomeFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let esSE = UIScreen.main.bounds.height == 568
        
        if textField == usernameTxt
        {
            return emailTxt.becomeFirstResponder()
        }
        
        if textField == emailTxt
        {
            if esSE
            {
                UIView.animate(withDuration: 0.25) {
                    self.scrollRegistro.contentOffset.y += 60
                }
            }
            
            return phoneTxt.becomeFirstResponder()
        }
        
        if textField == passTxt
        {
            UIView.animate(withDuration: 0.25) {
                self.scrollRegistro.contentOffset.y += 50
            }
            
            return confirmpassTxt.becomeFirstResponder()
        }
        
        if textField == confirmpassTxt
        {
            return confirmpassTxt.resignFirstResponder()
        }
        
        return true
    }
    
}

extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let cancelar = UIBarButtonItem(title: "Cancelar", style: .done, target: onCancel.target, action: onCancel.action)
        cancelar.tintColor = UIColor.darkGray
        
        let aceptar = UIBarButtonItem(title: "Aceptar", style: .done, target: onDone.target, action: onDone.action)
        aceptar.tintColor = #colorLiteral(red: 0.1627927423, green: 0.138674736, blue: 0.1184585318, alpha: 1)
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            cancelar,
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            aceptar
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() {
        self.restorationIdentifier = "Aceptado"
        self.resignFirstResponder()
    }
    @objc func cancelButtonTapped() {
        self.restorationIdentifier = "Cancelado"
        self.resignFirstResponder()
    }
}

//print("Tamaño foto",imgPhoto.image?.size.width,imgPhoto.image?.size.height)
//let imgUpload = UIImage(cgImage: imgPhoto.image!.cgImage!, scale: 5, orientation: UIImage.Orientation.up)


//let transform = CLTransformation()
//Los bytes tienen que ser menor que : 10485760
//let height : Int32 = Int32(imgUpload.size.width)
//let width : Int32 = 700
//transform?.setHeightWith(height)
//transform?.setWidthWith(width)
//transform?.setAngleWith(90)
