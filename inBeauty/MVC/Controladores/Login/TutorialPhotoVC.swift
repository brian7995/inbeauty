//
//  TutorialPhotoVC.swift
//  InBeauty
//
//  Created by ADMIN on 26/08/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

@objc protocol CameraAndPhotos
{
    func openPickerControllerTypes()
}

class TutorialPhotoVC: UIViewController {

    @IBOutlet weak var imgViewTips: UIImageView!
    @IBOutlet weak var btnTip: UIButton!
    @IBOutlet weak var numberTip: UILabel!
    @IBOutlet weak var messageTip: UILabel!
    @IBOutlet weak var dontShowAgain: UILabel!
    @IBOutlet weak var switchShow: UISwitch!
    
    let tipsImgs = [#imageLiteral(resourceName: "tipPhoto_1"),#imageLiteral(resourceName: "tipPhoto_2"),#imageLiteral(resourceName: "tipPhoto_3")]
    let msjTips = ["Selfies are ok, better make a photoshoot","Stay simple and clean, use a good light","Show your personality and inspire"]
    var currentTip = 0
    var delegate : CameraAndPhotos?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews()
    {
        dontShowAgain.isHidden = true
        switchShow.isHidden = true
        
        let flechaImg = btnTip.imageView?.image?.withHorizontallyFlippedOrientation()
        btnTip.setImage(flechaImg, for: .normal)
    }
    
    @IBAction func showTutorialAgain(_ switchTuto: UISwitch)
    {
        UserDefaults.standard.set(switchTuto.isOn, forKey: "tutorial")
    }
    
    @IBAction func nextTipAction(_ btn: UIButton)
    {
        currentTip += 1
        guard currentTip < tipsImgs.count else {
            UserDefaults.standard.set(switchShow.isOn, forKey: "tutorial")
            dismiss(animated: true) {
                self.delegate?.openPickerControllerTypes()
            }
            return
        }
    
        if currentTip == tipsImgs.count - 1
        {
            dontShowAgain.isHidden = false
            switchShow.isHidden = false
            btnTip.setImage(nil, for: .normal)
            btnTip.setTitle("Ok", for: .normal)
        }
       
        numberTip.text = "Tip \(currentTip + 1)"
        imgViewTips.image = tipsImgs[currentTip]
        messageTip.text = msjTips[currentTip]
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
