//
//  WelcomeVC.swift
//  InBeauty
//
//  Created by ADMIN on 3/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet weak var discoverLabel: UILabel!
    @IBOutlet weak var welcomeInbiLabel: UILabel!
    
    var msj = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        var sizeDiscover :CGFloat = 0
        
        switch UIScreen.main.bounds.height {
        case 667:
            sizeDiscover = 20
        case 568:
            sizeDiscover = 18
        default:
            sizeDiscover = 22
        }
        //print(sizeDiscover)
        
        let fontSystem = UIFont(name:"Futura-Medium", size: sizeDiscover)
        //print(fontSystem)
        discoverLabel.font = fontSystem!
        discoverLabel.text = msj
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goWelcomeServices"
        {
            let welcomeVC = segue.destination as! WelcomeServiceVC
            let type = ChooseVC.tipo
            let msj = (type == "profesional") ? "Tracks of every service and build a better relationship with your clients." : "And manage your service records."
            welcomeVC.msj = msj
        }
    }
    

}
