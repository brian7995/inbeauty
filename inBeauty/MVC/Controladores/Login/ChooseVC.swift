//
//  ChooseVC.swift
//  InBeauty
//
//  Created by ADMIN on 3/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class ChooseVC: UIViewController {

    @IBOutlet weak var btnProfesional: UIButton!
    @IBOutlet weak var btnBeauty: UIButton!
    @IBOutlet weak var chooseLabel: UILabel!
    
    static var tipo : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _ = UserManager.deleteUserLogged {
            AppDelegate.user = nil
        }
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        chooseLabel.adjustsFontSizeToFitWidth = true
        
        btnBeauty.layer.cornerRadius = btnBeauty.bounds.midY
        btnProfesional.layer.cornerRadius = btnProfesional.bounds.midY
        
        if UIScreen.main.bounds.height == 568
        {
            btnProfesional.titleLabel?.font = UIFont(name:"Futura-Bold", size: 21)
            btnBeauty.titleLabel?.font = UIFont(name:"Futura-Bold", size: 21)
        }
        
        btnProfesional.titleLabel?.adjustsFontSizeToFitWidth = true
        btnBeauty.titleLabel?.adjustsFontSizeToFitWidth = true
        
        btnProfesional.titleLabel?.numberOfLines = 2
        btnProfesional.titleLabel?.textAlignment = .center
        
        btnBeauty.titleLabel?.numberOfLines = 2
        btnBeauty.titleLabel?.textAlignment = .center
    }
    
    @IBAction func goLoginAction(_ btn: UIButton)
    {
        ChooseVC.tipo = (btn.tag == 1) ? "profesional" : "cliente"
    
        let msj = (btn.tag == 1) ? "Know your client from within,manage your services,share your passion." : "discover top trends and talent arround you."
        self.performSegue(withIdentifier: "goWelcome", sender: msj)
    }
    
    //MARK: LOG OUT
    
    @IBAction func logOutInbi(_ segue:UIStoryboardSegue)
    {
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goWelcome"
        {
            let welcomeVC = segue.destination as! WelcomeVC
            welcomeVC.msj = sender as! String
        }
    }
}
