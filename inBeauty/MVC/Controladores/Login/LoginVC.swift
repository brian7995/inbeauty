//
//  ViewController.swift
//  InBeauty
//
//  Created by ADMIN on 11/03/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Firebase

class LoginVC: UIViewController,GIDSignInUIDelegate,GIDSignInDelegate {

    @IBOutlet weak var btnGoogleSigIn: UIButton!
    @IBOutlet weak var iBLabel: UILabel!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewLogin: UIView!
    
    let loginFB = LoginManager()
    var tokenFB : String = ""
    var tokenFBase : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.activarOcultamientoTeclado()
        
        InstanceID.instanceID().instanceID { (instance, error) in
            
            if let error = error
            {
                print("Error a la hora de obtener el token de firebase \(error)")
            }else
            {
                self.tokenFBase = instance?.token ?? "No hay token"
                print("Token de Firebase : \(self.tokenFBase)")
                
                if let user = UserManager.userLogged() as? User
                {
                    print("Hay un usuario guardado")
                    self.usernameTxt.text = user.email
                    self.passwordTxt.text = user.contrasena
                    self.loginFromServer()
                }
            }
        }
        //self.usernameTxt.text = "profesional@profesional.com"
        //self.passwordTxt.text = "123456"
        self.setupLogin()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
        {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //btnGoogleSigIn.isUserInteractionEnabled = true
    }

    func setupLogin()
    {
        //btnLogin.layer.masksToBounds = true
        self.btnLogin.layoutIfNeeded()
//        print("Bounds: ",btnLogin.bounds)
//        print("Frame: ",btnLogin.frame)
        btnLogin.layer.cornerRadius = btnLogin.bounds.midY
        
        let sizeLogin = UIScreen.main.bounds.width * 0.065
        let fontLogin = UIFont(name: "Futura-Bold", size: sizeLogin)
        btnLogin.titleLabel?.font = fontLogin
        btnAccount.titleLabel?.adjustsFontSizeToFitWidth = true
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        usernameTxt.delegate = self
        passwordTxt.delegate = self
        emailTxt.delegate = self
        //self.btnGoogleSigIn.style = .iconOnly
        
        viewEmail.layer.cornerRadius = 10
        viewEmail.layer.shadowRadius = 10
        viewEmail.layer.shadowOpacity = 0.25
    }
    
    @IBAction func showPass(_ btn: UIButton)
    {
        passwordTxt.isSecureTextEntry = !passwordTxt.isSecureTextEntry
    }
    
    @IBAction func loginAction(_ sender: UIButton)
    {
        self.loginFromServer()
    }
    
    func loginFromServer()
    {
        self.activityIndicator.startAnimating()
        
        guard !usernameTxt.text!.isEmpty && !passwordTxt.text!.isEmpty else {
            self.mostrarAlerta(titulo: "inBi", msj: "Please enter your username and password.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            self.activityIndicator.stopAnimating()
            return
        }
        
        view.isUserInteractionEnabled = false
       
        inBiServices.instancia.login(email: usernameTxt.text!, password: passwordTxt.text!, tokenFbase: self.tokenFBase) { (respuesta) in
            
            self.view.isUserInteractionEnabled = true
            guard let status = respuesta as? StatusUser,status.status == "success" else {
                let body = respuesta as? [String:Any] ?? [:]
                let data = body["data"] as? [String:Any] ?? [:]
                let msj = data["error"] as? String ?? ""
                self.mostrarAlerta(titulo: "inBi", msj: msj, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                self.activityIndicator.stopAnimating()
                return
            }
            
            AppDelegate.user = status.data
            AppDelegate.user?.contrasena = self.passwordTxt.text!
            let save = UserManager.saveUserLogged(AppDelegate.user, nil)
            print("Se pudo guardar al usuario? : \(save)")
            //print("token user :" , AppDelegate.user?.access_token ?? "No token")
            self.activityIndicator.stopAnimating()
            self.performSegue(withIdentifier: "goMenu", sender: self)
        }
    }
    
    //MARK: LOGUEO GOOGLE
    @IBAction func iniciarSesionGoogle(_ btn: UIButton)
    {
        btn.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil
        {
            let urlPhoto = user.profile.imageURL(withDimension: 1080)?.absoluteString ?? ""
            inBiServices.instancia.login(email: user.profile.email, password: user.userID, tokenFbase: self.tokenFBase) { (respuesta) in
                
                if let dataUser = respuesta as? StatusUser
                {
                    if dataUser.status == "success"
                    {
                        print("Ya existe este usuario")
                        AppDelegate.user = dataUser.data
                        AppDelegate.user?.foto = urlPhoto
                        AppDelegate.user?.contrasena = user.userID ?? "idGoogle"
                        let _ = UserManager.saveUserLogged(AppDelegate.user, nil)
                        self.performSegue(withIdentifier: "goMenu", sender: self)
                    }
                }else
                {
                    print("Registramos este usuario")
                    let idGoogle = user.userID ?? "idGoogle"
                    inBiServices.instancia.register(username: user.profile.name ?? "Sin nombre", email: user.profile.email ?? "\(idGoogle)@gmail.com", phone: "000000000", password: idGoogle, confirm: idGoogle , pais: "Estados Unidos", urlPhoto: urlPhoto, tokenFbase: self.tokenFBase) { (respuesta) in
                        
                        let body = respuesta as? [String:Any] ?? [:]
                        let data = body["data"] as? [String:Any]
                        
                        if let exito = body["status"] as? String,exito == "success"
                        {
                            let msj = data?["mensaje"] as? String ?? ""
                            self.mostrarAlerta(titulo: "Inbi", msj: msj, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                            //self.loginSocial(email: user.profile.email, password: user.userID, urlPhoto: user.profile.imageURL(withDimension: 1080)?.absoluteString)
                        }else
                        {
                            let error = data?["error"] as? String ?? "An error occurred while registering. Try again."
                            self.mostrarAlerta(titulo: "inBi", msj: error, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
                self.activityIndicator.stopAnimating()
                self.btnGoogleSigIn.isUserInteractionEnabled = true
            }
        }else
        {
            btnGoogleSigIn.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
            print("Error sig in Google : \(error.localizedDescription)")
        }
    }

    //MARK: LOGUEO FACEBOOK
    
    @IBAction func inciarSesionFBook(_ sender: UIButton) {

        self.desloguearseFB()
        activityIndicator.startAnimating()
        let permisos = ["public_profile","email"]
        
        loginFB.logIn(permissions: permisos, from: self) { (result, error) in
            
            if error != nil
            {
                print("Lo Sentimos,Ocurrio un error")
                self.activityIndicator.stopAnimating()
                sender.isUserInteractionEnabled = true
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                self.mostrarAlerta(titulo:"Lo Sentimos", msj: "Ocurrió un error.Por favor,inténtelo más tarde.", acciones: [okAction], bloqueCompletacion: nil)
                
            }else if (result?.isCancelled)!
            {
                self.activityIndicator.stopAnimating()
                sender.isUserInteractionEnabled = true
            }else
            {
                print("El usuario ha podido loguearse")
                self.tokenFB = (result?.token?.tokenString)!
                self.obtenerDatosFB()
            }
        }
    }
    
    func obtenerDatosFB()
    {
        GraphRequest.init(graphPath: "/me", parameters: ["fields":"id,first_name,last_name,email"]).start(completionHandler: { (conexion, resultGraph, errorGraph) in
            
            if errorGraph != nil
            {
                return
            }
            
            guard let datosFB = resultGraph as? [String:Any] else {return}
            
            let idfb = datosFB["id"] as? String ?? ""
            let nombre = datosFB["first_name"] as? String ?? "Sin nombre"
            let apellido = datosFB["last_name"] as? String ?? "Sin apellido"
            let email =  datosFB["email"] as? String
            let urlPhoto = "http://graph.facebook.com/\(idfb)/picture?type=large"
            print("Nombre",nombre)
            print("Apellido",apellido)
            print("idFB",idfb)
            print("email",email ?? "No hay email en FB")
            
            if let emailFB = email
            {
                inBiServices.instancia.login(email: emailFB, password: idfb, tokenFbase: self.tokenFBase, completacion: { (respuesta) in
                    
                    if let dataUser = respuesta as? StatusUser
                    {
                        if dataUser.status == "success"
                        {
                            print("Ya existe este usuario")
                            self.activityIndicator.stopAnimating()
                            AppDelegate.user = dataUser.data
                            AppDelegate.user?.foto = urlPhoto
                            AppDelegate.user?.contrasena = idfb
                            let _ = UserManager.saveUserLogged(AppDelegate.user, nil)
                            self.performSegue(withIdentifier: "goMenu", sender: self)
                        }
                    }else
                    {
                        print("Registramos este usuario")
                        inBiServices.instancia.register(username:nombre, email: emailFB, phone: "000000000", password:idfb, confirm: idfb, pais: "Estados Unidos", urlPhoto: urlPhoto, tokenFbase: self.tokenFBase) { (respuesta) in
                            
                            let body = respuesta as? [String:Any] ?? [:]
                            let data = body["data"] as? [String:Any]
                            self.activityIndicator.stopAnimating()
                            
                            if let exito = body["status"] as? String,exito == "success"
                            {
                                let msj = data?["mensaje"] as? String ?? ""
                                self.mostrarAlerta(titulo: "Inbi", msj: msj, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                                //self.loginSocial(email: email, password: idfb, urlPhoto: urlPhoto)
                            }else
                            {
                                let error = data?["error"] as? String ?? "An error occurred while registering. Try again."
                                self.mostrarAlerta(titulo: "inBi", msj: error, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                            }
                        }
                    }
                })
            }else
            {
                self.viewEmail.isHidden = false
                self.emailTxt.becomeFirstResponder()
                
                AppDelegate.user = User()
                AppDelegate.user?.name = nombre
                AppDelegate.user?.contrasena = idfb
                AppDelegate.user?.foto = urlPhoto
            }
        })
    }
    
    func loginSocial(email:String,password:String,urlPhoto:String?)
    {
        inBiServices.instancia.login(email: email, password: password, tokenFbase: self.tokenFBase) { (respuesta) in
            
            guard let status = respuesta as? StatusUser,status.status == "success" else {
                let body = respuesta as? [String:Any]
                let data = body?["data"] as? [String:Any]
                let error = data?["error"] as? String
                self.mostrarAlerta(titulo: "inBi", msj: error ?? "There was an error. Please try again.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                self.activityIndicator.stopAnimating()
                return
            }
            
            AppDelegate.user = status.data
            AppDelegate.user?.foto = urlPhoto
            AppDelegate.user?.contrasena = password
            let _ = UserManager.saveUserLogged(AppDelegate.user, nil)
            self.activityIndicator.stopAnimating()
            
            if status.data.tipo == "profesional"
            {
                self.performSegue(withIdentifier: "goTitles", sender: self)
            }else
            {
                self.performSegue(withIdentifier: "goMenu", sender: self)
            }
        }
    }
    
    @IBAction func registerWithFBAction(_ btn: UIButton)
    {
        guard let email = emailTxt.text,email.count > 0,email.contains("@") else {return}
        btn.isUserInteractionEnabled = false
        let user = AppDelegate.user ?? User()
        
        inBiServices.instancia.register(username:user.name, email: email, phone: "000000000", password:user.contrasena ?? "", confirm: user.contrasena ?? "", pais: "Estados Unidos", urlPhoto: user.foto ?? "", tokenFbase: self.tokenFBase) { (respuesta) in
            
            btn.isUserInteractionEnabled = true
            let body = respuesta as? [String:Any] ?? [:]
            let data = body["data"] as? [String:Any]
            self.activityIndicator.stopAnimating()
            self.viewEmail.isHidden = true
            
            if let exito = body["status"] as? String,exito == "success"
            {
                let msj = data?["mensaje"] as? String ?? ""
                self.mostrarAlerta(titulo: "Inbi", msj: msj + "When you confirm your account please reset your password.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                //self.loginSocial(email: email, password: idfb, urlPhoto: urlPhoto)
            }else
            {
                let error = data?["error"] as? String ?? "An error occurred while registering. Try again."
                self.mostrarAlerta(titulo: "inBi", msj: error, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            }
        }
    }
    
    @IBAction func accionDeslogueoFB(_ sender:Any)
    {
        self.desloguearseFB()
    }
    
    func desloguearseFB()
    {
        print("Nos deslogueamos")
        loginFB.logOut()
        AccessToken.current = nil
        Profile.current = nil
    }
    
    @IBAction func restorePasswordAction(_ btn: UIButton)
    {
        guard let url = URL(string: "http://68.183.115.126/password") else {return}
        btn.isUserInteractionEnabled = false
        
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:]) { (bool) in
                btn.isUserInteractionEnabled = true
            }
        }
    }
    
    //MARK: Regresar del Registro
    @IBAction func volverRegistro(_ segue: UIStoryboardSegue)
    {
        
    }
}

extension LoginVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == usernameTxt
        {
            return passwordTxt.becomeFirstResponder()
        }
        
        if textField == passwordTxt
        {
            self.loginFromServer()
            return passwordTxt.resignFirstResponder()
        }
        
        if textField == emailTxt
        {
            self.registerWithFBAction(UIButton())
            return emailTxt.resignFirstResponder()
        }
        
        return true
    }
}

extension UIViewController
{
    func comprobarConexionRed() -> Bool
    {
        return Reachability()!.isReachable
    }
    
    func comprobarInternet(completionHandler: @escaping (Bool,String) -> Void)
    {
        // 1. Check the WiFi Connection
        guard comprobarConexionRed() else {
            completionHandler(false,"You are not connected to any network.")
            return
        }
        
        // 2. Check the Internet Connection but possibly use www.apple.com or www.alibaba.com instead of google.com because it's not available in China
        let webAddress = "https://www.apple.com" // Default Web Site
        
        guard let url = URL(string: webAddress) else {
            //print("could not create url from: \(webAddress)")
            completionHandler(false, "null")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            if error != nil || response == nil {
                completionHandler(false, "Check the intensity of your internet and try again.")
            }else
            {
                completionHandler(true, "")
            }
        })
        
        task.resume()
    }
    
    func mostrarAlerta(titulo:String,msj:String,acciones:[UIAlertAction],bloqueCompletacion: (() -> Void)?)
    {
        let alertController = UIAlertController(title: titulo, message: msj, preferredStyle: .alert)
        
        acciones.forEach { (action) in
            alertController.addAction(action)
        }
        
        self.present(alertController, animated: true, completion: bloqueCompletacion)
    }
    
    func activarOcultamientoTeclado()
    {
        let tapKeyboard = UITapGestureRecognizer(target: self, action: #selector(ocultarTeclado(sender:)))
        self.view.addGestureRecognizer(tapKeyboard)
    }
    
    @objc func ocultarTeclado(sender:UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @objc func appAbiertaPorPush()
    {
        self.comprobarIdNtfcacion()
    }
    
    func comprobarIdNtfcacion()
    {
        //guard let _ = AppDelegate.pushNtfcacion else {return}
        print("Comprobamos el vc ntfc")
        let ntfVC = (storyboard?.instantiateViewController(withIdentifier: "NotificacionesVC"))!
        self.navigationController?.pushViewController(ntfVC, animated: true)
    }
    
    func escucharAppPushNtfcacion()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.appAbiertaPorPush), name: NSNotification.Name(rawValue: "PushNtfcaciones"), object: nil)
    }
    
    func quitarAppPushNtfcacion()
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "PushNtfcaciones"), object: nil)
    }
}
