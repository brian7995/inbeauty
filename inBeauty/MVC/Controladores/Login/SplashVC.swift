//
//  SplashVC.swift
//  InBeauty
//
//  Created by ADMIN on 5/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var iBLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //iBLabel.layer.cornerRadius = 45
        //iBLabel.layer.borderWidth = 1.5
        //iBLabel.layer.borderColor = UIColor.white.cgColor
        //2019-06-29
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (timer) in
            
            if let _ = UserManager.userLogged() as? User
            {
                if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginID")
                {
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
            }else
            {
                self.performSegue(withIdentifier: "goChoose", sender: self)
            }
        
        }
    }
}
