//
//  WelcomeServiceVC.swift
//  InBeauty
//
//  Created by ADMIN on 17/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class WelcomeServiceVC: UIViewController {
    
    @IBOutlet weak var serviceTxt: UILabel!
    var msj = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        serviceTxt.text = msj
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
