//
//  TitleVC.swift
//  InBeauty
//
//  Created by ADMIN on 29/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class TitleVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UITextFieldDelegate {

    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var collectionTitle: UICollectionView!
    @IBOutlet weak var btnOksLetsGo: UIButton!
    @IBOutlet weak var myTitleTxt: UITextField!
    @IBOutlet weak var subrayadoTitle: UIView!
    
    //let titulos = ["color artist","barber","makeup artist","hairstyler"]
    let user = AppDelegate.user ?? User()
    var categories : [Category] = []
    var img : UIImage?
    //var titlesLabels = [UILabel]()
    //var sizesTitle = [CGSize]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.listTitlesWS()
    }
    
    func setupViews()
    {
        self.imgUser.alpha = 0
        self.imgUser.layoutIfNeeded()
        self.imgUser.clipsToBounds = true
        self.imgUser.layer.cornerRadius = self.imgUser.bounds.midX
        
        imgUser.cargarImgDesdeURL(urlImg: user.foto ?? "") {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgUser.alpha = 1
            })
        }
        
        collectionTitle.dataSource = self
        collectionTitle.delegate = self
        
        myTitleTxt.delegate = self
        myTitleTxt.attributedPlaceholder = NSAttributedString(string:"your title", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        //let sizeBtnOks = UIScreen.main.bounds.width * 0.085
        //btnOksLetsGo.titleLabel?.font = UIFont(name: "Futura Bold", size: sizeBtnOks)
    }
    
    func listTitlesWS()
    {
        inBiServices.instancia.listCategories(token: user.access_token) { (respuesta) in
            
            guard let status = respuesta as? StatusCategory,status.status == "success" else {return}
            
            self.categories = status.data
            self.collectionTitle.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func backAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleCell", for: indexPath)
        
        cell.layer.cornerRadius = cell.bounds.midY
        cell.layer.borderWidth = 1
        cell.layer.borderColor = #colorLiteral(red: 0.5990038032, green: 0.5665634729, blue: 1, alpha: 1)
        
        let category = categories[indexPath.row]
        let name = cell.viewWithTag(100) as! UILabel
        
        let selected = category.selected ?? false
        categories[indexPath.row].selected = selected
        
        name.textColor = (category.selected ?? false) ? UIColor.darkGray : UIColor.lightGray
        name.text = category.nombre
    
        cell.backgroundColor = (category.selected ?? false) ? #colorLiteral(red: 0.5990038032, green: 0.5665634729, blue: 1, alpha: 1) : UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let category = categories[indexPath.row]
        let widthTitle = CGFloat(category.nombre.count) * 10
        let finalWidth = (widthTitle <= 100) ? 110 : widthTitle
        return CGSize(width: finalWidth , height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categories[indexPath.row].selected = !(categories[indexPath.row].selected ?? false)
        collectionView.reloadItems(at: [indexPath])
    }
   
    @IBAction func oksLetsGoAction(_ sender: UIButton)
    {
        let marked = categories.filter({return $0.selected ?? false})
        
        guard marked.count > 0 && !myTitleTxt.text!.isEmpty else {
            subrayadoTitle.backgroundColor = UIColor.red
            return
        }
        
        subrayadoTitle.backgroundColor = UIColor.white
        let ids = marked.map({return $0.id})
    
        user.miscategorias = marked
        user.title = myTitleTxt.text
        
        inBiServices.instancia.saveCategories(token: user.access_token, title: myTitleTxt.text!, categories: ids) { (respuesta) in
            guard let exito = respuesta as? String,exito == "success" else {return}
            self.performSegue(withIdentifier: "goMenu", sender: self)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//        cell.addSubview(title)
//        cell.viewWithTag(100)!.translatesAutoresizingMaskIntoConstraints = false
//        cell.viewWithTag(100)!.centerXAnchor.constraint(equalToSystemSpacingAfter: cell.centerXAnchor, multiplier: 1.0).isActive = true
//        cell.viewWithTag(100)!.centerYAnchor.constraint(equalToSystemSpacingBelow: cell.centerYAnchor, multiplier: 1.0).isActive = true

/*
 func instanciarLabelsTitulos()
 {
 for titulo in self.titulos
 {
 let titleLabel = UILabel()
 titleLabel.tag = 100
 titleLabel.adjustsFontSizeToFitWidth = true
 titleLabel.text = titulo
 titleLabel.textColor = UIColor.lightGray
 titleLabel.font = UIFont(name: "Courier", size: 15)
 titleLabel.textAlignment = .center
 titleLabel.sizeToFit()
 self.titlesLabels.append(titleLabel)
 }
 }
 */
