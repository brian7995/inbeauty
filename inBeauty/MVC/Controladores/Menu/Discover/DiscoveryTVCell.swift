//
//  DiscoveryTVCell.swift
//  InBeauty
//
//  Created by Brian Nuñez Rios on 6/30/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class DiscoveryTVCell: UITableViewCell {
    
    @IBOutlet weak var imgViewClient: UIImageView!
    @IBOutlet weak var imgViewProfessional: UIImageView!
    @IBOutlet weak var tipoServicio: UILabel!
    @IBOutlet weak var nameProfesional: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var authorTag: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewClient.alpha = 0
        imgViewClient.setGradientBackground(colorOne: UIColor.clear, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.0, y2: 0.9, indice: 0, opacity: 0.5)
        imgViewProfessional.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgViewClient.image = nil
    }
    
    func buildCell(discovery:Discovery)
    {
        tipoServicio.text = discovery.categoria.nombre
        date.text = discovery.fecha_recordatorio
        nameProfesional.text = discovery.profesional.name
        authorTag.text = discovery.author_tag
        
        let urlClient = discovery.cliente.foto ?? discovery.cliente.foto_url
        
        imgViewClient.cargarImgDesdeURL(urlImg: urlClient) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewClient.alpha = 1
            })
        }
        
        let urlPro = discovery.profesional.foto ?? discovery.profesional.foto_url
        
        imgViewProfessional.cargarImgDesdeURL(urlImg: urlPro) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewProfessional.alpha = 1
            })
        }
    }
    
    

}
//if let imgFromCache = imgCache.object(forKey: discovery.cliente.foto_url as NSString)
//{
//    self.imgViewClient.image = imgFromCache
//    return
//}
//
//Alamofire.request(discovery.cliente.foto_url).responseData { (respuesta) in
//
//    guard let data = respuesta.data else
//    {
//        print("No tenemos data! 😱")
//        return
//    }
//
//    let img = UIImage(data: data)!
//    imgCache.setObject(img, forKey: discovery.cliente.foto_url as NSString)
//    self.imgViewClient.image = img
//}
