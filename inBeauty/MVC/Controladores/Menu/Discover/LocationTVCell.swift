//
//  LocationTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 10/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class LocationTVCell: UITableViewCell {

    @IBOutlet weak var imgViewPro: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewPro.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    
    func buildCell(pro:Profesional)
    {
        name.text = pro.name
        location.text = pro.location
        
        let urlPro = pro.foto ?? pro.foto_url
        
        if let imgFromCache = imgCache.object(forKey: urlPro as NSString)
        {
            self.imgViewPro.image = imgFromCache
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewPro.alpha = 1
            })
            return
        }
        
        //print(category.icon_url)
        Alamofire.request(urlPro).responseData { (respuesta) in
            
            guard let data = respuesta.data else
            {
                print("No  tenemos data! 😱")
                return
            }
            
            let img = UIImage(data: data)!
            imgCache.setObject(img, forKey: urlPro as NSString)
            self.imgViewPro.image = img
            
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewPro.alpha = 1
            })
        }
    }
}
