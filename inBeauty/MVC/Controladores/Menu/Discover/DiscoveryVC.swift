//
//  DiscoveryVC.swift
//  InBeauty
//
//  Created by ADMIN on 23/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

//let nameApp = "inBi"
class DiscoveryVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var discoveryTV: UITableView!
    @IBOutlet weak var btnDiscovery: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var progressViewDiscovery: UIProgressView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    
    var discovery = [Discovery]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tabBarItem.image.
        discoveryTV.dataSource = self
        discoveryTV.delegate = self
        
        btnDiscovery.titleLabel?.adjustsFontSizeToFitWidth = true
        searchTxt.delegate = self
        //btnDiscovery.imageView?.contentMode = .center
        
        searchTxt.attributedPlaceholder = NSAttributedString(string: "search", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        
        viewSearch.layer.cornerRadius = viewSearch.bounds.midY
        viewSearch.layer.borderWidth = 1
        viewSearch.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        //self.activityIndicator.startAnimating()
        listDiscoveryWS()
        
        let refreshControl = UIRefreshControl(frame: discoveryTV.frame)
        refreshControl.addTarget(self, action: #selector(listDiscoveryWS), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        discoveryTV.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func listDiscoveryWS()
    {
        //self.progressViewDiscovery.setProgress(1, animated: true)
        self.discovery.removeAll()
        self.discoveryTV.refreshControl?.beginRefreshing()
        self.discoveryTV.reloadData()
        
        inBiServices.instancia.listDiscovery(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
            
            if let discovery = respuesta as? [Discovery] {
                self.discovery = discovery
                self.discoveryTV.reloadData()
            }
    
            self.discoveryTV.refreshControl?.endRefreshing()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.discovery.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPicture", for: indexPath) as! DiscoveryTVCell
    
        //(cell.viewWithTag(100) as! UIImageView).image = UIImage(named: "profilePicture\(indexPath.row)") ?? #imageLiteral(resourceName: "profilePicture6")
        //(cell.viewWithTag(101) as! UIImageView).image = UIImage(named: "profilePicture\(6 - indexPath.row)") ?? #imageLiteral(resourceName: "profilePicture5")
        cell.tag = indexPath.row
        let discovery = self.discovery[indexPath.row]
        cell.buildCell(discovery: discovery)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("Deselect Discovery VC")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Select Discovery VC")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

    @IBAction func backDiscovery(_ segue:UIStoryboardSegue)
    {
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goDetailClient"
        {
            let detailClientVC = segue.destination as! DetailClientVC
            let cell = sender as! DiscoveryTVCell
            detailClientVC.discovery = self.discovery[cell.tag]
            detailClientVC.imgClient = cell.imgViewClient.image
            detailClientVC.imgProfessional = cell.imgViewProfessional.image
        }
    }
    

}
