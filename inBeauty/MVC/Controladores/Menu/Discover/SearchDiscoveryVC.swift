//
//  SearchDiscoveryVC.swift
//  InBeauty
//
//  Created by ADMIN on 3/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class SearchDiscoveryVC: UIViewController,UICollectionViewDataSource,UISearchBarDelegate,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var filterX: NSLayoutConstraint!
    @IBOutlet weak var subFilter: UIView!
    @IBOutlet weak var filterCV: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var btnsFilters: [UIButton]!
    @IBOutlet weak var noResults: UILabel!
    @IBOutlet weak var locationTV: UITableView!
    @IBOutlet weak var activitySearch: UIActivityIndicatorView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    
    var discovery = [Discovery]()
    var professional = [Profesional]()
    
    enum Filter : String
    {
        case style = "service"
        case artist = "professional"
        case location = "location"
    }
    
    var myFilter : Filter = .style
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchFilterWS(search: "", filter: myFilter.rawValue)
        setupViews()
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        btnsFilters.forEach({return $0.titleLabel?.adjustsFontSizeToFitWidth = true})
        
        filterCV.dataSource = self
        filterCV.delegate = self
        locationTV.dataSource = self
        locationTV.delegate = self
        searchBar.delegate = self
        searchTxt.delegate = self
        
        searchTxt.attributedPlaceholder = NSAttributedString(string: "search", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        
        viewSearch.layer.cornerRadius = viewSearch.bounds.midY
        viewSearch.layer.borderWidth = 1
        viewSearch.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        let flowLayout = filterCV.collectionViewLayout as! UICollectionViewFlowLayout
        let size = (UIScreen.main.bounds.width - 2) / 3
        flowLayout.itemSize = CGSize(width: size, height: size)
        
        filterX.constant = -((btnsFilters[0].bounds.width + subFilter.bounds.width) - (subFilter.bounds.width - subFilter.bounds.height))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print(searchBar.text!)
        self.searchFilter()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchFilter()
        return textField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       //self.searchFilter()
    }

    func searchFilter()
    {
        print("El filtro es ",myFilter.rawValue)
        searchFilterWS(search: searchTxt.text!, filter: myFilter.rawValue)
        searchBar.resignFirstResponder()
    }
    
    func searchFilterWS(search:String,filter:String)
    {
        self.discovery.removeAll()
        self.professional.removeAll()
        self.filterCV.reloadData()
        self.locationTV.reloadData()
        self.activitySearch.startAnimating()
//        print(search)
//        print(filter)
        inBiServices.instancia.searchDiscovery(token: AppDelegate.user?.access_token ?? "", search: search, filter: filter) { (respuesta) in
            
            //print(respuesta)
            if let discovery = respuesta as? StatusDiscovery
            {
                self.discovery = discovery.data
            }
            
            if let professional = respuesta as? StatusProfesional
            {
                self.professional = professional.data
            }
            
            self.noResults.isHidden = !(self.discovery.isEmpty && self.professional.isEmpty)
            self.activitySearch.stopAnimating()
            self.filterCV.reloadData()
            self.locationTV.reloadData()
        }
    }
    
    @IBAction func selectFilterAction(_ btn: UIButton)
    {
        var constantX :CGFloat = 0
        
        btnsFilters.forEach({return $0.setTitleColor(UIColor.lightGray, for: .normal)})
        
        switch btn.tag {
        case 0:
            btn.setTitleColor(#colorLiteral(red: 0.6085910201, green: 0.5922448039, blue: 0.9975995421, alpha: 1), for: .normal)
            myFilter = .style
            //searchBar.textContentType = UITextContentType.name
            constantX = (btn.bounds.width + subFilter.bounds.width) - (subFilter.bounds.width - subFilter.bounds.height)
            constantX.negate()
        case 1:
            btn.setTitleColor(#colorLiteral(red: 0.6085910201, green: 0.5922448039, blue: 0.9975995421, alpha: 1), for: .normal)
            myFilter = .artist
            //searchBar.textContentType = UITextContentType.jobTitle
            constantX = 0
        case 2:
            btn.setTitleColor(#colorLiteral(red: 0.6085910201, green: 0.5922448039, blue: 0.9975995421, alpha: 1), for: .normal)
            myFilter = .location
            //searchBar.textContentType = UITextContentType.location
            constantX = (btn.bounds.width + subFilter.bounds.width) - (subFilter.bounds.width - subFilter.bounds.height)
        default:
            break
        }
        
        filterCV.isHidden = (btn.tag != 0)
        locationTV.isHidden = (btn.tag == 0)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.filterX.constant = constantX
            self.view.layoutIfNeeded()
        }, completion: { (bool) in
        })

        self.searchFilterWS(search: searchBar.text!, filter: myFilter.rawValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return discovery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath)
        let imgView = cell.viewWithTag(100) as! UIImageView
        
        imgView.image = nil
        
        let discovery = self.discovery[indexPath.row]
        let urlClient = discovery.cliente.foto ?? discovery.cliente.foto_url
        imgView.cargarImgDesdeURL(urlImg: urlClient) {
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
        }
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)!
        let imgView = cell.viewWithTag(100) as! UIImageView

        let discovery = self.discovery[indexPath.item]
        let lista : [Any] = [discovery,imgView.image ?? UIImage()]
        self.performSegue(withIdentifier: "goClient", sender: lista)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return professional.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! LocationTVCell
        let pro = self.professional[indexPath.row]
        cell.buildCell(pro: pro)
        return cell
    }
    
//    func buildCell(imgView:UIImageView,url:String)
//    {
//        if let imgFromCache = imgCache.object(forKey:url as NSString)
//        {
//            imgView.image = imgFromCache
//            return
//        }
//
//        Alamofire.request(url).responseData { (respuesta) in
//
//            guard let data = respuesta.data else
//            {
//                print("No tenemos data! 😱")
//                return
//            }
//
//            let img = UIImage(data: data)!
//            imgCache.setObject(img, forKey: url as NSString)
//            imgView.image = img
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! LocationTVCell
        let imgView = cell.imgViewPro
        let profesional = professional[indexPath.item]
        let lista : [Any] = [profesional,imgView?.image ?? UIImage()]
        self.performSegue(withIdentifier: "goProfessional", sender: lista)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goProfessional"
        {
            let detailProVC = segue.destination as! ProfileVC
            let lista = sender as! [Any]
            let professional = lista.first as? Profesional
            
            if AppDelegate.user?.id == professional?.id 
            {
                detailProVC.myProfile = .my
            }else
            {
                detailProVC.myProfile = .professional
                detailProVC.professional = professional
                detailProVC.img = lista.last as? UIImage
            }
        }
        
        if segue.identifier == "goClient"
        {
            let detailClientVC = segue.destination as! DetailClientVC
            let lista = sender as! [Any]
            //print(lista.first)
            detailClientVC.discovery = lista.first as? Discovery
            detailClientVC.imgClient = lista.last as? UIImage
            detailClientVC.goPro = true
        }
    }
}
