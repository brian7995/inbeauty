//
//  DetailClientVC.swift
//  InBeauty
//
//  Created by ADMIN on 4/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class DetailClientVC: UIViewController {
    
    @IBOutlet weak var imgViewSmall: UIImageView!
    @IBOutlet weak var imgViewBig: UIImageView!
    @IBOutlet weak var imgViewProfessional: UIImageView!
    @IBOutlet weak var nameClient: UILabel!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var nameProfessional: UILabel!
    @IBOutlet weak var job: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var titleSatisfaction: UILabel!
    @IBOutlet weak var satisfacction: UILabel!
    @IBOutlet weak var viewProfessional: UIView!
    @IBOutlet weak var indicatorAfterBefore: UIView!
    @IBOutlet weak var stackBtnService: UIStackView!
    @IBOutlet weak var stackAfBe: UIStackView!
    
    var discovery : Discovery?
    var pictures : [Picture] = []
    var dictImgsBefore : [String:UIImage] = [:]
    var dictImgsAfter : [String:UIImage] = [:]
    var imgProfessional : UIImage?
    var imgClient : UIImage?
    var goPro : Bool = true
    
    var profileBtn : String = "Before-left"
    
    let idsBefore = ["Before-left","Before-front","Before-right","Before-top","Before-back"]
    let idsAfter = ["After-left","After-front","After-right","After-top","After-back"]
    
    enum GoProfile
    {
        case client
        case professional
    }
    
    enum TimePhoto
    {
        case before
        case after
    }
    
    var timePhoto : TimePhoto = .before
    var goProfile : GoProfile = .client
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    func setupViews()
    {
        print("Setup views")
        self.view.layoutIfNeeded()
        
        indicatorAfterBefore.layer.cornerRadius = indicatorAfterBefore.bounds.midY
        imgViewBig.image = imgClient
        imgViewBig.alpha = 0
        imgViewBig.setGradientBackground(colorOne: UIColor.black, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.2, y2: 1.0, indice: 0, opacity: 0.3)
        
        imgViewSmall.layer.borderWidth = 2
        imgViewSmall.layer.borderColor = UIColor.white.cgColor
        imgViewSmall.layer.cornerRadius = imgViewSmall.bounds.midY
        
        let urlPhotoClient = discovery?.cliente.foto_url ?? ""
        imgViewSmall.cargarImgDesdeURL(urlImg: urlPhotoClient) {
        }
      
        imgViewProfessional.layer.cornerRadius = imgViewProfessional.bounds.midY
        
        nameClient.text = discovery?.author_tag
        nameCategory.text = discovery?.categoria.nombre
        
        nameProfessional.text = discovery?.profesional.name
        date.text = discovery?.fecha_recordatorio
        job.text = discovery?.style ??  "Job"
        
        let tapPro = UITapGestureRecognizer(target: self, action: #selector(tapGoProfessional(tap:)))
        let tapClient = UITapGestureRecognizer(target: self, action: #selector(tapGoProfessional(tap:)))
        
        imgViewSmall.addGestureRecognizer(tapClient)
        nameProfessional.addGestureRecognizer(tapPro)
    
        stackBtnService.subviews.forEach { (view) in
            let btn = view as! UIButton
            btn.imageView?.contentMode = .scaleAspectFit
        }
        
        self.imgViewProfessional.alpha = 0
        
        self.pictureProfessional(url: discovery?.profesional.foto_url ?? "")
        /*if let imgPro = imgProfessional
        {
            self.imgViewProfessional.image = imgPro
            UIView.animate(withDuration: 0.25) {
                self.imgViewProfessional.alpha = 1
            }
        }else
        {
            
        }*/
        
        self.viewProfessional.layer.cornerRadius = viewProfessional.bounds.midY / 4
        
        //guard AppDelegate.user?.tipo == "profesional" else {return}
        
        if AppDelegate.user?.id == discovery?.profesional_id
        {
            satisfacction.isHidden = false
            titleSatisfaction.isHidden = false
            let dictSatisfaction : [Int:String] = [1:"😅",2:"🤨",3:"😃",4:"😕",5:"😓"]
            satisfacction.text = dictSatisfaction[discovery?.satisfaccion ?? -1]
        }else
        {
            satisfacction.isHidden = true
            titleSatisfaction.isHidden = true
        }
        
        for (indice,btn) in stackBtnService.subviews.enumerated()
        {
            btn.restorationIdentifier = idsBefore[indice]
        }
        
        setupPhotos()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ btn: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupPhotos()
    {
        print("setup photos")
        guard let pictures = discovery?.fotos,pictures.count > 0 else {
            self.imgViewBig.image = imgClient
            UIView.animate(withDuration: 0.25) {
                self.imgViewBig.alpha = 1
            }
            return
        }
        
        print("fotosss")
//        if let beforeLeft = pictures.first(where: {return $0.perfil == "Before-left"})
//        {
//            let indexBeforeLeft = pictures.firstIndex(where: {return $0.perfil == "Before-left"})!
//            pictures.remove(at: indexBeforeLeft)
//            pictures.insert(beforeLeft, at: 0)
//        }
        
        for (indice,photo) in pictures.enumerated()
        {
            let profile = photo.perfil
            let isBefore = profile.contains("Before")
            
            if let imgFromCache = imgCache.object(forKey: photo.url_fotos as NSString)
            {
                if isBefore
                {
                    self.dictImgsBefore[profile] = imgFromCache
                    
                    if profile == "Before-left"
                    {
                        print("Mostramos la primera foto")
                        self.showPictureTaken(profile:"Before-left", dict: dictImgsBefore)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewBig.alpha = 1
                        }
                    }
                }else
                {
                    self.dictImgsAfter[profile] = imgFromCache
                }
                
                if indice == pictures.count - 1 && self.imgViewBig.alpha == 0
                {
                    self.imgViewBig.image = imgClient
                    UIView.animate(withDuration: 0.25) {
                        self.imgViewBig.alpha = 1
                    }
                }
                
                continue
            }
            
            request(photo.url_fotos).responseData { (respuesta) in
                
                guard let data = respuesta.data else
                {
                    print("No tenemos  data 😱")
                    return
                }
                
                guard let img = UIImage(data: data) else {
                    if indice == 0
                    {
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewBig.alpha = 1
                        }
                    }
                    return
                }
                
                if isBefore
                {
                    self.dictImgsBefore[profile] = img
                    if profile == "Before-left"
                    {
                        print("Mostramos la primera foto")
                        self.showPictureTaken(profile: "Before-left", dict: self.dictImgsBefore)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewBig.alpha = 1
                        }
                    }
                }else
                {
                    self.dictImgsAfter[profile] = img
                }
                
                if indice == pictures.count - 1 && self.imgViewBig.alpha == 0
                {
                    self.imgViewBig.image = self.imgClient
                    UIView.animate(withDuration: 0.25) {
                        self.imgViewBig.alpha = 1
                    }
                }
                
                imgCache.setObject(img, forKey: photo.url_fotos as NSString)
            }
        }
        
    }
    
    @objc func tapGoProfessional(tap:UITapGestureRecognizer)
    {
        guard let viewGesture = tap.view else {return}
        
        switch viewGesture
        {
        case is UILabel:
            goProfile = .professional
            if AppDelegate.user?.id != discovery?.profesional.id
            {
                //self.navigationController?.popViewController(animated: true)
                self.performSegue(withIdentifier: "goProfile", sender: self.discovery?.profesional)
            }else
            {
               
            }
        case is UIImageView:
            goProfile = .client
            if AppDelegate.user?.id != discovery?.cliente.id
            {
                self.performSegue(withIdentifier: "goProfile", sender: self.discovery?.cliente)
                //self.navigationController?.popViewController(animated: true)
            }else
            {
                
            }
        default:
            break
        }
    }
    
    func pictureProfessional(url:String)
    {
        self.imgViewProfessional.cargarImgDesdeURL(urlImg: url) {
            UIView.animate(withDuration: 0.25) {
                self.imgViewProfessional.alpha = 1
            }
        }
    }
    
    @IBAction func timePhotosAction(_ btn: UIButton)
    {
        btn.alpha = 1
        
        let btnBefore = stackAfBe.subviews.first!
        let btnAfter = stackAfBe.subviews.last!
        
        let centerXIndicator = view.constraints.first(where: {return $0.identifier == "centerX"})!
        
        centerXIndicator.constant = (btn.tag == 1) ?  -70 : 70
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
        }
        
        switch btn.tag {
        case 1:
            print("Before")
            timePhoto = .before
            btnAfter.alpha = 0.5
            
            for (indice,btn) in stackBtnService.subviews.enumerated()
            {
                btn.restorationIdentifier = idsBefore[indice]
            }
            
            if let rangeBefore = profileBtn.range(of: "After")
            {
                profileBtn.replaceSubrange(rangeBefore, with: "Before")
            }
            
            showPictureTaken(profile: profileBtn, dict: dictImgsBefore)
        case 2:
            print("After")
            timePhoto = .after
            btnBefore.alpha = 0.5
            
            for (indice,btn) in stackBtnService.subviews.enumerated()
            {
                btn.restorationIdentifier = idsAfter[indice]
            }
            
            if let rangeBefore = profileBtn.range(of: "Before")
            {
                profileBtn.replaceSubrange(rangeBefore, with: "After")
            }
            
            showPictureTaken(profile: profileBtn, dict: dictImgsAfter)
            
        default:
            break
        }
    }
    
    @IBAction func selectedFaceAction(_ btn: UIButton)
    {
        profileBtn = btn.restorationIdentifier ?? "no id"
        print(profileBtn)
        switch timePhoto {
        case .before:
            showPictureTaken(profile: profileBtn, dict: dictImgsBefore)
        case .after:
            showPictureTaken(profile: profileBtn, dict: dictImgsAfter)
        }
    }
    
    func showPictureTaken(profile:String,dict:[String:UIImage])
    {
        guard let imgPhoto = dict[profile] else {
            self.imgViewBig.image = imgClient
            UIView.animate(withDuration: 0.25) {
                self.imgViewBig.alpha = 1
            }
            return
        }
        self.imgViewBig.image = imgPhoto
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goProfile"
        {
            let detailProfileVC = segue.destination as! ProfileVC
            
            switch goProfile
            {
            case .client:
                let client = sender as? Client
                
                detailProfileVC.client = client
                detailProfileVC.img = imgViewSmall.image
                detailProfileVC.myProfile = .client
                
                let isMe = AppDelegate.user?.id == client?.id
                detailProfileVC.myProfile = (isMe) ? .my : .client
            case .professional:
                let professional = sender as? Profesional
                detailProfileVC.professional = professional
                detailProfileVC.img = imgViewProfessional.image
                
                let isMe = AppDelegate.user?.id == professional?.id
                detailProfileVC.myProfile = (isMe) ? .my : .professional
            }
        }
        
        if segue.identifier == "goInbox"
        {
            let messageVC = segue.destination as! MessagesVC
            messageVC.idReceptor = self.discovery?.cliente.id ?? ""
            messageVC.imgClient = self.imgViewSmall.image
        }
    }
}
