//
//  SearchInboxVC.swift
//  InBeauty
//
//  Created by ADMIN on 14/08/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class SearchInboxVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var usersTV: UITableView!
    
    var customers : [Client] = []
    
    enum Search
    {
        case list
        case filter
    }
    
    var searchUsers : Search = .list
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usersTV.dataSource = self
        self.usersTV.delegate = self
        
        self.searchBar.delegate = self
        
        let refreshControl = UIRefreshControl(frame: usersTV.frame)
        refreshControl.addTarget(self, action: #selector(listCustomersWS), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        usersTV.refreshControl = refreshControl
        
        self.listCustomersWS()
    }
    
    @IBAction func backAction(_ btn: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func listCustomersWS()
    {
        self.customers.removeAll()
        self.usersTV.refreshControl?.beginRefreshing()
        self.usersTV.reloadData()
        
        switch self.searchUsers {
        case .list:
            inBiServices.instancia.listCustomers(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
                
                if let customers = respuesta as? [Client]  {
                    self.customers = customers
                    self.usersTV.reloadData()
                }
                
                self.usersTV.refreshControl?.endRefreshing()
            }
        case .filter:
            searchUsersWS(user: self.searchBar.text ?? "")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let user = searchBar.text,user.count > 0
        {
            self.searchUsers = .filter
            searchUsersWS(user: user)
        }else
        {
            self.searchUsers = .list
            listCustomersWS()
        }
        
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchUsers = .filter
        //let currentSearchText = searchText.replacingOccurrences(of: " ", with: "%20")
        self.searchUsersWS(user: searchText)
    }
    
    func searchUsersWS(user:String)
    {
        self.customers.removeAll()
        self.usersTV.refreshControl?.beginRefreshing()
        self.usersTV.reloadData()
        
        inBiServices.instancia.searchUser(token: AppDelegate.user?.access_token ?? "", user: user, chat: 1) { (respuesta) in
            
            if let users = respuesta as? [Client]
            {
                self.customers = users
                self.usersTV.reloadData()
            }
    
            self.usersTV.refreshControl?.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! ClientTVCell
        cell.tag = indexPath.row
        
        let client = customers[indexPath.row]
        cell.buildClient(client: client)
        
        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goMessage"
        {
            let messageVC = segue.destination as! MessagesVC
            let cell = sender as! ClientTVCell
            let imgUserView = cell.imgView
            
            messageVC.idReceptor = customers[cell.tag].id
            messageVC.imgClient = imgUserView?.image
        }
    }
    

}
