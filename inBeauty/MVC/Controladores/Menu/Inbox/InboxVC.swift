//
//  InboxVC.swift
//  InBeauty
//
//  Created by ADMIN on 2/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import UserNotifications

class InboxVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var inboxTV: UITableView!
    @IBOutlet weak var activityInbox: UIActivityIndicatorView!
    
    var myChats = [Chat]()
    let user = AppDelegate.user ?? User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inboxTV.dataSource = self
        self.inboxTV.delegate = self
        
        //formato de hora,minutos y am/pm : "h : mm a"
        //self.activityInbox.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(listChatsWS), name: Notification.Name(rawValue: "NewMessage"), object: nil)
        
        let refreshControl = UIRefreshControl(frame: inboxTV.frame)
        refreshControl.addTarget(self, action: #selector(listChatsWS), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        inboxTV.refreshControl = refreshControl
        
        self.listChatsWS()
    }
    
    @objc func newMessage(ntf:Notification)
    {
        self.listChatsWS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Inbox Will Appear")
        UIApplication.shared.applicationIconBadgeNumber = 0
        self.tabBarItem.badgeValue = nil
    }
    
    @objc func listChatsWS()
    {
        self.myChats.removeAll()
        self.inboxTV.refreshControl?.beginRefreshing()
        self.inboxTV.reloadData()
        
        inBiServices.instancia.getChatsActives(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
            
            if let chats = respuesta as? [Chat],chats.count > 0 {
                self.myChats = chats
                self.inboxTV.reloadData()
            }
            
            self.inboxTV.refreshControl?.endRefreshing()
            //self.activityInbox.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath)
        cell.tag = indexPath.row
        
        let imgView = cell.viewWithTag(100) as! UIImageView
        let name = cell.viewWithTag(101) as! UILabel
        let lastMessage = cell.viewWithTag(102) as! UILabel
        let readView = cell.viewWithTag(104) as! UIImageView
        
        let chat = myChats[indexPath.row]
        name.text = chat.chatcon.name
        lastMessage.text = chat.mensaje
        
        let read = chat.leido
        let myMessage = chat.de_user_id == user.id
        
        let nameFont = read == 0 && !myMessage ? "Futura-Bold" : "Futura-Medium"

        lastMessage.textColor = read == 0 && !myMessage ? UIColor.darkText : UIColor.lightGray
        lastMessage.font = UIFont(name: nameFont, size: 13)

        readView.backgroundColor = read == 0 && !myMessage ? #colorLiteral(red: 0.6588235294, green: 0.5098039216, blue: 0.9764705882, alpha: 1) : nil
        readView.image = read == 0  && !myMessage ? nil : #imageLiteral(resourceName: "forward")
        readView.alpha = read == 0 && !myMessage ? 1 : 0.25
        
        imgView.cargarImgDesdeURL(urlImg: chat.chatcon.foto_url ?? "") {
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Select InBox VC")
        myChats[indexPath.row].leido = 1
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    @IBAction func regresarInbox(_ segue: UIStoryboardSegue)
    {
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goMessage"
        {
            let messageVC = segue.destination as! MessagesVC
            
            let cell = sender as! UITableViewCell
            let imgView = cell.viewWithTag(100) as! UIImageView
            messageVC.imgClient = imgView.image
            messageVC.chatUser = self.myChats[cell.tag]
        }
    }
}
