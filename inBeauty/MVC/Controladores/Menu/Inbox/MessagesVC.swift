//
//  MessagesVC.swift
//  InBeauty
//
//  Created by ADMIN on 2/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import AVFoundation

class MessagesVC: UIViewController,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var messageTxt: UITextField!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var messageCV: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var iconBoardView: UIImageView!
    @IBOutlet weak var createRecommendation: UIView!
    @IBOutlet weak var createRecommendationTitle: UILabel!
    @IBOutlet weak var bottomRecommendation: NSLayoutConstraint!
    @IBOutlet weak var btnBoard: UIButton!
    @IBOutlet weak var relatedServiceCV: UICollectionView!
    @IBOutlet weak var btnRelatedBack: UIButton!
    @IBOutlet weak var btnRelatedClose: UIButton!
    @IBOutlet weak var relatedServiceTitle: UILabel!
    @IBOutlet weak var centerTxtMsj: NSLayoutConstraint!
    @IBOutlet weak var centerRelatedService: NSLayoutConstraint!
    @IBOutlet weak var scrollServices: UIScrollView!
    @IBOutlet weak var DoDontTV: UITableView!
    @IBOutlet weak var useTV: UITableView!
    @IBOutlet weak var reminderTV: UITableView!
    @IBOutlet weak var heightViewServices: NSLayoutConstraint!
    @IBOutlet weak var heightDoDontTV: NSLayoutConstraint!
    @IBOutlet weak var heightUseTV: NSLayoutConstraint!
    @IBOutlet weak var heightReminderTV: NSLayoutConstraint!
    @IBOutlet weak var heightScrolleable: NSLayoutConstraint!
    @IBOutlet weak var heightCreateRecommendation: NSLayoutConstraint!
    
    var messages : [Message] = []
    var imgClient : UIImage?
    var chatUser: Chat?
    var idReceptor : String = ""
    var idChat : String = ""
    var lastService : OnService? = nil
    var reminders : [Reminder] = []
    var lastDate : String? = nil
    var reminder:Reminder?
    //var widthMax = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.activarOcultamientoTeclado()
        print("Tengo el id receptor :",chatUser?.para_user_id ?? idReceptor)
        self.setupViews()
        self.getLastService()
        self.getChatWithUser()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    @IBAction func backToTheInbox(_ btn: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "NewMessage"), object: nil, userInfo: nil)
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
     
        self.relatedServiceCV.dataSource = self
        self.relatedServiceCV.delegate = self
        self.relatedServiceCV.alwaysBounceHorizontal = true
        
        self.messageCV.dataSource = self
        self.messageCV.delegate = self
        self.messageCV.alwaysBounceVertical = true
        
        //let flow = self.relatedServiceCV.collectionViewLayout as! UICollectionViewFlowLayout
        //flow.itemSize = CGSize(width: , height: relatedServiceCV.bounds.height * 0.8)
        self.imgUser.image = imgClient
        self.imgUser.layer.cornerRadius = self.imgUser.bounds.midX
        self.imgUser.clipsToBounds = true
    
        let layerShadow = CALayer()
        layerShadow.shadowRadius = 8
        layerShadow.shadowOpacity = 0.25
        
        //self.imgUser.layer.mask = layerShadow
        
        self.btnRelatedBack.layer.cornerRadius = btnRelatedBack.bounds.midX
        self.btnRelatedClose.layer.cornerRadius = btnRelatedClose.bounds.midX
        
        self.messageTxt.delegate = self
     
        let sizeService = UIScreen.main.bounds.width * 0.05
        relatedServiceTitle.font = UIFont(name:"Futura-Bold", size: sizeService)
        
        let sizeCreate = UIScreen.main.bounds.width * 0.055
        createRecommendationTitle.font = UIFont(name:"Futura-Bold", size: sizeCreate)
        
        iconBoardView.image = iconBoardView.image!.withRenderingMode(.alwaysTemplate)
        
        if let tipo = AppDelegate.user?.tipo
        {
            if tipo == "profesional"
            {
                bottomRecommendation.constant = -createRecommendation.bounds.height
            }
            
            if tipo == "cliente"
            {
                heightCreateRecommendation.constant = -createRecommendation.bounds.height
            }
            
            createRecommendation.isHidden = tipo == "cliente"
        }
    
        centerRelatedService.constant = -view.bounds.width
        
        let tapRe = UITapGestureRecognizer(target: self, action: #selector(openRelatedService))
        createRecommendation.addGestureRecognizer(tapRe)
        
        heightViewServices.constant = 0
        heightUseTV.constant = 0
        heightDoDontTV.constant = 0
        heightReminderTV.constant = 0
    }
    
    func getLastService()
    {
        guard let usuario = AppDelegate.user,usuario.tipo == "profesional" else {return}
        
        inBiServices.instancia.listServices(token: usuario.access_token, id: usuario.id) { (respuesta) in
            if let allServices = respuesta as? [OnService]  {
                var clientServices = allServices.filter({return $0.cliente.id == self.chatUser?.chatcon.id ?? self.idReceptor})
                clientServices.sort(by: {return $0.updated_at > $1.updated_at})

                self.lastService = clientServices.first
                print("Obtenemos el ultimo servicio del cliente")
            }
            self.relatedServiceCV.reloadData()
        }
    }
    
    func getChatWithUser()
    {
        inBiServices.instancia.getChatOfUser(token: AppDelegate.user?.access_token ?? "", id: chatUser?.chatcon.id ?? self.idReceptor) { (respuesta) in
            if let messages = respuesta as? [Message],messages.count > 0
            {
                print("Capturamos los mensajes")
                self.messages = messages
                self.messageCV.reloadData()
                
                let myID = AppDelegate.user?.id ?? ""
                
                for (_,msj) in self.messages.enumerated()
                {
                    if myID != msj.de_user_id && msj.tipo_mensaje == "messageCell"
                    {
                        msj.tipo_mensaje = "receivedCell"
                    }
                }
                
                self.lastDate = self.messages.last?.created_at
                self.idChat = self.messages.last?.identificador ?? ""
                self.updatedChat()
                
                let index = IndexPath(item: messages.count - 1, section: 0)
                self.messageCV.scrollToItem(at: index, at: .bottom, animated: true)
            }
            
            //print("Comprobamos si hay un reminder disponible")
            self.checkIfReminderAvailable()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.lastDate = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func checkIfReminderAvailable()
    {
        if let reminder = self.reminder
        {
            print("Hay un reminder disponible para mandar")
            let newMessage = Message()
            newMessage.tipo_mensaje = "reminderCell"
            print(reminder)
            let tiempoRestante = "In \(reminder.tiempo)"
            newMessage.mensaje = "\(reminder.mensaje);\(tiempoRestante)"
            newMessage.de_user_id = AppDelegate.user?.id ?? ""
            newMessage.para_user_id = chatUser?.chatcon.id ?? idReceptor
            
            self.messages.append(newMessage)
            
            let index = IndexPath(item: messages.count - 1, section: 0)
            messageCV.insertItems(at:[index])
            messageCV.scrollToItem(at: index, at: .bottom, animated: true)
       
            if let lastMessage = self.messages.last
            {
                self.sendMessage(message: lastMessage)
            }
            
            self.reminder = nil
        }
    }
    
    func updatedChat()
    {
        if let newLastDate = self.lastDate
        {
            inBiServices.instancia.updatedChatWithUser(token: AppDelegate.user?.access_token ?? "", id: chatUser?.chatcon.id ?? self.idReceptor, lastDate: newLastDate) { (respuesta) in
                if let newMessages = respuesta as? [Message],newMessages.count > 0
                {
                    let myID = AppDelegate.user?.id ?? ""
                    
                    for (_,msj) in newMessages.enumerated()
                    {
                        if myID != msj.de_user_id && msj.tipo_mensaje == "messageCell"
                        {
                            msj.tipo_mensaje = "receivedCell"
                        }
                    }
                    
                    self.messages.append(contentsOf: newMessages)
                    self.messageCV.reloadData()
                    
                    let index = IndexPath(item: self.messages.count - 1, section: 0)
                    self.messageCV.scrollToItem(at: index, at: .bottom, animated: true)
                    
                    self.lastDate = self.messages.last?.created_at
                }
                
                Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                    self.updatedChat()
                })
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let txt = textField.text,txt.count > 0
        {
            print("El txt es",textField.text!)
            
            var espacioEnBlanco = true
            var txtFiltro = ""
            
            txt.forEach { (letra) in
                if letra != " " || !espacioEnBlanco
                {
                    espacioEnBlanco = false
                    txtFiltro.append(letra)
                }
            }
            
            if txtFiltro.count > 0
            {
                print("Reducido el txt es",txtFiltro)
                let newMessage = Message()
                newMessage.mensaje = txtFiltro
                newMessage.tipo_mensaje = "messageCell"
                newMessage.de_user_id = AppDelegate.user?.id ?? ""
                newMessage.para_user_id = chatUser?.para_user_id ?? self.idReceptor
                messages.append(newMessage)

                let index = IndexPath(item: messages.count - 1, section: 0)
                messageCV.insertItems(at:[index])
                messageCV.scrollToItem(at: index, at: .bottom, animated: true)

                if let lastMessage = self.messages.last
                {
                    self.sendMessage(message: lastMessage)
                }
            }
            
            textField.text = nil
        }

        return textField.resignFirstResponder()
    }
    
    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = ntf.name == UIResponder.keyboardWillShowNotification
        
        //self.view.frame.origin.y = (isShowNtf) ? -keyboardRect.height : 0
        let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomViewMsj"})!
        bottomMessage.constant = (isShowNtf) ? -keyboardRect.height : 0
        
        UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            
            if isShowNtf && self.messages.count > 0
            {
                let lastItem = self.messages.endIndex - 1
                let index = IndexPath(item: lastItem, section: 0)
                self.messageCV.scrollToItem(at: index, at: .bottom, animated: true)
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if let tipo = AppDelegate.user?.tipo,tipo == "profesional"
        {
            UIView.animate(withDuration: 0.2)
            {
                self.bottomRecommendation.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if let tipo = AppDelegate.user?.tipo,tipo == "profesional"
        {
            UIView.animate(withDuration: 0.2)
            {
                self.bottomRecommendation.constant = -self.createRecommendation.bounds.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func openCreateRecommendation(_ btn: UIButton)
    {
        let constant = (bottomRecommendation.constant == 0) ? -createRecommendation.bounds.height : 0
        
        UIView.animate(withDuration: 0.2) {
             self.bottomRecommendation.constant = constant
             self.view.layoutIfNeeded()
        }
    }

    @IBAction func backRelatedService(_ btn: UIButton)
    {
        UIView.animate(withDuration: 0.2) {
            self.centerTxtMsj.constant = 0
            self.centerRelatedService.constant = self.view.bounds.width
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func openRelatedService()
    {
        self.messageTxt.resignFirstResponder()
        UIView.animate(withDuration: 0.2) {
            self.centerTxtMsj.constant = -self.view.bounds.width
            self.centerRelatedService.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: MESSAGE ,SERVICE,REMINDER CV
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case messageCV:
            return messages.count
        case relatedServiceCV:
            return (lastService == nil) ? 1 : 2
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case relatedServiceCV:
             return CGSize(width:view.bounds.width * 0.65,height:collectionView.bounds.height * 0.8)
        default:
            switch messages[indexPath.item].tipo_mensaje
            {
            case "receivedCell","messageCell":
                let messageStr = messages[indexPath.item].mensaje ?? ""
                let size = CGSize(width: 250, height: 10000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: messageStr).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font:UIFont(name: "Futura-Medium", size: 16)!], context: nil)
                return CGSize(width: view.bounds.width, height: estimatedFrame.height + 20)
            case "dodontCell":
                return CGSize(width:view.bounds.width,height:75)
            case "useCell":
                return CGSize(width:view.bounds.width,height:120)
            default:
                return CGSize(width:view.bounds.width,height:100)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case messageCV:
            
            let message = messages[indexPath.item]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: message.tipo_mensaje, for: indexPath)
         
            switch message.tipo_mensaje
            {
            case "messageCell","receivedCell":
                let txt = message.mensaje ?? ""
                let myId = AppDelegate.user?.id ?? ""
                let myMessage = myId == message.de_user_id
                insertMessage(cell: cell, item: indexPath.item, txt: txt, myMessage: myMessage)
            case "dodontCell":
                //print("dodont")
                let bodyDoDont = message.mensaje?.components(separatedBy: ";") ?? [String]()
                
                var mensaje = bodyDoDont.last ?? ""
                
                if bodyDoDont.count > 2
                {
                    let msjs = bodyDoDont[1..<bodyDoDont.count]
                    mensaje = msjs.reduce("") { (result, msj) -> String in
                        return result + msj + ";"
                    }
                    _ = mensaje.popLast()
                }
                
                let doDont = DoDont(id: "dodontId", tipo: bodyDoDont.first ?? "", tarea: mensaje)
                insertDoDont(cell: cell, item:  indexPath.item, doDont: doDont)
            case "useCell":
                //print("dodont")
                let bodyUse = message.mensaje?.components(separatedBy: ";") ?? [String]()
                let producto = Producto(id: Int(bodyUse[2]) ?? -1, categoria_id: -1, nombre: bodyUse[1], medida: "-", numero: "-", descripcion: "-", url_foto: bodyUse.last ?? "", cantidad: nil, categoria: nil, nombreCategoria: nil)
                let use = Use(id: "useId", producto_id: producto.id, cantidad: 0, medida: "-", frecuencia: bodyUse.first ?? "", producto: producto)
                inserUseRecommendation(cell: cell, item: indexPath.item, useRecommend: use)
            case "reminderCell":
                //print("reminder")
                let bodyReminder = message.mensaje?.components(separatedBy: ";") ?? [String]()
                
                var mensaje = bodyReminder.first ?? ""
                
                if bodyReminder.count > 2
                {
                    let msjs = bodyReminder[0..<bodyReminder.count - 1]
                    mensaje = msjs.reduce("") { (result, msj) -> String in
                        return result + msj + ";"
                    }
                    _ = mensaje.popLast()
                }
            
                let reminder = Reminder(mensaje: mensaje, numero: nil, tiempo: "tiempo", numero_restante: -1, tiempo_restante: bodyReminder.last ?? "")
                inserReminder(cell: cell, item: indexPath.item, reminder: reminder)
            default:
                break
            }
            return cell
        case relatedServiceCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellService", for: indexPath)
            setupRelatedServices(cell: cell, item: indexPath.item)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func insertMessage(cell:UICollectionViewCell,item:Int,txt:String,myMessage:Bool)
    {
        guard let txtBubbleView = cell.viewWithTag(100),let txtMessage = cell.viewWithTag(101) as? UITextView
        else {
            //print("no podemos configurar la celda del texto")
            return
        }
    
        //print("configuramos la celda del texto")
        txtMessage.text = txt
        
        let size = CGSize(width: 250, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: txt).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font:UIFont(name: "Futura-Medium", size: 16)!], context: nil)
 
        if let widthBubbleView = txtBubbleView.constraints.first(where: {return $0.identifier == "widthBubbleView"})
        {
            widthBubbleView.constant = (txt.count == 1) ? 41.25 : estimatedFrame.width + 25
        }
      
        txtBubbleView.layer.masksToBounds = true
        txtBubbleView.layer.cornerRadius = 20.625
        
        txtBubbleView.backgroundColor = (myMessage) ? #colorLiteral(red: 0.2507906258, green: 0.246149838, blue: 0.3331091106, alpha: 1) : UIColor(white: 0.95, alpha: 1)
        txtMessage.textColor = (myMessage) ? UIColor.white : UIColor.black
    }
    
    func insertDoDont(cell:UICollectionViewCell,item:Int,doDont:DoDont)
    {
        let viewBG = cell.viewWithTag(100)!
        let doDontLabel = cell.viewWithTag(101) as! UILabel
        let task = cell.viewWithTag(102) as! UILabel
        
        var tipo = doDont.tipo
        tipo.removeFirst()
        tipo.insert("D", at: tipo.startIndex)
        doDontLabel.text = tipo
        
        viewBG.backgroundColor = (tipo == "Do") ? #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1) : #colorLiteral(red: 0.2232428193, green: 0.2219871581, blue: 0.2965724468, alpha: 1)
        doDontLabel.layer.cornerRadius = doDontLabel.bounds.midX
        doDontLabel.layer.masksToBounds = true
        doDontLabel.adjustsFontSizeToFitWidth = true
        
        task.text = doDont.tarea
    }
    
    func inserUseRecommendation(cell:UICollectionViewCell,item:Int,useRecommend:Use)
    {
        let use = cell.viewWithTag(101) as! UILabel
        let frecuency = cell.viewWithTag(102) as! UILabel
        let name = cell.viewWithTag(103) as! UILabel
        let imgView = cell.viewWithTag(104) as! UIImageView
        
        imgView.layer.cornerRadius = imgView.bounds.midX * 0.25
        
        use.layer.cornerRadius = use.bounds.midX
        use.text = "Use"
        
        imgView.cargarImgDesdeURL(urlImg: useRecommend.producto.url_foto) {
        }
        
        frecuency.text = useRecommend.frecuencia
        name.text = useRecommend.producto.nombre
    }
    
    func inserReminder(cell:UICollectionViewCell,item:Int,reminder:Reminder)
    {
        let imgView = cell.viewWithTag(101) as! UIImageView
        imgView.layer.cornerRadius = imgView.bounds.midX
    
        let user = AppDelegate.user ?? User()
        
        if user.tipo == "cliente"
        {
            imgView.cargarImgDesdeURL(urlImg: user.foto ?? "") {
            }
        }else
        {
            imgView.image = imgUser.image
        }
        
        let msj = cell.viewWithTag(102) as! UILabel
        msj.text = reminder.mensaje
        
        let time = cell.viewWithTag(104) as! UILabel
        time.adjustsFontSizeToFitWidth = true
        
        let timeRest = reminder.tiempo_restante
        time.text = timeRest.uppercased()
    }
    
    func setupRelatedServices(cell:UICollectionViewCell,item:Int)
    {
        cell.layer.cornerRadius = cell.bounds.midY / 2.25
        cell.layer.borderWidth = (item == 0 && lastService != nil) ? 1 : 0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.layer.shadowRadius = 5
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = CGSize(width: 3, height: 2)
        
        cell.backgroundColor = (item == 0 && lastService != nil) ? UIColor.white : #colorLiteral(red: 0.7742605805, green: 0.7712902427, blue: 0.7707846761, alpha: 1)
        
        let viewService = cell.viewWithTag(50)!
        viewService.isHidden = item == 1 || lastService == nil
        
        if let service = self.lastService
        {
            let imgView = cell.viewWithTag(100) as! UIImageView
            imgView.layer.cornerRadius = imgView.bounds.midX

            imgView.layer.masksToBounds = true
            imgView.cargarImgDesdeURL(urlImg: service.categoria.icon_url ?? "") {
                UIView.animate(withDuration: 0.25, animations: {
                    imgView.alpha = 1
                })
            }
            
            let nameService = cell.viewWithTag(101) as! UILabel
            nameService.text = service.author_tag
            
            let name = cell.viewWithTag(102) as! UILabel
            name.text = self.chatUser?.chatcon.name
            
            let date = cell.viewWithTag(103) as! UILabel
            date.text = service.fecha_recordatorio
        }
        
        let followUp = cell.viewWithTag(60)!
        let noService = cell.viewWithTag(70)!
        followUp.isHidden = item == 0 && lastService != nil
        noService.isHidden = item == 0 && lastService != nil
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == relatedServiceCV
        {
            guard let _ = self.lastService else {
                self.performSegue(withIdentifier: "goFollowUp", sender: self.lastService)
                return
            }
            
            var identifier = ""
            
            switch indexPath.row {
            case 0:
                identifier = "goRecommendation"
            case 1:
                identifier = "goFollowUp"
            default:
                break
            }
            
            self.performSegue(withIdentifier: identifier, sender: self.lastService)
        }
    }
    
    func sendMessage(message:Message)
    {
        print("El identificador es \(self.idChat)")
        
        inBiServices.instancia.sendMessageChat(token: AppDelegate.user?.access_token ?? "", id: chatUser?.chatcon.id ?? self.idReceptor, tipo: message.tipo_mensaje, mensaje: message.mensaje ?? "", identificador: self.idChat) { (respuesta) in
            
            if let body = respuesta as? [String:Any],let lastDate = body["updated_at"] as? String
            {
                self.idChat = body["identificador"] as? String ?? ""
                if self.lastDate == nil
                {
                    self.lastDate = lastDate
                    self.updatedChat()
                }else
                {
                    self.lastDate = lastDate
                }
                print("Se pudo enviar el mensaje y obtenemos la fecha de ese mensaje",lastDate)
                print("El identificador es \(self.idChat)")
            }else
            {
                print("No se pudo enviar el mensaje")
            }
        }
    }   
    
    @IBAction func addRecommend(_ segue:UIStoryboardSegue)
    {
        self.backRelatedService(btnRelatedBack)
        self.messageCV.reloadData()
        
        let index = IndexPath(item: self.messages.count - 1, section: 0)
        self.messageCV.scrollToItem(at: index, at: .bottom, animated: true)
        
        if let lastMessage = self.messages.last
        {
            self.sendMessage(message: lastMessage)
        }
        
        guard let update = try? JSONEncoder.init().encode(self.lastService) else {
            return
        }
        
        inBiServices.instancia.updateCustomersService(id: self.lastService?.id ?? "", token: AppDelegate.user?.access_token ?? "", update: update) { (respuesta) in
            
            if let status = respuesta as? String,status == "success"
            {
                BoardVC.sendRecommendInbox = true
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goRecommendation"
        {
            let newRecoVC = segue.destination as! NewRecommendationVC
            newRecoVC.addReco = .inbox
        }
        
        if segue.identifier == "goFollowUp"
        {
            let followUpVC = segue.destination as! DueOnVC
            followUpVC.imgClient = self.imgUser.image
            followUpVC.nameClient = self.chatUser?.chatcon.name ?? ""
            followUpVC.addReco = .inbox
        }
    }
}

//func instanciarBtnMsjCell()
//{
//    let msj = messages[indexPath.row]
//    cell.addSubview(msj)
//    let msjCell = cell.viewWithTag(100) as! UIButton
//
//
//     if msjCell.constraints.isEmpty
//     {
//     msjCell.clipsToBounds = true
//     msjCell.translatesAutoresizingMaskIntoConstraints = false
//
//     msjCell.centerYAnchor.constraint(equalToSystemSpacingBelow: cell.centerYAnchor, multiplier: 1.0).isActive = true
//     msjCell.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: -10).isActive = true
//
//     msjCell.heightAnchor.constraint(equalTo: cell.heightAnchor, multiplier: 0.8).isActive = true
//     msjCell.layer.cornerRadius = msjCell.bounds.midY
//
//     if widthMax
//     {
//     print("Nos pasamos de la anchura maxima")
//     msjCell.titleLabel!.numberOfLines = 0
//     msjCell.titleEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//     msjCell.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75).isActive = true
//     }else
//     {
//     let newWidth = msjCell.bounds.width + (msjCell.bounds.height * 0.75)
//     msjCell.widthAnchor.constraint(equalToConstant: newWidth).isActive = true
//     }
//     }
//
//    print("Tamaño actual del msj \"\(msjCell.titleLabel!.text!)\" es \(msjCell.bounds.size)")
//}

//func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    switch tableView {
//    case messageTV:
//        return messages.count
//    case DoDontTV:
//        return lastService?.recomendaciones?.dodont.count ?? 0
//    case useTV:
//        return lastService?.recomendaciones?.use.count ?? 0
//    case reminderTV:
//        return self.reminders.count
//    default:
//        return 0
//    }
//}
//
//func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//    if tableView == messageTV
//    {
//        return "Messages"
//    }else
//    {
//        return nil
//    }
//}
//
//func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//    guard !messages.isEmpty else {
//        return 0
//    }
//
//    let msj = messages[indexPath.row]
//
//    let countLetters = CGFloat(msj.text!.count)
//    let newHeight : CGFloat = countLetters
//
//    let heightMsj = (newHeight > 45) ? newHeight * 1.25 : 45
//
//    if heightMsj > 45
//    {
//        print("Tamaño de la altura del msj \"\(msj.text!)\" despues de modificarlo",heightMsj)
//    }
//
//    return heightMsj
//}
//
//func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//    switch tableView {
//    case messageTV:
//        let cell = tableView.dequeueReusableCell(withIdentifier: "msjCell", for: indexPath)
//
//        let msj = messages[indexPath.row]
//
//        let viewMsj = cell.viewWithTag(100)!
//
//        let widthMax = msj.bounds.width + (msj.bounds.height * 1.5) > self.view.bounds.width * 0.7
//
//        msj.textAlignment = (widthMax) ? .left : .center
//
//        if !widthMax
//        {
//            guard let widthMaxConst = cell.constraints.first(where: {$0.identifier == "widthMax"}) else {return cell}
//            widthMaxConst.isActive = false
//
//            viewMsj.widthAnchor.constraint(equalToConstant: msj.bounds.width + (msj.bounds.height * 1.5)).isActive = true
//        }
//
//        if viewMsj.layer.cornerRadius == 0
//        {
//            viewMsj.layer.cornerRadius = viewMsj.bounds.midY
//        }
//
//        viewMsj.addSubview(msj)
//        instanciarMsj(bgMsj: viewMsj)
//
//        print("Tamaño del viewMsj es \(viewMsj.bounds.size)")
//        return cell
//    case DoDontTV:
//        let cell = tableView.dequeueReusableCell(withIdentifier: "recommendationCell", for: indexPath)
//
//        if let doDont = self.lastService?.recomendaciones?.dodont
//        {
//            let viewBG = cell.viewWithTag(100)!
//            let doDontLabel = cell.viewWithTag(101) as! UILabel
//            let task = cell.viewWithTag(102) as! UILabel
//
//            var tipo = doDont[indexPath.row].tipo
//            tipo.removeFirst()
//            tipo.insert("D", at: tipo.startIndex)
//            doDontLabel.text = tipo
//
//            viewBG.backgroundColor = (tipo == "Do") ? #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1) : #colorLiteral(red: 0.2232428193, green: 0.2219871581, blue: 0.2965724468, alpha: 1)
//            doDontLabel.clipsToBounds = true
//            doDontLabel.layer.cornerRadius = doDontLabel.bounds.midX
//            doDontLabel.adjustsFontSizeToFitWidth = true
//
//            task.text = doDont[indexPath.row].tarea
//        }
//
//        return cell
//    case useTV:
//        let cell = tableView.dequeueReusableCell(withIdentifier: "useCell", for: indexPath)
//
//        if let uses = self.lastService?.recomendaciones?.use
//        {
//            let use = cell.viewWithTag(101) as! UILabel
//            let frecuency = cell.viewWithTag(102) as! UILabel
//            let name = cell.viewWithTag(103) as! UILabel
//            let imgView = cell.viewWithTag(104) as! UIImageView
//
//            imgView.layer.cornerRadius = imgView.bounds.midX * 0.25
//
//            use.layer.cornerRadius = use.bounds.midX
//            use.text = "Use"
//
//            imgView.cargarImgDesdeURL(urlImg: uses[indexPath.row].producto.url_foto) {
//            }
//
//            frecuency.text = uses[indexPath.row].frecuencia
//            name.text = uses[indexPath.row].producto.nombre
//        }
//
//        return cell
//    case reminderTV:
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath)
//
//        let reminder = self.reminders[indexPath.row]
//
//        let imgView = cell.viewWithTag(101) as! UIImageView
//        imgView.layer.cornerRadius = imgView.bounds.midX
//
//        let msj = cell.viewWithTag(102) as! UILabel
//        msj.text = reminder.mensaje
//
//        let time = cell.viewWithTag(104) as! UILabel
//        time.adjustsFontSizeToFitWidth = true
//
//        let timeRest = reminder.tiempo_restante
//
//        if timeRest == "dias" || timeRest == "semanas" || timeRest == "meses"
//        {
//            let timeStr = "In \(reminder.tiempo)"
//            time.text = timeStr.uppercased()
//        }else
//        {
//            time.text = timeRest.uppercased()
//        }
//
//        return cell
//    default:
//        return UITableViewCell()
//    }
//}

/*func activateTViews(tb:UITableView,delegate:Bool)
 {
 tb.dataSource = self
 
 if delegate
 {
 tb.delegate = self
 }
 }
 
 activateTViews(tb: messageTV, delegate: true)
 activateTViews(tb: DoDontTV, delegate: false)
 activateTViews(tb: useTV, delegate: false)
 activateTViews(tb: reminderTV, delegate: false)*/
//func instanciarMsj(bgMsj:UIView)
//{
//    let msj = bgMsj.subviews.first as! UILabel
//
//    msj.translatesAutoresizingMaskIntoConstraints = false
//
//    msj.centerXAnchor.constraint(equalTo: bgMsj.centerXAnchor).isActive = true
//    msj.centerYAnchor.constraint(equalTo: bgMsj.centerYAnchor).isActive = true
//
//    msj.topAnchor.constraint(equalTo: bgMsj.topAnchor, constant: 7).isActive = true
//    msj.leadingAnchor.constraint(equalTo: bgMsj.leadingAnchor, constant: 7).isActive = true
//    msj.bottomAnchor.constraint(equalTo: bgMsj.bottomAnchor, constant: -7).isActive = true
//    msj.trailingAnchor.constraint(equalTo: bgMsj.trailingAnchor, constant: -7).isActive = true
//}

//        switch segue.source {
//        case is DoRecommendationVC:
//            print("DoRecom")
//            heightDoDontTV.constant = CGFloat(75 * (lastService?.recomendaciones?.dodont.count ?? 0))
//            self.DoDontTV.reloadData()
//        case is UseDaysVC:
//            print("user days")
//            heightUseTV.constant =  CGFloat(120 * (lastService?.recomendaciones?.use.count ?? 0))
//            self.useTV.reloadData()
//        case is DueOnVC:
//            print("Due on")
//            heightReminderTV.constant = CGFloat(100 * (lastService?.reminder.count ?? 0))
//            self.reminderTV.reloadData()
//        default:
//            break
//        }
//
//        let constantViewService = heightReminderTV.constant + heightDoDontTV.constant + heightUseTV.constant
//
//        let floatConstant = simd_clamp(Float(constantViewService), 0, 295)
//        heightViewServices.constant = CGFloat(floatConstant)
//        if let trailingBubble = cell.contentView.constraints.first(where: {return $0.identifier == "trailingBubble"}), let leadingBubble = cell.contentView.constraints.first(where: {return $0.identifier == "leadingBubble"})
//        {
//            //let centerTxt = cell.contentView.constraints.first(where: {return $0.identifier == "centerMessage"})!
//
//
//            trailingBubble.isActive = myMessage
//            leadingBubble.isActive = !myMessage //? .required : .defaultLow
//        }
