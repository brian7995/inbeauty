//
//  MenuInbi.swift
//  InBeauty
//
//  Created by ADMIN on 1/08/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class MenuInbi: UITabBarController {

     let user = AppDelegate.user ?? User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let createVC = UIStoryboard(name: "Create", bundle: nil).instantiateInitialViewController()
        createVC?.tabBarItem.image = #imageLiteral(resourceName: "plus")
        let profileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateInitialViewController()
        profileVC?.tabBarItem.image = #imageLiteral(resourceName: "profile_Icon")
        let boardVC = UIStoryboard(name: "Board", bundle: nil).instantiateInitialViewController()
        boardVC?.tabBarItem.image = #imageLiteral(resourceName: "board_Icon")
        let discoveryVC = UIStoryboard(name: "Discovery", bundle: nil).instantiateInitialViewController()
        discoveryVC?.tabBarItem.image = #imageLiteral(resourceName: "discovery_Icon")
        let inboxVC = UIStoryboard(name: "Inbox", bundle: nil).instantiateInitialViewController()
        inboxVC?.tabBarItem.image = #imageLiteral(resourceName: "msj_Icon")
        
        if UIApplication.shared.applicationIconBadgeNumber > 0
        {
            inboxVC?.tabBarItem.badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        }
        
        switch user.tipo {
        case "cliente":
            print("cliente")
            self.viewControllers = [profileVC,boardVC,inboxVC,discoveryVC] as? [UIViewController]
        case "profesional":
            print("profesional")
            self.viewControllers = [boardVC,profileVC,createVC,inboxVC,discoveryVC] as? [UIViewController]
            if user.miscategorias.isEmpty || user.phone == "000000000"
            {
                self.selectedIndex = 1
            }
        default:
            break
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(newMessage(ntf:)), name: Notification.Name(rawValue: "NewMessage"), object: nil)
    }
    
    @objc func newMessage(ntf:Notification)
    {
        guard let userInfo = ntf.userInfo else {return}
        
        let aps = userInfo["aps"] as? [String:Any] ?? [:]
        let badge = aps["badge"] as? Int ?? 0
        let isClient = user.tipo == "cliente" ? 2 : 3
        
        //print(UIApplication.shared.applicationIconBadgeNumber)
        self.viewControllers?[isClient].tabBarItem.badgeValue = String(badge)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
