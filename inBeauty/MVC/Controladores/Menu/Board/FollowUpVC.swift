//
//  FollowUpVC.swift
//  InBeauty
//
//  Created by ADMIN on 25/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FollowUpVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CustomerService,ReSendFollowUp {
   
    @IBOutlet weak var followUpTV: UITableView!
    @IBOutlet weak var activityFollow: UIActivityIndicatorView!
    
    let user = AppDelegate.user ?? User()
    var isClient = false
    
    var followUps : [OnService] = []
    var refreshBoard : RefreshBoard?
    
    var recomendationsClient : [RecomendationClient] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        followUpTV.delegate = self
        
        self.isClient = user.tipo == "cliente"
        
        let refreshControl = UIRefreshControl(frame: followUpTV.frame)
        refreshControl.addTarget(self, action: #selector(callRefreshBoard), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.000754669134, green: 0.9009926915, blue: 0.7528850436, alpha: 1)
        
        followUpTV.refreshControl = refreshControl
    }
    
    func startActivityIndicator()
    {
        recomendationsClient.removeAll()
        followUps.removeAll()
        followUpTV.reloadData()
        followUpTV.refreshControl?.beginRefreshing()
        //activityFollow.startAnimating()
    }
    
    @objc func callRefreshBoard()
    {
        refreshBoard?.reloadBoard()
    }
    
    func getCustomerReminders(reminders: [OnService]) {
        print("Customer services follow up")
        self.followUps = reminders
        self.followUps.sort(by: {return $0.updated_at > $1.updated_at})
        self.setupRecomendationsClient()
    }
    
    func setupRecomendationsClient()
    {
        if self.isClient
        {
            //print("Soy cliente")
            self.followUps.forEach { (service) in
                
                service.reminder.forEach({ (reminder) in
                    let reminderClient = RecomendationClient(type: "followUpCell", recomendation: reminder)
                    self.recomendationsClient.append(reminderClient)
                })
                
                if let recomendation = service.recomendaciones
                {
                    recomendation.dodont.forEach({ (doDont) in
                        let doDontClient = RecomendationClient(type: "dodontCell", recomendation: doDont)
                        self.recomendationsClient.append(doDontClient)
                    })
                    
                    recomendation.use.forEach({ (use) in
                        let useClient = RecomendationClient(type: "useCell", recomendation: use)
                        self.recomendationsClient.append(useClient)
                    })
                }
            }
            
            inBiServices.instancia.reminderInbox(token: user.access_token) { (respuesta) in
                
                if var remindersInbox = respuesta as? [Reminder],remindersInbox.count > 0
                {
                    print("obtenemos los reminder del inbox")
                    for (indice,reminder) in remindersInbox.enumerated()
                    {
                        if reminder.mensaje.contains(";")
                        {
                            let bodyReminder = remindersInbox[indice].mensaje.components(separatedBy: ";")
                            
                            var mensaje = bodyReminder.first ?? ""
                            
                            if bodyReminder.count > 2
                            {
                                let msjs = bodyReminder[0..<bodyReminder.count - 1]
                                mensaje = msjs.reduce("") { (result, msj) -> String in
                                    return result + msj + ";"
                                }
                                _ = mensaje.popLast()
                            }
                            
                            remindersInbox[indice].mensaje = mensaje
                        }
                        
                        let reminderClient = RecomendationClient(type: "followUpCell", recomendation: remindersInbox[indice])
                        self.recomendationsClient.insert(reminderClient, at: 0)
                    }
                }
                
                self.activityFollow.stopAnimating()
                self.followUpTV.refreshControl?.endRefreshing()
                self.followUpTV.reloadData()
            }
            
        }else
        {
            activityFollow.stopAnimating()
            self.followUpTV.refreshControl?.endRefreshing()
            self.followUpTV.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.isClient) ? 1 : self.followUps.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (self.isClient) ? self.recomendationsClient.count : self.followUps[section].reminder.count
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isClient
        {
            let recomendationClient = self.recomendationsClient[indexPath.row]
            switch recomendationClient.type
            {
            case "followUpCell":
                //print("reminder")
                return 100
            case "dodontCell":
                //print("dodont")
                return 85
            case "useCell":
                //print("dodont")
                return 130
            default:
                break
            }
        }
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isClient
        {
            let recomendationClient = self.recomendationsClient[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: recomendationClient.type, for: indexPath)
            
            switch recomendationClient.type
            {
            case "followUpCell":
                //print("reminder")
                let reminder = recomendationClient.recomendation as! Reminder
                showReminder(cell: cell, reminder: reminder)
            case "dodontCell":
                //print("dodont")
                let doDont = recomendationClient.recomendation as! DoDont
                showDoDont(cell: cell, doDont: doDont)
            case "useCell":
                //print("dodont")
                let use = recomendationClient.recomendation as! Use
                showUse(cell: cell, use: use)
            default:
                break
            }
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "followUpCell", for: indexPath) as! FollowUpTVCell
            let follow = self.followUps[indexPath.section]
            let reminder = follow.reminder[indexPath.row]
            cell.reSendFollowUp = self
            cell.buildCell(follow: follow, reminder: reminder)
            return cell
        }
    }
    
    func showReminder(cell:UITableViewCell,reminder:Reminder)
    {
        let viewFollow = cell.viewWithTag(10)!
        viewFollow.backgroundColor = reminder.numero_restante == 0 ? UIColor.lightGray : #colorLiteral(red: 0.001304339617, green: 0.8347914815, blue: 0.7023031712, alpha: 1)
        
        let imgView = cell.viewWithTag(101) as! UIImageView
        imgView.layer.cornerRadius = imgView.bounds.midX
        
        imgView.cargarImgDesdeURL(urlImg: user.foto ?? "") {
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
        }
    
        let name = cell.viewWithTag(103) as! UILabel
        name.text = user.name
        
        let msj = cell.viewWithTag(102) as! UILabel
        msj.text = reminder.mensaje
        
        let time = cell.viewWithTag(104) as! UILabel
        time.adjustsFontSizeToFitWidth = true
        
        //let timeRest = reminder.tiempo_restante
        let timeStr = "IN" + " " + reminder.tiempo.uppercased()
        time.text = timeStr.uppercased()
        
//        if timeRest == "dias" || timeRest == "semanas" || timeRest == "meses"
//        {
//
//        }else
//        {
//            time.text = timeRest.uppercased()
//        }
    }
    
    func showDoDont(cell:UITableViewCell,doDont:DoDont)
    {
        let viewBG = cell.viewWithTag(100)!
        let doDontLabel = cell.viewWithTag(101) as! UILabel
        let task = cell.viewWithTag(102) as! UILabel
        
        var tipo = doDont.tipo
        tipo.removeFirst()
        tipo.insert("D", at: tipo.startIndex)
        doDontLabel.text = tipo
        
        viewBG.backgroundColor = (tipo == "Do") ? #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1) : #colorLiteral(red: 0.2232428193, green: 0.2219871581, blue: 0.2965724468, alpha: 1)
        doDontLabel.layer.cornerRadius = doDontLabel.bounds.midX
        doDontLabel.layer.masksToBounds = true
        doDontLabel.adjustsFontSizeToFitWidth = true
        
        task.text = doDont.tarea
    }
    
    func showUse(cell:UITableViewCell,use:Use)
    {
        let useLabel = cell.viewWithTag(101) as! UILabel
        let frecuency = cell.viewWithTag(102) as! UILabel
        let name = cell.viewWithTag(103) as! UILabel
        let imgView = cell.viewWithTag(104) as! UIImageView
        
        imgView.layer.cornerRadius = imgView.bounds.midX * 0.25
        
        useLabel.layer.cornerRadius = useLabel.bounds.midX
        useLabel.text = "Use"
        
        imgView.cargarImgDesdeURL(urlImg: use.producto.url_foto) {
        }
        
        frecuency.text = use.frecuencia
        name.text = use.producto.nombre
    }
    
    func reSendFollowUp(idClient: String,imgClient:UIImage?, reminder: Reminder) {
        let list : [Any] = [idClient,imgClient as Any,reminder]
        self.performSegue(withIdentifier: "goInbox", sender: list)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let followUp = self.followUps[indexPath.section]
//        let cell = tableView.cellForRow(at: indexPath) as! FollowUpTVCell
//        let list : [Any] = [followUp,cell.imgViewClient.image ?? UIImage()]
//        self.performSegue(withIdentifier: "goServiceName", sender: list)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceName"
        {
            let serviceVC = segue.destination as! ServiceNameVC
            let list = sender as! [Any]
            serviceVC.service = list.first as? OnService
            serviceVC.imgClient = list.last as? UIImage
        }
        
        if segue.identifier == "goInbox"
        {
            let messageVC = segue.destination as! MessagesVC
            let list = sender as! [Any]
            messageVC.idReceptor = list.first as! String
            messageVC.imgClient = list[1] as? UIImage
            messageVC.reminder = list.last as? Reminder
        }
    }
}
