//
//  OnServiceVC.swift
//  InBeauty
//
//  Created by ADMIN on 25/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class OnServiceVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CustomerService {
  
    @IBOutlet weak var serviceTV: UITableView!
    @IBOutlet weak var activityServices: UIActivityIndicatorView!
    
    var refreshBoard: RefreshBoard?
    var services : [OnService] = []
    
    var serviceOriginal : OnService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceTV.dataSource = self
        serviceTV.delegate = self
        self.activityServices.startAnimating()
        
        let refreshControl = UIRefreshControl(frame: serviceTV.frame)
        refreshControl.addTarget(self, action: #selector(callRefreshBoard), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.9720008969, green: 0.8148944974, blue: 0.1307675838, alpha: 1)
        
        serviceTV.refreshControl = refreshControl
    }
    
    @objc func callRefreshBoard()
    {
        refreshBoard?.reloadBoard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //print("service will appear")
        //self.services = BoardVC.boardInstance.services
        //self.serviceTV.reloadData()
    }

    func startActivityIndicator() {
        serviceTV.refreshControl?.beginRefreshing()
        //self.activityServices.startAnimating()
    }
    
    func getCustomerService(services: [OnService]) {
        //print("Customer services vc")
        self.services = services
        self.services.sort(by: {return $0.updated_at > $1.updated_at})
        self.activityServices.stopAnimating()
        self.serviceTV.refreshControl?.endRefreshing()
        self.serviceTV.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! OnServiceTVCell
        //let imgView = cell.viewWithTag(100) as! UIImageView
        
        //imgView.image = UIImage(named: "profilePicture\((services.count - 1) - indexPath.row)") ?? #imageLiteral(resourceName: "profilePicture4")
        //(cell.viewWithTag(101) as! UIImageView).image = UIImage(named: "profilePicture\(indexPath.row)")
        
        let service = services[indexPath.row]
        cell.buildCell(service: service)
        
//        let name = cell.viewWithTag(102) as! UILabel
//        name.text = service.cliente.name
//        
//        let fecha = cell.viewWithTag(101) as! UILabel
//        fecha.text = service.fecha_recordatorio
//        
//        let categoryTxt = cell.viewWithTag(103) as! UILabel
//        let category = AppDelegate.user?.miscategorias.first(where: {return $0.id == service.categoria_id})
//        categoryTxt.text = category?.nombre ?? "-"
//        
//        let authorTag = cell.viewWithTag(105) as! UILabel
//        authorTag.text = service.author_tag
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Go Service  Name")
        let cell = tableView.cellForRow(at: indexPath) as! OnServiceTVCell
        let service = self.services[indexPath.row]
        let list : [Any] = [service,cell.imgViewClient.image ?? UIImage()]
        self.performSegue(withIdentifier: "goServiceName", sender: list)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceName"
        {
            let serviceVC = segue.destination as! ServiceNameVC
          
            let list = sender as! [Any]
            serviceVC.service = list.first as? OnService
            serviceVC.imgClient = list.last as? UIImage
        }
    }
    

}
