//
//  ClienteVC.swift
//  InBeauty
//
//  Created by ADMIN on 24/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

@objc protocol CustomerService
{
    @objc optional func getCustomerService(services:[OnService])
    @objc optional func startActivityIndicator()
    @objc optional func getCustomerOnRecord(records:[OnService])
    @objc optional func getCustomerReminders(reminders:[OnService])
}

protocol RefreshBoard
{
    func reloadBoard()
}

class BoardVC: UIViewController,RefreshBoard/*,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout*/{

    @IBOutlet weak var board: UILabel!
    @IBOutlet var stackClient: UIStackView!
    @IBOutlet var subCliente: UIView!
    @IBOutlet var onServiceVC: UIView!
    @IBOutlet var followUpVC: UIView!
    @IBOutlet var onRecordVC: UIView!
    @IBOutlet var collectionBoard: UICollectionView!
    
    //static let boardInstance = BoardVC()
    //var services : [OnService] = []
    static var sendRecommendInbox = false
    
    var horizontalBar : NSLayoutConstraint!
    var delegateServices : CustomerService?
    var delegateReminders : CustomerService?
    var delegateOnRecord : CustomerService?
    var change : Bool = false
    
    enum Board
    {
        case followUp
        case onService
        case onRecord 
    }
    
    enum CurrentUser
    {
        case client
        case profesional
    }
    
    var currentUser : CurrentUser = .profesional
    var currentBoard : Board = .onService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        horizontalBar = self.view.constraints.first(where: {$0.identifier == "centerX"})!
        //horizontalBar.constant = UIScreen.main.bounds.midX - (subCliente.bounds.midX)
        //btnViewSelected = stackClient.subviews[1]
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeBoardMenu(_:)))
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeBoardMenu(_:)))
        swipeLeftGesture.direction = .left
        
        self.view.addGestureRecognizer(swipeRightGesture)
        self.view.addGestureRecognizer(swipeLeftGesture)
        
        let btnService = stackClient.subviews[1]
    
        self.followUpVC.alpha = 0
        self.onRecordVC.alpha = 0
        
        if let tipo = AppDelegate.user?.tipo,tipo == "cliente"
        {
            currentUser = .client
            self.followUpVC.alpha = 1
            btnService.isHidden = true
            onServiceVC.isHidden = true
            let btnFollow = stackClient.subviews[0] as! UIButton
            board.textColor = #colorLiteral(red: 0.000754669134, green: 0.9009926915, blue: 0.7528850436, alpha: 1)
            btnFollow.setTitleColor(#colorLiteral(red: 0.000754669134, green: 0.9009926915, blue: 0.7528850436, alpha: 1), for: .normal)
            subCliente.backgroundColor = #colorLiteral(red: 0.000754669134, green: 0.9009926915, blue: 0.7528850436, alpha: 1)
            horizontalBar.constant = -(view.bounds.width / 4)
        }else
        {
            btnService.isHidden = false
            horizontalBar.constant = 0
        }
        
        //collectionBoard.dataSource = self
        //collectionBoard.delegate = self
        //collectionBoard.reloadData()
     
        self.checkLastVersionApp()
        self.listServicesWS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if BoardVC.sendRecommendInbox
        {
            BoardVC.sendRecommendInbox = false
            self.listServicesWS()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //let indexBoard = IndexPath(item: 1, section: 0)
        //collectionBoard.scrollToItem(at: indexBoard, at: .centeredHorizontally, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .default
    }
    
    func checkLastVersionApp()
    {
        let inbiVersion : String = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")
        
        inBiServices.instancia.versionDeInbi { (respuesta) in
            if let appVersion = respuesta as? String
            {
                print("Version de la web:",appVersion)
                if inbiVersion < appVersion
                {
                    let ok = UIAlertAction(title: "Update", style: .default) { (action) in
                        
                        guard let urlInbi = URL(string: "http://itunes.apple.com/us/app/id1475534700") else {return}
                        
                        if UIApplication.shared.canOpenURL(urlInbi)
                        {
                            UIApplication.shared.open(urlInbi, options: [:], completionHandler: nil)
                        }
                    }
                    self.mostrarAlerta(titulo: "Inbi", msj: "A new version is available.Please,download it.", acciones: [ok], bloqueCompletacion: nil)
                }
            }
        }
    }
    
    func listServicesWS()
    {
        print("List services WS")
        //delegateOnRecord?.getCustomerOnRecord?(records: [OnService]())
        delegateOnRecord?.startActivityIndicator?()
        //delegateServices?.getCustomerService?(services: [OnService]())
        delegateServices?.startActivityIndicator?()
        //delegateReminders?.getCustomerReminders?(reminders: [OnService]())
        delegateReminders?.startActivityIndicator?()
        
        self.comprobarInternet { (conectado, msj) in
            
            if conectado
            {
                inBiServices.instancia.listServices(token: AppDelegate.user?.access_token ?? "", id: AppDelegate.user?.id ?? "") { (respuesta) in
                    
                    //print(respuesta)
                    guard let services = respuesta as? [OnService] else {
                        print("UPS!!")
                        return
                    }
                    //print("List Services!!")
                    let onServices = services.filter({return $0.status == 0})
                    self.delegateServices?.getCustomerService?(services: onServices)
                    
                    let records = services.filter({return $0.status == 1})
                    self.delegateOnRecord?.getCustomerOnRecord?(records: records)
                    
                    self.delegateReminders?.getCustomerReminders?(reminders: services)
                    //BoardVC.boardInstance.services = services
                }
            }else
            {
                self.mostrarAlerta(titulo: "inBi", msj: msj, acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            }
        }
    }
    
    @objc func swipeBoardMenu(_ gesture : UISwipeGestureRecognizer)
    {
        let allViews : [UIView] = [onServiceVC,onRecordVC,followUpVC]
        
        switch gesture.direction {
        case .left:
            //allViews.forEach({$0.isUserInteractionEnabled = false})
            
            if self.currentUser == .profesional
            {
                if horizontalBar.constant == 0
                {
                    self.changeBoardMenu(btn: stackClient.subviews[2])
                }else if horizontalBar.constant.sign == .minus
                {
                    self.changeBoardMenu(btn: stackClient.subviews[1])
                }
            }else
            {
                self.changeBoardMenu(btn: stackClient.subviews[0])
            }
            
         
        case .right:
            //allViews.forEach({$0.isUserInteractionEnabled = false})
            if self.currentUser == .profesional
            {
                if horizontalBar.constant == 0
                {
                    self.changeBoardMenu(btn: stackClient.subviews[0])
                }else if horizontalBar.constant.sign == .plus
                {
                    self.changeBoardMenu(btn: stackClient.subviews[1])
                }
            }else
            {
                self.changeBoardMenu(btn: stackClient.subviews[2])
            }
         
        default:
            allViews.forEach({$0.isUserInteractionEnabled = true})
        }
    }
    
    func reloadBoard() {
        self.listServicesWS()
    }
    
    @IBAction func opcionesCliente(_ sender: UIButton)
    {
        let btn = stackClient.subviews[sender.tag]
        
        self.changeBoardMenu(btn: btn)
    }

    func changeBoardMenu(btn:UIView)
    {
        var constantX :CGFloat = 0
    
        func transitionFade(viewShow:UIView,viewsFade:[UIView])
        {
            UIView.animate(withDuration: 0.25) {
                viewShow.alpha = 1
                viewsFade.forEach({$0.alpha = 0})
            }
        }
        
        self.onRecordVC.endEditing(true)
        
        let colors : [UIColor] = [#colorLiteral(red: 0.000754669134, green: 0.9009926915, blue: 0.7528850436, alpha: 1),#colorLiteral(red: 0.9720008969, green: 0.8148944974, blue: 0.1307675838, alpha: 1),#colorLiteral(red: 0.5457351208, green: 0.9523947835, blue: 0.9556872249, alpha: 1)]
        
        func changeColor(btnColor:UIButton,btnColors:[UIButton])
        {
            UIView.animate(withDuration: 0.25) {
                self.board.textColor = colors[btn.tag]
                self.subCliente.backgroundColor = colors[btn.tag]
                btnColor.setTitleColor(colors[btn.tag], for: .normal)
                btnColors.forEach({$0.setTitleColor(UIColor.lightGray, for: .normal)})
            }
        }
        
        switch btn.tag {
        case 0:
            //print("Follow Up")
            currentBoard = .followUp
            
            let divisor : CGFloat = (self.currentUser == .profesional) ? 3 : 4
            constantX = view.bounds.width / divisor
            constantX.negate()
            
            transitionFade(viewShow: followUpVC, viewsFade: [onRecordVC,onServiceVC])
            changeColor(btnColor: btn as! UIButton, btnColors: [stackClient.subviews[1] as! UIButton,stackClient.subviews[2] as! UIButton])
            self.view.bringSubviewToFront(self.followUpVC)
        case 1:
            //print("On Service")
            currentBoard = .onService
            constantX = 0
            transitionFade(viewShow: onServiceVC, viewsFade: [onRecordVC,followUpVC])
            changeColor(btnColor: btn as! UIButton, btnColors: [stackClient.subviews[0] as! UIButton,stackClient.subviews[2] as! UIButton])
            self.view.bringSubviewToFront(self.onServiceVC)
        case 2:
            //print("On Record")
            currentBoard = .onRecord
            let divisor : CGFloat = (self.currentUser == .profesional) ? 3 : 4
            constantX = view.bounds.width / divisor
          
            transitionFade(viewShow: onRecordVC, viewsFade: [followUpVC,onServiceVC])
            changeColor(btnColor: btn as! UIButton, btnColors: [stackClient.subviews[0] as! UIButton,stackClient.subviews[1] as! UIButton])
            self.view.bringSubviewToFront(self.onRecordVC)
        default:
            break
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.horizontalBar.constant = constantX
            self.view.layoutIfNeeded()
        }, completion: { (bool) in
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print(scrollView.contentOffset.x / 2)
        //self.horizontalBar.constant = (scrollView.contentOffset.x / 2) - (subCliente.bounds.midX)
    }
    
    @IBAction func volverBoard(_ segue: UIStoryboardSegue)
    {
        print("On Boardd papu")
        let serviceVC = segue.source as! ServiceNameVC
        serviceVC.service = nil
        ServiceNameVC.serviceClient = ServiceClient()
        
        self.listServicesWS()
        
        if change
        {
            change = false
            
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        switch segue.identifier {
        case "OnService":
            //print("Estamos en el Services")
            let onService = segue.destination as! OnServiceVC
            onService.refreshBoard = self
            self.delegateServices = onService
        case "OnRecord":
            //print("Estamos en el OnRecord")
            let onRecord = segue.destination as! OnRecordVC
            onRecord.refreshBoard = self
            self.delegateOnRecord = onRecord
        case "FollowUp":
            //print("Estamos en el OnFollow")
            let followUp = segue.destination as! FollowUpVC
            followUp.refreshBoard = self
            self.delegateReminders = followUp
        default:
            break
        }
    }
}
//        let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeBoardMenu(_:)))
//        swipeUpGesture.direction = .up
//
//        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeBoardMenu(_:)))
//        swipeDownGesture.direction = .down

//let tapGesture = UIGestureRecognizer(target: self, action: #selector(enableViews(_:)))

//let panGesture = UIPanGestureRecognizer(target: self, action: #selector(enableViews(_:)))
//panGesture.state =

//self.view.addGestureRecognizer(swipeUpGesture)
//self.view.addGestureRecognizer(swipeDownGesture)
//self.view.addGestureRecognizer(panGesture)
//self.view.addGestureRecognizer(tapGesture)

//self.followUpVC.alpha = 0
//self.onRecordVC.alpha = 0

/*
@objc func enableViews(_ gesture : UIGestureRecognizer)
{
    let positionX = gesture.location(in: self.view).x
    let distance = subCliente.center.x.distance(to: btnViewSelected.center.x)
    
    switch gesture.state {
    case .changed:
        print("State Changed \(distance)")
        horizontalBar.constant = positionX
    case .ended:
        print("State Ended \(positionX)")
        lastValueX = positionX
        
        if distance > 70
        {
            
        }
        
        if distance < -70
        {
            
        }
        
    default:
        print("Other States")
    }
}
*/

/*

*/

/*
func codigodescartado()
{
    //self. stackClient.layoutIfNeeded()
    //self.stackClient.layoutSubviews()
    
    self.stackClient.updateConstraints()
    //self.stackClient.updateConstraintsIfNeeded()
    
    print(stackClient.frame.origin)
    print(stackClient.bounds.origin)
    
    print(stackClient.frame.midX)
    print(stackClient.bounds.midX)
    
    print(subCliente.frame.origin)
    print(subCliente.bounds.origin)
    
    print(subCliente.frame.midX)
    print(subCliente.bounds.midX)
    
    self.subCliente.frame.origin.x = stackClient.bounds.midX - subCliente.bounds.midX
    
    self.subCliente.frame.origin.y = (stackClient.frame.origin.y + stackClient.bounds.height) + stackClient.bounds.height * 0.1
    
}
*/
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: view.bounds.width, height: view.bounds.height)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "boardCell", for: indexPath)
//
//        let colors : [UIColor] = [UIColor.green,UIColor.red,UIColor.blue]
//        cell.backgroundColor = colors[indexPath.item]
//
//        return cell
//    }
