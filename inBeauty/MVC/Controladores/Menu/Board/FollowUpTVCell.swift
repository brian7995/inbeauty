//
//  FollowUpTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 10/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

protocol ReSendFollowUp {
    func reSendFollowUp(idClient:String,imgClient:UIImage?,reminder:Reminder)
}

class FollowUpTVCell: UITableViewCell {

    @IBOutlet weak var imgViewClient: UIImageView!
    @IBOutlet weak var nameClient: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var days: UILabel!
    @IBOutlet weak var btnReSend: UIButton!
    @IBOutlet weak var centerYBtnReSend: NSLayoutConstraint!
    
    let user = AppDelegate.user ?? User()
    
    var reSendFollowUp:ReSendFollowUp?
    var idClient : String = ""
    var reminder : Reminder? = nil
    //var btnReSend : UIButton? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewClient.alpha = 0
        btnReSend.alpha = 0
        //btnReSend.transform = CGAffineTransform(scaleX: 0, y: 0)
        btnReSend.layer.cornerRadius = btnReSend.bounds.midY
        
        if user.tipo == "profesional"
        {
            let longTap = UILongPressGestureRecognizer(target: self, action: #selector(longTapCell))
            longTap.minimumPressDuration = 0.5
            addGestureRecognizer(longTap)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //  Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //btnReSend?.removeFromSuperview()
        //btnReSend = nil
        //btnReSend.transform = CGAffineTransform(scaleX: 0, y: 0)
        centerYBtnReSend.constant = 0
        btnReSend.alpha = 0
        btnReSend.isHidden = true
    }

    @objc func longTapCell()
    {
        //delegate?.reSendFollowUp(cell: self, btn: &btnReSend)
        if btnReSend.isHidden
        {
            btnReSend.isHidden = false
           
            UIView.animate(withDuration: 0.1) {
                self.btnReSend.alpha = 1
                self.centerYBtnReSend.constant = -10
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func reSendFollowUpAction(_ btn:UIButton)
    {
        print("Re send action")
        btnReSend.alpha = 0
        centerYBtnReSend.constant = 0
        btnReSend.isHidden = true
        
        if let reminder = self.reminder
        {
            reSendFollowUp?.reSendFollowUp(idClient: self.idClient, imgClient: imgViewClient.image, reminder: reminder)
        }
    }
    
    @objc func tapCellUp()
    {
        
    }
    
    func buildCell(follow:OnService,reminder:Reminder)
    {
        let viewFollow = viewWithTag(10)!
        viewFollow.backgroundColor = reminder.numero_restante == 0 ? UIColor.lightGray : #colorLiteral(red: 0.001304339617, green: 0.8347914815, blue: 0.7023031712, alpha: 1)
        
        nameClient.text = follow.cliente.name
        message.text = reminder.mensaje
        days.text = "IN " + reminder.tiempo.uppercased()
        
        self.idClient = follow.cliente.id
        self.reminder = reminder
        
        imgViewClient.cargarImgDesdeURL(urlImg: follow.cliente.foto_url) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewClient.alpha = 1
            })
        }
    }
}
