//
//  OnServiceTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 10/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class OnServiceTVCell: UITableViewCell {

    @IBOutlet weak var imgViewClient: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var nameClient: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layoutIfNeeded()
        //self.imgViewClient.adjustsImageSizeForAccessibilityContentSizeCategory = true
        self.imgViewClient.alpha = 0
        self.imgViewClient.setGradientBackground(colorOne: UIColor.clear, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.0, y2: 0.8, indice: 0, opacity: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //  Configure the view for the selected state
    }

    func buildCell(service:OnService)
    {
        nameService.text = service.author_tag
        nameCategory.text = service.categoria.nombre
        nameClient.text = service.style//"By" + " " + service.profesional.name
        date.text = service.fecha_recordatorio
        
        let mainPhoto = (service.foto_principal.count > 0) ? service.foto_principal : service.cliente.foto_url
        
        imgViewClient.cargarImgDesdeURL(urlImg: mainPhoto) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewClient.alpha = 1
            })
        }
    }
}
