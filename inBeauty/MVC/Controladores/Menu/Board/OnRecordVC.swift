//
//  OnRecordVC.swift
//  InBeauty
//
//  Created by ADMIN on 25/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class OnRecordVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CustomerService {
    
    @IBOutlet weak var recordTV: UITableView!
    @IBOutlet weak var activityRecord: UIActivityIndicatorView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var searchTxt: UITextField!
    
    var records : [OnService] = []
    var recordsFilter : [OnService] = []
    
    var refreshBoard:RefreshBoard?
    
    let user = AppDelegate.user ?? User()
    
    enum Search
    {
        case filter
        case none
    }
    
    var searhRecords : Search = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recordTV.delegate = self
        
        viewSearch.layer.cornerRadius = viewSearch.bounds.midY
        viewSearch.layer.borderWidth = 1
        viewSearch.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        searchTxt.delegate = self
        
        let refreshControl = UIRefreshControl(frame: recordTV.frame)
        refreshControl.addTarget(self, action: #selector(callRefreshBoard), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.5457351208, green: 0.9523947835, blue: 0.9556872249, alpha: 1)
    
        recordTV.refreshControl = refreshControl
    }
    
    @objc func callRefreshBoard()
    {
        refreshBoard?.reloadBoard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //print("on record vc")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.recordsFilter.count > 0
        {
            print("Empezamos a editar")
            self.recordTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var txt = searchTxt.text! + string
        print("searchtxt: ",searchTxt.text!)
        print("txt: ",txt)
        
        if txt == searchTxt.text!
        {
            txt.removeLast()
            print("Ahora txt es :",txt)
        }
        
        self.searhRecords = .filter
        self.recordsFilter = self.records.filter({return $0.author_tag.localizedCaseInsensitiveContains(txt)})
        self.recordTV.reloadData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let txt = textField.text,txt.isEmpty
        {
            self.searhRecords = .none
            self.recordsFilter = self.records
            self.recordTV.reloadData()
        }
        
        return textField.resignFirstResponder()
    }
    
    func startActivityIndicator()
    {
        //activityRecord.startAnimating()
        recordTV.refreshControl?.beginRefreshing()
    }
    
    func getCustomerOnRecord(records: [OnService]) {
        
        self.records = records
        self.records.sort(by: {return $0.updated_at > $1.updated_at})
        
        if self.searhRecords == .none
        {
            self.recordsFilter = records
        }
        
        activityRecord.stopAnimating()
     
        self.recordTV.refreshControl?.endRefreshing()
        self.recordTV.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recordCell", for: indexPath) as! OnRecordTVCell
        let record = recordsFilter[indexPath.row]
        cell.buildCell(record: record)
        //(cell.viewWithTag(100) as! UIImageView).image = UIImage(named: "profilePicture\(indexPath.row)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let cell = tableView.cellForRow(at: indexPath) as? OnRecordTVCell
        let record = self.recordsFilter[indexPath.row]
        print(record.author_tag)
        //let list : [Any] = [record,cell?.imgViewClient.image ?? UIImage()]
        self.performSegue(withIdentifier: "goServiceOnRecord", sender: record)
    }

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceOnRecord"
        {
            let serviceVC = segue.destination as! ServiceOnRecordVC
            
            serviceVC.service = sender as? OnService
            //serviceVC.imgClient = list.last as? UIImage
            serviceVC.isClient = user.tipo == "cliente"
        }
    }
}
