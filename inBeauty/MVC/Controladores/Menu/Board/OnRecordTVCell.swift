//
//  OnRecordTVCell.swift
//  InBeauty
//
//  Created by Brian Nuñez Rios on 6/29/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class OnRecordTVCell: UITableViewCell {

    @IBOutlet weak var imgViewClient: UIImageView!
    @IBOutlet weak var imgViewCategory: UIImageView!
    @IBOutlet weak var typeService: UILabel!
    @IBOutlet weak var forUserClient: UILabel!
    @IBOutlet weak var totalRecommendations: UILabel!
    @IBOutlet weak var tomorrowAt: UILabel!
    @IBOutlet weak var viewRecord: UIView!
    @IBOutlet weak var viewRecommendations: UIView!
    @IBOutlet weak var lastService: UILabel!
    @IBOutlet weak var titleService: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgViewClient.alpha = 0
        imgViewCategory.alpha = 1
        
        viewRecord.layer.borderWidth = 1
        viewRecord.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        viewRecommendations.layer.cornerRadius = viewRecommendations.bounds.midY
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func buildCell(record:OnService)
    {
        titleService.text = record.author_tag
        forUserClient.text = record.cliente.name
        lastService.text = "last service: \(record.fecha_recordatorio)"
        
        tomorrowAt.isHidden = record.reminder.isEmpty
        tomorrowAt.text = "Follow up in \(record.reminder.last?.tiempo ?? "")"
        
        //let category = AppDelegate.user?.miscategorias.first(where: {return $0.id == record.categoria_id})
        typeService.text = record.categoria.nombre//category?.nombre ??  "-"
        //print("Id Recomendacion :", record.recomendaciones.id)
        let total = (record.recomendaciones?.dodont.count ?? 0) + (record.recomendaciones?.use.count ?? 0)
        totalRecommendations.text = String(total) + " recommendations"
        let photoClient = record.foto_principal.count > 0
        paintPhotos(imgView: imgViewClient, urlPhoto: photoClient ? record.foto_principal : record.cliente.foto ?? record.cliente.foto_url)
        paintPhotos(imgView: imgViewCategory, urlPhoto: record.categoria.icon_url ?? "")
    }
    
    func paintPhotos(imgView:UIImageView,urlPhoto:String)
    {
        guard urlPhoto.count > 0 else {return}
        
        if let imgFromCache = imgCache.object(forKey: urlPhoto as NSString)
        {
            imgView.image = imgFromCache
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
            return
        }
        
        
        Alamofire.request(urlPhoto).responseData { (respuesta) in
            
            guard let data = respuesta.data else
            {
                print("No tenemos data! 😱")
                return
            }
            
            let img = UIImage(data: data)!
            imgCache.setObject(img, forKey: urlPhoto as NSString)
            imgView.image = img
            
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
        }
    }
}
