//
//  DetailProfessionalVC.swift
//  InBeauty
//
//  Created by ADMIN on 3/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import GoogleSignIn

class ProfileVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var servicesCV: UICollectionView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var widthFollow: NSLayoutConstraint!
    @IBOutlet weak var stackCountServices: UIStackView!
    @IBOutlet weak var stackCountFollowing: UIStackView!
    @IBOutlet weak var stackCountFollowers: UIStackView!
    
    var services = [OnService]()
    var professional : Profesional?
    var client : Client?
    var img : UIImage?
    
    enum TheProfile
    {
        case menu
        case my
        case professional
        case client
    }
    
    var myProfile : TheProfile = .my
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listServicesWS()
        profileData()
        
//        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
//        tracker.set(kGAIScreenName, value: "ProfileVC")
//        
//        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
//        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func setupViews()
    {
        servicesCV.dataSource = self
        servicesCV.delegate = self
        
        let flowLayout = servicesCV.collectionViewLayout as! UICollectionViewFlowLayout
        let size = (UIScreen.main.bounds.width - 2) / 3
        flowLayout.itemSize = CGSize(width: size, height: size)
        
        imgView.image = nil
        imgView.alpha = 0
        imgView.setGradientBackground(colorOne: UIColor.black, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.0, y2: 1.0, indice: 0, opacity: 0.75)
        
        name.adjustsFontSizeToFitWidth = true
    }
    
    func profileData()
    {
        name.text = professional?.name ?? client?.name ?? AppDelegate.user?.name ?? ""
     
        let urlPhoto = self.client?.foto ?? self.professional?.foto ?? AppDelegate.user?.foto ?? ""
        
        imgView.cargarImgDesdeURL(urlImg: urlPhoto) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgView.alpha = 1
            })
        }
        
        let countServices = stackCountServices.subviews.first as! UILabel
        let countFollowing = stackCountFollowing.subviews.first as! UILabel
        let countFollowers = stackCountFollowers.subviews.first as! UILabel
        
        countServices.text = String(professional?.servicios ?? client?.servicios ?? AppDelegate.user?.servicios ?? 0)
        countFollowing.text = String(professional?.seguidos ?? client?.seguidos ?? AppDelegate.user?.seguidos ?? 0)
        countFollowers.text = String(professional?.seguidores ?? client?.seguidores ?? AppDelegate.user?.seguidores ?? 0)
        
        switch myProfile {
        case .my,.menu:
            print("my profile")
            btnEdit.isHidden = false
            btnFollow.isHidden = true
            //btnBack.isHidden = myProfile == .menu
            let myCategories =  AppDelegate.user?.miscategorias
            
            if let categories = myCategories,categories.count > 0
            {
                var categoryStr = categories.reduce("", {return $0 + $1.nombre + ", "})
                categoryStr.removeLast()
                categoryStr.removeLast()
                subTitle.text = categoryStr
            }
            
            imgView.cargarImgDesdeURL(urlImg: AppDelegate.user?.foto ?? "") {
                UIView.animate(withDuration: 0.25, animations: {
                    self.imgView.alpha = 1
                }, completion: { (bool) in
                    
                    let user = AppDelegate.user ?? User()
                    
                    switch user.tipo
                    {
                    case "cliente":
                        if let user = AppDelegate.user,user.phone == "000000000"
                        {
                            self.performSegue(withIdentifier: "goEditProfile", sender: self)
                        }
                    case "profesional":
                        if let user = AppDelegate.user,user.miscategorias.isEmpty || user.phone == "000000000"
                        {
                            self.performSegue(withIdentifier: "goEditProfile", sender: self)
                        }
                    default:
                        break
                    }
                })
            }
            
        case .professional:
            print("pro")
            let follow = (professional?.soyseguidor == 1) ? "Following" : "Follow"
            btnEdit.isHidden = true
            btnFollow.isHidden = false
            widthFollow.constant = (professional?.soyseguidor == 1) ? 40 : 0
            btnFollow.tag = professional?.soyseguidor ?? 0
            btnFollow.setTitle(follow, for: .normal)
            
            if let location = professional?.location
            {
                subTitle.text = location
            }else
            {
                let myCategories = professional?.miscategorias
                
                if let categories = myCategories,categories.count > 0
                {
                    var categoryStr = categories.reduce("", {return $0 + $1.nombre + ", "})
                    categoryStr.removeLast()
                    categoryStr.removeLast()
                    subTitle.text = categoryStr
                }
            }
            
            imgView.cargarImgDesdeURL(urlImg: professional?.foto ?? professional?.foto_url ?? "") {
                UIView.animate(withDuration: 0.25, animations: {
                    self.imgView.alpha = 1
                })
            }
            
        case .client:
            print("client")
            let follow = (client?.soyseguidor == 1) ? "Following" : "Follow"
            btnEdit.isHidden = true
            btnFollow.isHidden = false
            widthFollow.constant = (client?.soyseguidor == 1) ? 40 : 0
            btnFollow.tag = client?.soyseguidor ?? 0
            btnFollow.setTitle(follow, for: .normal)
            
            imgView.cargarImgDesdeURL(urlImg: client?.foto ?? client?.foto_url ?? "") {
                UIView.animate(withDuration: 0.25, animations: {
                    self.imgView.alpha = 1
                })
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
        {
        return .lightContent
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        if myProfile == .my
        {
            let logOutAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                
                inBiServices.instancia.logOut(token: AppDelegate.user?.access_token ?? "", completacion: { (respuesta) in
                    
                    //print(respuesta)
                    
                    if let chooseVC = self.navigationController?.viewControllers.first(where: {return $0 is ChooseVC})
                    {
                        self.navigationController?.popToViewController(chooseVC, animated: true)
                    }else
                    {
                        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
                        let chooseVC = loginStoryboard.instantiateViewController(withIdentifier: "ChooseID")
                        self.navigationController?.pushViewController(chooseVC, animated: true)
                    }
                    
                    GIDSignIn.sharedInstance()?.signOut()
                    
                    let loginFB = LoginManager()
                    loginFB.logOut()
                    
                    AccessToken.current = nil
                    Profile.current = nil
                })
            }
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action) in
            }
            
            self.mostrarAlerta(titulo: "inBi", msj: "Log Out?", acciones: [logOutAction,cancelAction], bloqueCompletacion: nil)
        }else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func listServicesWS()
    {
        let idUsuario = professional?.id ?? client?.id ?? AppDelegate.user?.id ?? ""
        inBiServices.instancia.listServices(token: AppDelegate.user?.access_token ?? "", id: idUsuario) { (respuesta) in
            
            //print(respuesta)
            guard let services = respuesta as? [OnService] else {
                print("UPS!!")
                return
            }
          
            self.services = services
            self.servicesCV.reloadData()
        }
    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        //let indexCRV = IndexPath(item: 0, section: 0)
//        let profileCRV = servicesCV.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "profilePictureCRV", for: indexPath)
//        let sizeProfileCRV = CGSize(width: view.bounds.width, height: view.bounds.height * 0.6)
//        profileCRV.bounds.size = sizeProfileCRV
//        return profileCRV
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath)
        
        let imgView = cell.viewWithTag(100) as! UIImageView
        let service = services[indexPath.row]
        
        let mainPhoto = (service.foto_principal.count > 0) ? service.foto_principal : service.cliente.foto_url
        
        imgView.cargarImgDesdeURL(urlImg: mainPhoto) {
            UIView.animate(withDuration: 0.25, animations: {
                imgView.alpha = 1
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)!
        let imgView = cell.viewWithTag(100) as! UIImageView
        let client = services[indexPath.item]
        let lista : [Any] = [client,imgView.image ?? UIImage(),self.imgView.image ?? UIImage()]
        self.performSegue(withIdentifier: "goClient", sender: lista)
    }
    
    @IBAction func followAction(_ btn: UIButton)
    {
        func startFollowPro(tag:Int)
        {
            UIView.animate(withDuration: 0.3, animations: {
                let colorBg = (btn.backgroundColor == #colorLiteral(red: 0.655457139, green: 0.5059521794, blue: 0.9694766402, alpha: 1))
                btn.backgroundColor = (colorBg) ? #colorLiteral(red: 0.8769749932, green: 0.8827018637, blue: 0.9885920922, alpha: 1) : #colorLiteral(red: 0.655457139, green: 0.5059521794, blue: 0.9694766402, alpha: 1)
                
                let colorTxt = (btn.titleLabel?.textColor == UIColor.white)
                btn.setTitleColor(colorTxt ? UIColor.lightGray : UIColor.white, for: .normal)
                
            }) { (bool) in
                
                if btn.tag == tag
                {
                    startFollowPro(tag: 0)
                }
            }
        }
        
        if btn.tag == 0
        {
            startFollowPro(tag: 0)
            followUp(pro: professional, cli: client)
        }else
        {
            startFollowPro(tag: 1)
            followDown(pro: professional, cli: client)
        }
    }
    
    func followUp(pro:Profesional?,cli:Client?)
    {
        inBiServices.instancia.followUp(token: AppDelegate.user?.access_token ?? "", id: pro?.id ?? cli?.id ?? "") { (respuesta) in
            self.btnFollow.tag = 1
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.655457139, green: 0.5059521794, blue: 0.9694766402, alpha: 1)
            self.btnFollow.setTitle("Following", for: .normal)
            self.btnFollow.setTitleColor(UIColor.white, for: .normal)
            AppDelegate.user?.seguidos += 1
            UIView.animate(withDuration: 0.25, animations: {
                self.widthFollow.constant = 40
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func followDown(pro:Profesional?,cli:Client?)
    {
        inBiServices.instancia.followDown(token: AppDelegate.user?.access_token ?? "", id: pro?.id ?? cli?.id ?? "") { (respuesta) in
            self.btnFollow.tag = 0
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.655457139, green: 0.5059521794, blue: 0.9694766402, alpha: 1)
            self.btnFollow.setTitle("Follow", for: .normal)
            self.btnFollow.setTitleColor(UIColor.white, for: .normal)
            AppDelegate.user?.seguidos -= 1
            UIView.animate(withDuration: 0.25, animations: {
                self.widthFollow.constant = 0
                self.view.layoutIfNeeded()
            })
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "goClient"
        {
            let lista = sender as! [Any]
            let client = lista.first as! OnService
            let discovery = Discovery.init(id: client.id, satisfaccion: client.satisfaccion, mensaje: client.mensaje ?? "Type a message here...", fecha_recordatorio: client.fecha_recordatorio, profesional_id: client.profesional.id, author_tag: client.author_tag,style:client.style ?? "Job", cliente: client.cliente, status: client.status, profesional: client.profesional, categoria: client.categoria,fotos:client.fotos)
            
            let detailClientVC = segue.destination as! DetailClientVC
            //detailClientVC.pictures = client.fotos
            detailClientVC.discovery = discovery
            detailClientVC.imgClient = lista[1] as? UIImage
            
            let user = AppDelegate.user ?? User()
            
            if user.tipo == "profesional"
            {
                detailClientVC.imgProfessional = lista.last as? UIImage
            }
            
            //detailClientVC.goPro = false
        }
        
        if segue.identifier == "goEditProfile"
        {
            let editVC = segue.destination as! EditProfileVC
            editVC.myPhoto = self.imgView.image
        }
     }
}
