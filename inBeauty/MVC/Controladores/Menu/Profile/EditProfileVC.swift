//
//  EditProfileVC.swift
//  InBeauty
//
//  Created by ADMIN on 18/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Cloudinary

class EditProfileVC: UIViewController,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CameraAndPhotos,CLUploaderDelegate {
 
    @IBOutlet weak var myPhotoView: UIImageView!
    @IBOutlet weak var viewEditing: UIView!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var proSwitch: UISwitch!
    @IBOutlet weak var titleCV: UICollectionView!
    @IBOutlet weak var btnChanges: UIButton!
    @IBOutlet var editTxts: [UITextField]!
    @IBOutlet var hideTxts: [UIButton]!
    @IBOutlet var underlineTxts: [UIView]!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var bottomViewTitle: NSLayoutConstraint!
    @IBOutlet weak var viewTitlePro: UIView!
    @IBOutlet weak var viewCountries: UIView!
    @IBOutlet weak var codeCountry: UILabel!
    @IBOutlet weak var countriesTV: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var heightViewTitle: NSLayoutConstraint!
    @IBOutlet weak var activityCategories: UIActivityIndicatorView!
    
    var myPhoto : UIImage?
    var categories : [Category] = []
    let user = AppDelegate.user ?? User()
    var countries : [Country] = []
    var currentCountry = Country()
    var uploaderCloud : CLUploader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploaderCloud = CLUploader(AppDelegate.cloud, delegate: self)
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var msjProfile = ""
        
        switch user.tipo {
        case "cliente":
            if user.phone == "000000000"
            {
                editTxts[2].textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                msjProfile = "Please enter your phone to complete your profile."
            }
        case "profesional":
            if user.miscategorias.isEmpty && user.phone == "000000000"
            {
                editTxts[2].textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                msjProfile = "Please choose your categories and enter your phone to complete your profile."
            }else if user.miscategorias.isEmpty
            {
                msjProfile = "Please choose your categories to complete your profile."
            }else if user.phone == "000000000"
            {
                editTxts[2].textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                msjProfile = "Please enter your phone to complete your profile."
            }
        default:
            break
        }
        
        if msjProfile.count > 0
        {
            self.mostrarAlerta(titulo: "inBi", msj: msjProfile, acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            })], bloqueCompletacion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ btn: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        let inbiVersion : String = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")
        versionLabel.adjustsFontSizeToFitWidth = true
        versionLabel.text = "version : \(inbiVersion)"
        
        countriesTV.dataSource = self
        countriesTV.delegate = self
        
        let sizeBtnEdit = view.bounds.width * 0.055
        let fontBtnEdit = UIFont(name: "Futura-Bold", size: sizeBtnEdit)
        btnChanges.titleLabel?.font = fontBtnEdit
        
        editTxts.forEach { (txt) in
            txt.delegate = self
            txt.attributedPlaceholder = NSAttributedString(string: txt.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        }
        
        hideTxts.forEach { (btn) in
            btn.imageView?.contentMode = .scaleAspectFit
        }
        
        self.countries = self.currentCountry.setupCountries()
        self.countriesTV.reloadData()
        
        editTxts[0].text = user.name
        editTxts[1].text = user.email
        editTxts[3].text = user.contrasena
        editTxts[4].text = user.contrasena
        
        if user.phone.contains("_")
        {
            let splitPhone = user.phone.components(separatedBy: "_")
            codeCountry.text = splitPhone.first
            editTxts[2].text = splitPhone.last
            let pais = user.pais ?? ""
            self.currentCountry = self.countries.first(where: {return $0.nombre == pais}) ?? Country()
        }else
        {
            editTxts[2].text = user.phone
        }
        
        editTxts[2].addDoneCancelToolbar()
        btnChanges.layer.cornerRadius = btnChanges.bounds.midY
        
        titleTxt.attributedPlaceholder = NSAttributedString(string: "your title", attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray])
        titleTxt.delegate = self
        
        titleTxt.text = user.title
        
        titleCV.dataSource = self
        titleCV.delegate = self
        
        let isProfessional = user.tipo == "profesional"
        
        if isProfessional
        {
            getCategorysWS()
        }
        
        viewEditing.backgroundColor = (user.tipo == "profesional") ? #colorLiteral(red: 0.8623955846, green: 0.8630526662, blue: 0.8624973893, alpha: 1) : UIColor.white
        viewTitlePro.isHidden = user.tipo == "cliente"
        heightViewTitle.constant = (isProfessional) ? 330 : 0
        
        self.myPhotoView.image = myPhoto
    
        viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
        viewCountries.layer.cornerRadius = 20
        viewCountries.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewCountries.layer.shadowOpacity = 0.25
        viewCountries.layer.shadowRadius = 10
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(openViewCountries))
        codeCountry.addGestureRecognizer(tap)
    }
    
    func getCategorysWS()
    {
        activityCategories.startAnimating()
        inBiServices.instancia.listCategories(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
            
            //print(respuesta)
            self.activityCategories.stopAnimating()
            guard let status = respuesta as? StatusCategory,status.status == "success" else {return}
            
            self.categories = status.data
        
            for (i,categoria) in self.categories.enumerated()
            {
                for myCategoria in self.user.miscategorias
                {
                    if categoria.id == myCategoria.id
                    {
                       self.categories[i].selected = true
                    }
                }
            }

            self.titleCV.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == editTxts[2]
//        {
//            if string == "" && textField.text?.count == 9
//            {
//                textField.text?.removeLast()
//            }
//
//            return textField.text!.count < 9
//        }else
//        {
//            return true
//        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == titleTxt
        {
//            UIView.animate(withDuration: 0.25)
//            {
//                self.bottomViewTitle.constant = 120
//                self.view.layoutIfNeeded()
//            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
//        UIView.animate(withDuration: 0.25) {
//            self.bottomViewTitle.constant = 0
//            self.view.layoutIfNeeded()
//        }
        
        return textField.resignFirstResponder()
    }
    
    @IBAction func openLibraryphotos(_ btn: UIButton)
    {
        let dontShowTutorial = UserDefaults.standard.value(forKey: "tutorial") as? Bool ?? false
        
        if dontShowTutorial
        {
            self.showPickerControllerTypes()
        }else
        {
            self.performSegue(withIdentifier: "goTutorialPhotos", sender: self)
        }
    }
    
    func showPickerControllerTypes()
    {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default) { (action) in
            picker.sourceType = .camera
            picker.cameraDevice = .front
            picker.cameraViewTransform = CGAffineTransform(scaleX: -1, y: 1)
            self.showDetailViewController(picker, sender: nil)
        }
        
        let photoLibraryAction = UIAlertAction(title: "Open Photo Library", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            self.showDetailViewController(picker, sender: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openPickerControllerTypes() {
        self.showPickerControllerTypes()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("Img Picker Controller")
        myPhotoView.image = info[.originalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: CLUploader Delegate
    func uploaderSuccess(_ result: [AnyHashable : Any]!, context: Any!) {
        let version = result["version"] as? Int ?? -12
        let publicId = result["public_id"] as? String ?? "noid"
        let urlPhoto = AppDelegate.cloud?.url("v" + String(version) + "/" + publicId) ?? "nada"
        print(version)
        print("Exito en Uploader",urlPhoto,"context :",context)
        self.saveProfileWS(urlPhoto: urlPhoto)
    }
    
    func uploaderError(_ result: String!, code: Int, context: Any!) {
        print("Error uploaded",result,code)
        self.saveProfileWS(urlPhoto: nil)
        self.mostrarAlerta(titulo: "Inbi", msj: "Profile successfully updated.Failed to upload photo. Try again.", acciones: [UIAlertAction(title: "Ok", style: .cancel, handler: nil)], bloqueCompletacion: nil)
    }
    
    func uploaderProgress(_ bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int, context: Any!) {
        //let pro : CLongDouble = CLongDouble(totalBytesWritten/totalBytesExpectedToWrite)
        print("Uploader Progress",bytesWritten,totalBytesWritten,totalBytesExpectedToWrite)
    }
    
    //MARK: Table View Countries
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath)
        
        let country = self.countries[indexPath.row]
        
        let bandera = cell.viewWithTag(100) as! UILabel
        let codigo = cell.viewWithTag(101) as! UILabel
        let nombre = cell.viewWithTag(102) as! UILabel
        
        bandera.text = country.bandera
        codigo.text = country.codigo
        nombre.text = country.nombre
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentCountry = self.countries[indexPath.row]
        self.codeCountry.text = self.currentCountry.codigo
        self.codeCountry.textColor = UIColor.darkGray
        self.viewCountries.isHidden = true
        self.viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    @objc func openViewCountries()
    {
        self.viewCountries.isHidden = false
        //self.viewCountries.isUserInteractionEnabled = true
        //self.viewCountries.alpha = 1
        //self.viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.75, options: .curveEaseOut, animations: {
            self.viewCountries.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (bool) in
        }
    }
    
    @IBAction func closeViewCountruries(_ btn: UIButton)
    {
        self.viewCountries.isHidden = true
        //self.viewCountries.isUserInteractionEnabled = false
        //self.viewCountries.alpha = 0
        self.viewCountries.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    @IBAction func showTxt(_ btn: UIButton)
    {
        editTxts[btn.tag].isSecureTextEntry = !editTxts[btn.tag].isSecureTextEntry
    }
    
    //MARK: Collection View Categories
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let category = categories[indexPath.row]
        let widthTitle = CGFloat(category.nombre.count) * 10
        let finalWidth = (widthTitle <= 100) ? 110 : widthTitle
        return CGSize(width: finalWidth , height: 30)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleCell", for: indexPath)
        
        cell.layer.cornerRadius = cell.bounds.midY
        cell.layer.borderWidth = 1
        cell.layer.borderColor = #colorLiteral(red: 0.659155786, green: 0.509832561, blue: 0.9762772918, alpha: 1)
        
        let category = categories[indexPath.row]
        let name = cell.viewWithTag(100) as! UILabel
        
        let selected = category.selected ?? false
        categories[indexPath.row].selected = selected
        
        name.textColor = (category.selected ?? false) ? UIColor.white : UIColor.darkGray
        name.text = category.nombre
        
        cell.backgroundColor = (category.selected ?? false) ? #colorLiteral(red: 0.659155786, green: 0.509832561, blue: 0.9762772918, alpha: 1) : UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        categories[indexPath.row].selected = !(categories[indexPath.row].selected ?? false)
        collectionView.reloadItems(at: [indexPath])
    }
    
    @IBAction func saveChangesAction(_ btn: UIButton)
    {
        guard editTxts.reduce(true, {return $0 && !$1.text!.isEmpty}),editTxts[1].text!.contains("@"),editTxts[2].text!.count >= 9
            ,editTxts[3].text! == editTxts[4].text!,codeCountry.text! != "+00",let imgProfile = self.myPhotoView.image else {
                
                editTxts.forEach { (txt) in
                    if let bg = underlineTxts.first(where: {return $0.tag == txt.tag})
                    {
                        bg.backgroundColor = txt.text!.isEmpty ? UIColor.red : #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1)
                    }
                    
                    if txt == editTxts[1]
                    {
                        underlineTxts[1].backgroundColor = txt.text!.contains("@") ? #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1) : UIColor.red
                    }
                    
                    if txt == editTxts[2] || codeCountry.text! == "+00"
                    {
                        underlineTxts[2].backgroundColor = txt.text!.count < 9 || codeCountry.text! == "+00" ? UIColor.red : #colorLiteral(red: 0.915440567, green: 0.915440567, blue: 0.915440567, alpha: 1)
                    }
                }
                
                if editTxts[3].text! != editTxts[4].text!
                {
                    underlineTxts[3].backgroundColor = UIColor.red
                    underlineTxts[4].backgroundColor = UIColor.red
                }
                
                if myPhotoView.image == nil
                {
                    myPhotoView.layer.borderWidth = 2
                    myPhotoView.layer.borderColor = UIColor.red.cgColor
                }
                return
        }
        
        activityIndicator.startAnimating()
        btnChanges.titleEdgeInsets.right = 15
        btn.isUserInteractionEnabled = false
        
        if let imgSegue = self.myPhoto,imgSegue == imgProfile
        {
            print("Es la misma foto asi que solo actualizamos la demas data")
            self.saveProfileWS(urlPhoto: nil)
        }else
        {
            if let jpegData = imgProfile.jpegData(compressionQuality: 0.25)
            {
                let idPhoto = editTxts[1].text! + "-" + user.tipo
                let subFolder = (user.tipo == "profesional") ? "Professional" : "Client"
                uploaderCloud?.upload(jpegData, options: ["folder":"ProfilePictures/\(subFolder)/","public_id":idPhoto])
            }
        }
    }
    
    func saveProfileWS(urlPhoto:String?)
    {
        let name = editTxts[0].text!
        let phone = self.codeCountry.text! + "_" + editTxts[2].text!
        let pass = editTxts[3].text!
        let confirmPass = editTxts[4].text!
        
        inBiServices.instancia.updateProfile(token: user.access_token, name: name, pass: pass, confirmPass: confirmPass, phone: phone, pais: currentCountry.nombre, foto: urlPhoto ?? user.foto ?? "") { (respuesta) in
            
            let body = respuesta as? [String:Any] ?? [:]
            let status = body["status"] as? String ?? ""
            
            let msj = (status == "success") ?  "Profile successfully updated." : "An error occurred while updating your profile. Try again."
            
            self.activityIndicator.stopAnimating()
            self.btnChanges.isUserInteractionEnabled = true
            self.btnChanges.titleEdgeInsets.right = 0
            
            if self.user.tipo == "profesional"
            {
                let marked = self.categories.filter({return $0.selected ?? false})
                let ids = marked.map({return $0.id})
                
                inBiServices.instancia.saveCategories(token: self.user.access_token, title: self.titleTxt.text ?? "", categories: ids) { (respuesta) in
                    
                    let exito = respuesta as? String ?? ""
                    //let msjCategories = (exito == "success" && status == "success") ? "Profile successfully updated." : "An error occurred while updating your profile. Try again."
                
                    if  status == "success"
                    {
                        AppDelegate.user?.name = name
                        AppDelegate.user?.contrasena = pass
                        AppDelegate.user?.phone = phone
                        
                        if let urlNewPhoto = urlPhoto
                        {
                            AppDelegate.user?.foto = urlNewPhoto
                        }
                    }
                    
                    if exito == "success"
                    {
                        AppDelegate.user?.miscategorias = marked
                        AppDelegate.user?.title = self.titleTxt.text
                    }
                    
                     _ = UserManager.saveUserLogged(AppDelegate.user, nil)
                    
                    self.mostrarAlerta(titulo: "inBi", msj: msj , acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })], bloqueCompletacion: nil)
                }
            }else
            {
                if status == "success"
                {
                    AppDelegate.user?.name = name
                    AppDelegate.user?.contrasena = pass
                    AppDelegate.user?.phone = phone
                    
                    if let urlNewPhoto = urlPhoto
                    {
                        AppDelegate.user?.foto = urlNewPhoto
                    }
                    
                    _ = UserManager.saveUserLogged(AppDelegate.user, nil)
                }
                
                self.mostrarAlerta(titulo: "inBi", msj: msj, acciones:  [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })], bloqueCompletacion: nil)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goTutorialPhotos"
        {
            let tutoVC = segue.destination as! TutorialPhotoVC
            tutoVC.delegate = self
        }
    }
    

}
