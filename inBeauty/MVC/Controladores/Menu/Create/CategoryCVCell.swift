//
//  CategoryCVCell.swift
//  InBeauty
//
//  Created by ADMIN on 3/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class CategoryCVCell: UICollectionViewCell {
 
    @IBOutlet weak var imgViewCategory: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgViewCategory.alpha = 0
        imgViewCategory.layer.cornerRadius = imgViewCategory.bounds.midY
    }
    
    func buildCategory(category:Category)
    {
        name.text = category.nombre
    
        guard let iconUrl = category.icon_url else {return}
        
        if let imgFromCache = imgCache.object(forKey: iconUrl as NSString)
        {
            self.imgViewCategory.image = imgFromCache
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewCategory.alpha = 1
            })
            return
        }
        
        //print(category.icon_url)
        Alamofire.request(iconUrl).responseData { (respuesta) in
            
            guard let data = respuesta.data else
            {
                print("No  tenemos data! 😱")
                return
            }
            
            let img = UIImage(data: data)!
            imgCache.setObject(img, forKey: iconUrl as NSString)
            self.imgViewCategory.image = img
            
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewCategory.alpha = 1
            })
        }
    }
}
