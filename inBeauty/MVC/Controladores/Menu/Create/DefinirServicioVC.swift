//
//  DefinirServicioVC.swift
//  InBeauty
//
//  Created by ADMIN on 24/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class DefinirServicioVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate {
  
    @IBOutlet weak var collectionCategory: UICollectionView!
    @IBOutlet weak var clientImg: UIImageView!
    @IBOutlet weak var nameClient: UILabel!
    @IBOutlet weak var coloringTxt: UITextField!
    @IBOutlet weak var styleTxt: UITextField!
    @IBOutlet weak var authorTxt: UITextField!
    @IBOutlet weak var subColoring: UIView!
    @IBOutlet weak var subStyle: UIView!
    @IBOutlet weak var subAuthor: UIView!
    @IBOutlet weak var scrollCreate: UIScrollView!
    @IBOutlet weak var stackCreate: UIStackView!
    @IBOutlet weak var centerStackCreate: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!
    
    
    var imgClient : UIImage?
    var client : Client!
    var user = AppDelegate.user ?? User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.activarOcultamientoTeclado()
        self.setupViews()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        nameClient.text = "for" + " " + client.name
        collectionCategory.delegate = self
        clientImg.image = imgClient
        
        styleTxt.delegate = self
        authorTxt.delegate = self
        
        let esSE = UIScreen.main.bounds.height == 568
        stackCreate.spacing = (esSE) ? 0 : 15
        centerStackCreate.constant = (esSE) ? 40 : 20
        btnNext.contentVerticalAlignment = (esSE) ? .bottom : .center
        
        let tapProfile = UITapGestureRecognizer(target: self, action: #selector(goToProfile))
        clientImg.addGestureRecognizer(tapProfile)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func goToProfile()
    {
        let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let profileVC = profileStoryboard.instantiateViewController(withIdentifier: "ProfessionalID") as! ProfileVC
        profileVC.myProfile = .client
        profileVC.client = self.client
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        switch textField {
        case styleTxt:
            UIView.animate(withDuration: 0.25) {
                self.collectionCategory.alpha = 0
                self.centerStackCreate.constant = -100
                self.view.layoutIfNeeded()
            }
        case authorTxt:
            UIView.animate(withDuration: 0.25) {
                self.collectionCategory.alpha = 0
                self.centerStackCreate.constant = -200
                self.view.layoutIfNeeded()
            }
        default:
            break
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let esSE = UIScreen.main.bounds.height == 568
        UIView.animate(withDuration: 0.25) {
            self.collectionCategory.alpha = 1
            self.centerStackCreate.constant = (esSE) ? 40 : 20
            self.view.layoutIfNeeded()
        }

        return textField.resignFirstResponder()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return user.miscategorias.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCVCell
        
//        let imgView = (cell.viewWithTag(100) as! UIImageView)
//        let name = cell.viewWithTag(101) as! UILabel
        
        let category = user.miscategorias[indexPath.row]
        let selected = category.selected ?? false
        user.miscategorias[indexPath.row].selected = selected
        
        cell.buildCategory(category: category)
        
//        imgView.image = UIImage(named: "profilePicture\(indexPath.row)")
//        name.text = category.nombre
        
       
        
        //imgView.layer.borderWidth = (selected) ? 2 : 0
        //imgView.layer.borderColor = (selected) ? #colorLiteral(red: 0.61090976, green: 0.5979192257, blue: 0.9999906421, alpha: 1) : UIColor.clear.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)!
        user.miscategorias[indexPath.row].selected = true//!(user.miscategorias[indexPath.row].selected ?? false)
        
        let imgView = cell.viewWithTag(100) as! UIImageView
        
        imgView.layer.borderWidth = 3
        imgView.layer.borderColor = #colorLiteral(red: 0.61090976, green: 0.5979192257, blue: 0.9999906421, alpha: 1)
        
        coloringTxt.text = user.miscategorias[indexPath.row].nombre
        //collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        guard let cell = collectionView.cellForItem(at: indexPath) else {return}
        user.miscategorias[indexPath.row].selected = false
        
        let imgView = cell.viewWithTag(100) as! UIImageView
    
        imgView.layer.borderWidth = 0
        imgView.layer.borderColor = UIColor.clear.cgColor
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton)
    {
        guard let category = user.miscategorias.first(where: {return $0.selected ?? false}),!styleTxt.text!.isEmpty,!authorTxt.text!.isEmpty else {
            
            UIView.animate(withDuration: 0.5) {
                self.subColoring.backgroundColor = (self.coloringTxt.text!.isEmpty) ? UIColor.red : #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
                self.subStyle.backgroundColor = (self.styleTxt.text!.isEmpty) ? UIColor.red : #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
                self.subAuthor.backgroundColor = (self.authorTxt.text!.isEmpty) ? UIColor.red : #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
            }
            
            return
        }
        
        let list : [Any] = [client,category]
        
        self.subColoring.backgroundColor =  #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
        self.subStyle.backgroundColor =  #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
        self.subAuthor.backgroundColor =  #colorLiteral(red: 0.9010849078, green: 0.8976425322, blue: 0.9913689489, alpha: 1)
        
        ServiceNameVC.serviceClient.style = styleTxt.text!
        ServiceNameVC.serviceClient.author_tag = authorTxt.text!
        self.performSegue(withIdentifier: "goServiceName", sender: list)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceName"
        {
            let serviceVC = segue.destination as! ServiceNameVC
            let list = sender as! [Any]
        
            serviceVC.client = list.first as? Client
            serviceVC.category = list.last as? Category
            serviceVC.imgClient = imgClient
            serviceVC.nameCategoryStr = coloringTxt.text ?? ""
        }
        
    }

}
