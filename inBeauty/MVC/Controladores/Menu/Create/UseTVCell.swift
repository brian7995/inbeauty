//
//  UseTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 18/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseTVCell: UITableViewCell {

    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var frecuency: UILabel!
    @IBOutlet weak var timeUse: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var nameBrand: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameCategory.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func buildCell(theFrecuency:String,product:Producto)
    {
        nameProduct.text = product.nombre
        nameCategory.text = (product.categoria?.nombre ?? "") + "\n" + (product.descripcion ?? "")
        nameBrand.text = product.categoria?.marca?.nombre
        
        let frecuencyStr = theFrecuency.components(separatedBy: " ")
        timeUse.text = frecuencyStr.last
        var numberFrecuency = frecuencyStr.first
        numberFrecuency?.removeLast()
        frecuency.text = numberFrecuency
        
        imgViewProduct.image = #imageLiteral(resourceName: "LorealProducto")
        imgViewProduct.cargarImgDesdeURL(urlImg: product.url_foto) {
        }
    }

}
