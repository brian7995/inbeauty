//
//  FechaVencimientoVC.swift
//  InBeauty
//
//  Created by ADMIN on 24/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class DueOnVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var stackWeek: UIStackView!
    @IBOutlet weak var daysCV: UICollectionView!
    @IBOutlet weak var timeCV: UICollectionView!
    @IBOutlet weak var serviceTxt: UITextField!
    @IBOutlet weak var centerYTxt: NSLayoutConstraint!
    @IBOutlet weak var viewFollowUp: UIView!
    @IBOutlet weak var forUsername: UILabel!
    @IBOutlet weak var imgClientView: UIImageView!
    
    var allDays : [Int] = []
    var times = ["Days","Weeks","Months"]
    var nameClient : String = ""
    var imgClient:UIImage?
    
    var addReco : AddRecommend = .service
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        
        serviceTxt.delegate = self
        
        forUsername.adjustsFontSizeToFitWidth = true
        forUsername.text = "for" + " " + nameClient
        imgClientView.image = imgClient
        
        for num in 1...31
        {
            allDays.append(num)
        }
        
        var btns = [UIButton]()
        
        stackWeek.subviews.forEach { (stack) in
            btns.append(stack.subviews.first(where: {$0 is UIButton}) as! UIButton)
        }
        
        btns.forEach { (btn) in
            btn.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        //serviceTxt.attributedPlaceholder = NSAttributedString(string:"write down the service you'll do", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        setupCollectionView(collection: daysCV, cellScale: 0.5, cellHeight: 1)
        setupCollectionView(collection: timeCV, cellScale: 1, cellHeight: 1)
        //daysCV.dataSource = self
        daysCV.delegate = self
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25) {
            self.viewFollowUp.alpha = 0
            self.centerYTxt.constant = 150
            self.view.layoutIfNeeded()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.25) {
            self.viewFollowUp.alpha = 1
            self.centerYTxt.constant = 0
            self.view.layoutIfNeeded()
        }
        return textField.resignFirstResponder()
    }
    
    func setupCollectionView(collection : UICollectionView,cellScale:CGFloat,cellHeight:CGFloat)
    {
        collection.dataSource = self
        
        let sizeCell = collection.bounds.width * cellScale
        
        let insetCell = (collection.bounds.width - sizeCell) / 2.0
        let layoutCell = collection.collectionViewLayout as! UICollectionViewFlowLayout
        print(layoutCell.minimumLineSpacing)
        layoutCell.itemSize = CGSize(width: sizeCell, height: collection.bounds.height)
        collection.contentInset = UIEdgeInsets(top: 0, left: insetCell, bottom: 0, right: insetCell)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = daysCV.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case daysCV:
            return allDays.count
        case timeCV:
            return times.count
        default:
            break
        }
        return allDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case daysCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "numberDayCell",for: indexPath)
            
            let numberLabel = cell.viewWithTag(100) as! UILabel
            numberLabel.text = "\(allDays[indexPath.row])"
            
            return cell
        case timeCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timeCell",for: indexPath)
            
            let time = cell.viewWithTag(100) as! UILabel
            time.text = times[indexPath.row]
            return cell
        default:
            return UICollectionViewCell()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) else {return}
        
        let numberLabel = cell.viewWithTag(100) as! UILabel
        numberLabel.alpha = 1
        collectionView.reloadItems(at: [indexPath])
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        print("Scroll View Did End Decelerating")
//        let sortedVisibleCells = daysCV.visibleCells.sorted(by: {return $0.frame.origin.x > $1.frame.origin.x})
//
//        if sortedVisibleCells.count == 2
//        {
//            let firsOrLast = (sortedVisibleCells.first!.frame.origin.x == 0.0)
//            let cell = sortedVisibleCells[(firsOrLast) ? 1 : 0]
//            (cell.viewWithTag(100) as! UILabel).alpha = 0.5
//        }else
//        {
//            let cellL = sortedVisibleCells[0]
//            (cellL.viewWithTag(100) as! UILabel).alpha = 0.5
//
//            let cellM = sortedVisibleCells[1]
//            (cellM.viewWithTag(100) as! UILabel).alpha = 1
//
//            let cellR = sortedVisibleCells[2]
//            (cellR.viewWithTag(100) as! UILabel).alpha = 0.5
//        }
//
//        daysCV.reloadItems(at: daysCV.indexPathsForVisibleItems)
    }
    
    @IBAction func createReminderAction(_ sender: UIButton)
    {
        guard let txt = serviceTxt.text,!txt.isEmpty else {
            self.mostrarAlerta(titulo: "Inbi", msj: "Please,write down the service you'll do.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            return
        }
        
        let sortedVisibleCells = daysCV.visibleCells.sorted(by: {return $0.frame.origin.x > $1.frame.origin.x})
        let cell = sortedVisibleCells[1]
        let numberLabel = cell.viewWithTag(100) as! UILabel
        
        var diaNum = Int(numberLabel.text!)!
        diaNum += (sortedVisibleCells.count == 2 && diaNum == 30) ? 1 : 0

        let indexVisibleTime = timeCV.indexPathsForVisibleItems.first!
        
        var time = times[indexVisibleTime.row]
        var tiempo : String = ""
        
        switch times[indexVisibleTime.row] {
        case "Days":
            tiempo = "dias"
        case "Weeks":
            tiempo = "semanas"
        case "Months":
            tiempo = "meses"
        default:
            break
        }
        
        if diaNum == 1
        {
            time.removeLast()
        }
        
        print(tiempo)
        
        let reminder = Reminder(mensaje: txt, numero: diaNum, tiempo: tiempo, numero_restante: diaNum, tiempo_restante: "In \(diaNum) \(time)")
        
        print(reminder)
        
        var identifier : String = ""
        
        switch addReco {
        case .service:
            identifier = "goServiceName"
        case .inbox:
            identifier = "goInbox"
        }
        
        self.performSegue(withIdentifier: identifier, sender: reminder)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceName"
        {
            let serviceVC = segue.destination as! ServiceNameVC
            
            if let service = serviceVC.service
            {
                service.reminder.append(sender as! Reminder)
            }else
            {
                ServiceNameVC.serviceClient.reminder.append(sender as! Reminder)
            }
        }
        
        if segue.identifier == "goInbox"
        {
            let messageVC = segue.destination as! MessagesVC
            let reminder = sender as! Reminder
    
            let newMessage = Message()
            newMessage.tipo_mensaje = "reminderCell"
            //let mensaje = reminder.mensaje.replacingOccurrences(of: ";", with: "_")
            newMessage.mensaje = "\(reminder.mensaje);\(reminder.tiempo_restante)"
            newMessage.de_user_id = AppDelegate.user?.id ?? ""
            newMessage.para_user_id = messageVC.chatUser?.chatcon.id ?? messageVC.idReceptor
            messageVC.messages.append(newMessage)
        }
    }
}
