//
//  ClientTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 28/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class ClientTVCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //imgView.image = #imageLiteral(resourceName: "placeHolderPicture_Inbi_")
        imgView.layer.cornerRadius = imgView.bounds.midY
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //imgView.image = #imageLiteral(resourceName: "placeHolderPicture_Inbi_")
    }
    
    func buildClient(client:Client)
    {
        name.text = client.name
        //info.text = client.
        let urlClient = client.foto ?? client.foto_url
        //print(urlClient)
        
        if let imgFromCache = imgCache.object(forKey: urlClient as NSString)
        {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgView.alpha = 1
            })
            self.imgView.image = imgFromCache
            return
        }
        
        //print(category.icon_url)
        Alamofire.request(urlClient).responseData { (respuesta) in
            
            guard let data = respuesta.data else
            {
                print("No tenemos  data! 😱")
                return
            }
            
            guard let img = UIImage(data: data) else {return}
            imgCache.setObject(img, forKey: urlClient as NSString)
            self.imgView.image = img
            
            UIView.animate(withDuration: 0.25, animations: {
                self.imgView.alpha = 1
            })
        }
    }
}
