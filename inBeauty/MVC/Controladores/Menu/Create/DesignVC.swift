//
//  DesignVC.swift
//  InBeauty
//
//  Created by ADMIN on 15/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

struct Line
{
    var strokeWidths : CGFloat
    let color : UIColor
    var points : [CGPoint]
}

class Draw : UIView
{
    var colorLine = UIColor.red
    var strokeWidth : CGFloat = 3.5
    //var saveImgIcon = false
    //var saveDesignAsImg = false
    var generateImg = false
    var lines = [Line]()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {return}
  
        context.setLineCap(.round)

        lines.forEach { (line) in
            
            context.setStrokeColor(line.color.cgColor)
            //(saveImgIcon) ? 5 :
            context.setLineWidth(line.strokeWidths)
            
            for (i,p) in line.points.enumerated()
            {
                if i == 0
                {
                    context.move(to: p)
                }else
                {
                    context.addLine(to: p)
                }
            }
            
            context.strokePath()
        }
        
        if generateImg//saveImgIcon || saveDesignAsImg
        {
            if let cgImage = context.makeImage(),!lines.isEmpty
            {
                //print("Guardamos el CGImage del context!")
                //let user : [AnyHashable:Any] = ["Img":cgImage]
                let ntf = Notification(name: Notification.Name(rawValue: "ImgDesignSaved"), object: UIImage(cgImage: cgImage), userInfo: nil)
                NotificationCenter.default.post(ntf)
            }
        }
    }
    
    func generateImgLine(context:CGContext)
    {
        if let cgImage = context.makeImage(),!lines.isEmpty
        {
            //print("Guardamos el CGImage del context!")
            //let user : [AnyHashable:Any] = ["Img":cgImage]
            let ntf = Notification(name: Notification.Name(rawValue: "ImgDesignSaved"), object: UIImage(cgImage: cgImage), userInfo: nil)
            NotificationCenter.default.post(ntf)
        }
    }
    
    func clear()
    {
        print("borramos el diseño que hicimos")
        self.lines.removeAll()
        setNeedsDisplay()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       lines.append(Line(strokeWidths: strokeWidth, color: colorLine, points: [CGPoint]()))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let point = touches.first?.location(in: self) else {return}
        //print(point)
        guard var lastLine = lines.popLast() else {return}
        
        lastLine.points.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ContinueDesigning")))
    }
}

class DesignVC: UIViewController {

    @IBOutlet weak var viewSelectedFace: UIView!
    @IBOutlet weak var stackHead: UIStackView!
    @IBOutlet weak var stackDesign: UIStackView!
    @IBOutlet var btnsDesign: [UIButton]!
    @IBOutlet weak var drawImg: UIImageView!
    @IBOutlet weak var viewDraw: Draw!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var viewDraft: UIView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var imgDesign: UIImageView!
    
    var nameServiceStr : String = ""
    var horizontalSelectedFace : NSLayoutConstraint!
    
    var myDesigns : [Int:Draw] = [:]
    var imgsAngles = [#imageLiteral(resourceName: "designLeft"),#imageLiteral(resourceName: "designBack"),#imageLiteral(resourceName: "designRight"),#imageLiteral(resourceName: "designTop"),#imageLiteral(resourceName: "designFront")]
    var indice = 0
    var imgsDesigns = [UIImage]()
    var generateImgsDesigns = false
    var designId : String = "Face-Left"
    var delegateDesign:StatePhoto?
    
    enum StateService {
        case edit
        case finish
    }
    
    enum StateDesigns
    {
        case follow
        case done
    }
    
    var caseService : StateService = .edit
    var caseDesign : StateDesigns = .follow
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        nameService.text = nameServiceStr
        
        btnContinue.isHidden = true
        btnDone.isHidden = true
        btnDelete.isHidden = true
        
        horizontalSelectedFace = self.view.constraints.first(where: {$0.identifier == "horizontalSelectedFace"})!
        
        viewSelectedFace.layer.cornerRadius = viewSelectedFace.bounds.midY
        
        let centerX = stackHead.subviews.first!.bounds.midX - viewSelectedFace.bounds.midX
        horizontalSelectedFace.constant = stackHead.subviews.first!.frame.origin.x + centerX
        
        viewDraw.isUserInteractionEnabled = (caseService == .edit)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showBtnContinue), name: NSNotification.Name(rawValue: "ContinueDesigning"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(imgDesignSaved), name: NSNotification.Name(rawValue: "ImgDesignSaved"), object: nil)
        
        if ServiceNameVC.designsService.count > 0
        {
            showDesignImg(id: designId)
            
            ServiceNameVC.designsService.forEach { (profile,design) in
                let btnHead = stackHead.subviews.first(where: {return $0.restorationIdentifier == profile}) as! UIButton
                btnHead.setBackgroundImage(design, for: .normal)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //stackHead.subviews.forEach({($0 as! UIButton).currentBackgroundImage?.imageRendererFormat = $0.bounds.size})
    }
    
    @objc func showBtnContinue()
    {
        /*if myDesigns.count == stackHead.subviews.count - 1
        {
            btnContinue.setTitle("Done", for: .normal)
            self.btnContinue.isHidden = false
        }*/
        print("show btn continue")
        self.caseDesign = .done
        
        let hiddenBtnContinue = ServiceNameVC.designsService.count == stackHead.subviews.count - 1
        self.btnContinue.isHidden = hiddenBtnContinue
        
        self.btnDone.isHidden = false
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ContinueDesigning"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueDesignAction(_ sender: UIButton)
    {
        /*if indice == stackHead.subviews.count - 1
        {
            print("Ya hemos terminado de diseñar")
            self.savedDesign(indice: indice)
            //generateImgsDesigns = true
            /*myDesigns.values.forEach { (draw) in
                draw.saveDesignAsImg = true
                draw.setNeedsDisplay()
            }*/
            self.navigationController?.popViewController(animated: true)
        }*/
        self.caseDesign = .follow
        self.btnContinue.isHidden = true
        self.btnDone.isHidden = true
        self.savedDesign(indice: indice)
    }
    
    @IBAction func doneDesignsAction(_ btn: UIButton)
    {
        self.savedDesign(indice: indice)
    }
    
    @objc func imgDesignSaved(ntf:Notification)
    {
        guard let img = ntf.object as? UIImage else {return}

        print("Generamos la img del perfil \(designId)")
        //let id = dicIds[indice] ?? "no id"
        ServiceNameVC.designsService[designId] = img
    
        let btnHead = stackHead.subviews.first(where: {return $0.restorationIdentifier == designId}) as! UIButton
        btnHead.setBackgroundImage(img, for: .normal)
        
        delegateDesign?.newPhoto(id: designId)

        showDesignImg(id: designId)
        
        viewDraw.generateImg = false
        viewDraw.clear()
        
        if self.caseDesign == .done
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func selectedFaceAction(_ btn: UIButton) {
    
        let centerX = btn.bounds.midX - viewSelectedFace.bounds.midX
        self.horizontalSelectedFace.constant = btn.frame.origin.x + centerX
        
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
        }
        
        self.designId = btn.restorationIdentifier ?? "no id"
        print(designId)
        
        viewDraw.setNeedsDisplay()
        
        self.drawImg.image = imgsAngles[btn.tag]
        self.showDesignImg(id: designId)
    }
    
    func showDesignImg(id:String)
    {
        imgDesign.isHidden = true
        viewDraw.isUserInteractionEnabled = caseService == .edit
        btnDelete.isHidden = true
        
        if let img = ServiceNameVC.designsService[id]
        {
            //myDesigns.values.forEach({$0.isHidden = true})
            print("Mostramos el diseño guardado")
            btnDelete.isHidden = caseService == .finish
            
            if viewDraw.lines.count > 0
            {
                print("Eliminamos el diseño que hay")
                viewDraw.lines.removeAll()
            }
            
            self.viewDraw.isUserInteractionEnabled = false
            
            self.imgDesign.isHidden = false
            self.imgDesign.image = img
        }
    }
    
    func savedDesign(indice:Int)
    {
        //print("Guardamos este diseño")
        viewDraw.generateImg = true
        viewDraw.setNeedsDisplay()
    }
    
    @IBAction func designAction(_ btn: UIButton)
    {
        btnsDesign.forEach({$0.alpha = ($0.tag == btn.tag) ? 1 : 0.25})
        
        viewDraw.colorLine = (btn.tag == 2) ? UIColor.white : UIColor.red
        viewDraw.strokeWidth = (btn.tag == 2) ? 10 : 2.5
    }
    
    @IBAction func deleteDesignAction(_ btn: UIButton)
    {
        if let indexDesign = ServiceNameVC.designsService.index(forKey: self.designId)
        {
            print("Tenemos el indice del diseño \(designId)")
            
            viewDraw.isUserInteractionEnabled = true
            viewDraw.clear()
    
            imgDesign.isHidden = true
            btnDelete.isHidden = true
            
            delegateDesign?.deletePhoto(id: self.designId, timePhoto: "Design")
            ServiceNameVC.designsService.remove(at: indexDesign)
            
            let btnHead = stackHead.subviews.first(where: {return $0.restorationIdentifier == designId}) as! UIButton
            btnHead.setBackgroundImage(nil, for: .normal)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
