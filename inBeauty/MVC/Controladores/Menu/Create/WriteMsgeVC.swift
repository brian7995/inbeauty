//
//  WriteMsgeVC.swift
//  InBeauty
//
//  Created by ADMIN on 27/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.

import UIKit

class WriteMsgeVC: UIViewController,UITextViewDelegate{

    @IBOutlet weak var txtMessage: UITextView!
    var message: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtMessage.delegate = self
        txtMessage.becomeFirstResponder()
        
        if message != "Type a message here..."
        {
            txtMessage.text = message
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func saveMessage(_ sender: UIButton)
    {
        guard let txt = txtMessage.text,txt.count > 0 else {return}
        self.performSegue(withIdentifier: "writeMsjUnwind", sender: txt)
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("Did End Editing")
    }
    
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//        print("Should Editing")
//        return textView.resignFirstResponder()
//    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "writeMsjUnwind"
        {
            let serviceName = segue.destination as! ServiceNameVC
            
            if let servicio = serviceName.service
            {
                servicio.mensaje = sender as? String
            }else
            {
                ServiceNameVC.serviceClient.mensaje = sender as! String
            }
        }
    }
    
}
