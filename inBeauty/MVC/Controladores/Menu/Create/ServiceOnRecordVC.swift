//
//  ServiceOnRecordVC.swift
//  InBeauty
//
//  Created by ADMIN on 18/07/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class ServiceOnRecordVC: UIViewController,UITableViewDataSource {

    @IBOutlet weak var imgViewClient: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var indicatorAfBe: UIView!
    @IBOutlet weak var viewPictures: UIView!
    @IBOutlet weak var stackBtnsClient: UIStackView!
    @IBOutlet weak var stackAfBe: UIStackView!
    @IBOutlet weak var nameProfessional: UILabel!
    @IBOutlet weak var style: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var satisfaction: UIButton!
    @IBOutlet weak var viewDataProfessional: UIView!
    @IBOutlet weak var btnDesign: UIButton!
    @IBOutlet weak var numberDesign: UILabel!
    @IBOutlet weak var numberFormula: UILabel!
    @IBOutlet weak var formulaTitle: UILabel!
    @IBOutlet weak var recomendationsTitle: UILabel!
    @IBOutlet weak var useTV: UITableView!
    @IBOutlet weak var DoDontTV: UITableView!
    @IBOutlet weak var formulationsTV: UITableView!
    @IBOutlet weak var heightUseTV: NSLayoutConstraint!
    @IBOutlet weak var heightFormulationsTV: NSLayoutConstraint!
    @IBOutlet weak var heightDoDontTV: NSLayoutConstraint!
    @IBOutlet weak var imgViewProfessional: UIImageView!
    @IBOutlet weak var heightScrollView: NSLayoutConstraint!
    
    var service : OnService!
    var imgClient : UIImage?
    
    var profileBtn : String = "Before-left"
    
    let idsBefore = ["Before-left","Before-front","Before-right","Before-top","Before-back"]
    let idsAfter = ["After-left","After-front","After-right","After-top","After-back"]
    
    var imgsDictBefore : [String:UIImage] = [:]
    var imgsDictAfter : [String:UIImage] = [:]

    var isClient = false
    
    enum TimePhoto
    {
        case before
        case after
    }
    
    var timePhoto : TimePhoto = .after
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
        {
        return .lightContent
    }
    
    func setupViews()
    {
        nameService.text = service.author_tag
    
        imgViewClient.alpha = 0
        self.imgViewClient.cargarImgDesdeURL(urlImg: service.cliente.foto ?? service.cliente.foto_url) {
        }
        imgViewClient.setGradientBackground(colorOne: UIColor.black, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.0, y2: 1.0, indice: 0, opacity: 0.5)
        
        numberFormula.text = String(service.formula?.productos.count ?? 0) + " " + "products"
        date.text = service.fecha_recordatorio
        nameProfessional.text = service.profesional.name
        style.text = service.style
        imgViewProfessional.alpha = 0
        
        imgViewProfessional.cargarImgDesdeURL(urlImg: service.profesional.foto_url) {
            UIView.animate(withDuration: 0.25, animations: {
                self.imgViewProfessional.alpha = 1
            })
        }

        viewDataProfessional.isHidden = self.isClient
        let anchura = viewDataProfessional.constraints.first(where: {return $0.identifier == "anchura"})!
        anchura.constant = self.isClient ? 0 : 250
        
        
        formulaTitle.alpha = (service.formula == nil) ? 0.5 : 1
        recomendationsTitle.alpha = (service.recomendaciones?.use.count ?? 0 > 0 || service.recomendaciones?.dodont.count ?? 0 > 0) ? 1 : 0.5
        
        if !self.isClient
        {
            heightFormulationsTV.constant = 250 * CGFloat(service.formula?.productos.count ?? 0)
        }
        
        let alturaScrollView : CGFloat = self.isClient ? 570 : 820
        
        heightUseTV.constant = 220.0 * CGFloat(service.recomendaciones?.use.count ?? 0)
        heightDoDontTV.constant = 75 * CGFloat(service.recomendaciones?.dodont.count ?? 0)
        heightScrollView.constant = alturaScrollView + heightUseTV.constant + heightFormulationsTV.constant + heightDoDontTV.constant
        
        setupPhotos()
        
        for (indice,btn) in stackBtnsClient.subviews.enumerated()
        {
            btn.restorationIdentifier = idsAfter[indice]
        }
    }
    
    func setupPhotos()
    {
        guard service.fotos.count > 0 else {
            UIView.animate(withDuration: 0.25) {
                self.imgViewClient.alpha = 1
            }
            return
        }
        
        let profileMainPhoto = service.foto_principal.components(separatedBy: "_").last ?? "empty"
        
        for (indice,photo) in service.fotos.enumerated()
        {
            let profile = photo.perfil
            let isBefore = profile.contains("Before")
         
            if let imgFromCache = imgCache.object(forKey: photo.url_fotos as NSString)
            {
                if isBefore
                {
                    self.imgsDictBefore[profile] = imgFromCache
                }else
                {
                    self.imgsDictAfter[profile] = imgFromCache
                    
                    if profile == profileMainPhoto
                    {
                        self.showPictureTaken(profile: profile, dict: self.imgsDictAfter)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewClient.alpha = 1
                        }
                    }else if profile == "After-left"
                    {
                        self.showPictureTaken(profile: "After-left", dict: self.imgsDictAfter)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewClient.alpha = 1
                        }
                    }
                }
                
                if indice == service.fotos.count - 1 && self.imgViewClient.alpha == 0
                {
                    self.imgViewClient.image = imgClient
                    UIView.animate(withDuration: 0.25) {
                        self.imgViewClient.alpha = 1
                    }
                }
                
                continue
            }
            
            request(photo.url_fotos).responseData { (respuesta) in
                
                guard let data = respuesta.data else
                {
                    print("No tenemos  data 😱")
                    return
                }
                
                guard let img = UIImage(data: data) else {
                    if indice == 0
                    {
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewClient.alpha = 1
                        }
                    }
                    return
                }
                
                if isBefore
                {
                    self.imgsDictBefore[profile] = img
                    
                }else
                {
                    self.imgsDictAfter[profile] = img
                    
                    if profile == profileMainPhoto
                    {
                        self.showPictureTaken(profile: profile, dict: self.imgsDictAfter)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewClient.alpha = 1
                        }
                    }else if profile == "After-left"
                    {
                        self.showPictureTaken(profile: "After-left", dict: self.imgsDictAfter)
                        UIView.animate(withDuration: 0.25) {
                            self.imgViewClient.alpha = 1
                        }
                    }
                }
                
                if indice == self.service.fotos.count - 1 && self.imgViewClient.alpha == 0
                {
                    self.imgViewClient.image = self.imgClient
                    UIView.animate(withDuration: 0.25) {
                        self.imgViewClient.alpha = 1
                    }
                }
                
                imgCache.setObject(img, forKey: photo.url_fotos as NSString)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupDesign()
    }
    
    func setupDesign()
    {
        guard service.componentes.count > 0 else {
            numberDesign.text = "0 views updated"
            return
        }
        numberDesign.text = "Loading Designs..."
        btnDesign.isUserInteractionEnabled = false
        
        for (indice,design) in service.componentes.enumerated()
        {
            if let imgFromCache = imgCache.object(forKey: design.url_componente as NSString)
            {
                ServiceNameVC.designsService[design.perfil] = imgFromCache
                
                if indice == service.componentes.count - 1
                {
                    btnDesign.isUserInteractionEnabled = true
                    numberDesign.text = "\(service.componentes.count) view(s) updated"
                }
                
                continue
            }
            
            request(design.url_componente).responseData { (respuesta) in
                
                guard let data = respuesta.data else
                {
                    print("No tenemos  data 😱")
                    return
                }
                
                guard let img = UIImage(data: data) else {
                    return
                }
                
                ServiceNameVC.designsService[design.perfil] = img
                imgCache.setObject(img, forKey: design.url_componente as NSString)
                
                if indice == self.service.componentes.count - 1
                {
                    self.btnDesign.isUserInteractionEnabled = true
                    self.numberDesign.text = "\(self.service.componentes.count) view(s) updated"
                }
                
            }
        }
    }
    
    @IBAction func backAction(_ btn: UIButton)
    {
        ServiceNameVC.designsService.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case formulationsTV:
            return service.formula?.productos.count ?? 0
        case useTV:
            return service.recomendaciones?.use.count ?? 0
        case DoDontTV:
            return service.recomendaciones?.dodont.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case useTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "useProductCell", for: indexPath) as! UseTVCell
            
            if let recomendation = service.recomendaciones
            {
                let use = recomendation.use[indexPath.row]
                cell.buildCell(theFrecuency: use.frecuencia ?? "", product: use.producto)
            }
            
            return cell
        case formulationsTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "formulationCell", for: indexPath) as! FormulaTVCell
            
            if let formula = service.formula
            {
                let product = formula.productos[indexPath.row]
                cell.buildProduct(product: product)
            }
            
            return cell
            
        case DoDontTV:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "recommendationCell", for: indexPath)
            if let recomendation = service.recomendaciones
            {
                let viewBG = cell.viewWithTag(100)!
                let doDontLabel = cell.viewWithTag(101) as! UILabel
                let task = cell.viewWithTag(102) as! UILabel
                
                var tipo = recomendation.dodont[indexPath.row].tipo
                tipo.removeFirst()
                tipo.insert("D", at: tipo.startIndex)
                doDontLabel.text = tipo
                
                viewBG.backgroundColor = (tipo == "Do") ? #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1) : #colorLiteral(red: 0.2232428193, green: 0.2219871581, blue: 0.2965724468, alpha: 1)
                doDontLabel.clipsToBounds = true
                doDontLabel.layer.cornerRadius = doDontLabel.bounds.midX
                doDontLabel.adjustsFontSizeToFitWidth = true
                
                task.text = recomendation.dodont[indexPath.row].tarea
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    @IBAction func changeGroupPhotos(_ btn: UIButton)
    {
        btn.alpha = 1
        
        let btnBefore = stackAfBe.subviews.first!
        let btnAfter = stackAfBe.subviews.last!
     
        let centerXIndicator = viewPictures.constraints.first(where: {return $0.identifier == "centerX"})!
        
        centerXIndicator.constant = (btn.tag == 1) ?  -70 : 70
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
        }
        
        switch btn.tag {
        case 1:
            print("Before")
            timePhoto = .before
            btnAfter.alpha = 0.5
        
            for (indice,btn) in stackBtnsClient.subviews.enumerated()
            {
                btn.restorationIdentifier = idsBefore[indice]
            }
            
            if let rangeBefore = profileBtn.range(of: "After")
            {
                profileBtn.replaceSubrange(rangeBefore, with: "Before")
            }
            
            showPictureTaken(profile: profileBtn, dict: imgsDictBefore)
        case 2:
            print("After")
            timePhoto = .after
            
            btnBefore.alpha = 0.5
            
            for (indice,btn) in stackBtnsClient.subviews.enumerated()
            {
                btn.restorationIdentifier = idsAfter[indice]
            }
            
            if let rangeBefore = profileBtn.range(of: "Before")
            {
                profileBtn.replaceSubrange(rangeBefore, with: "After")
            }
            
            showPictureTaken(profile: profileBtn, dict: imgsDictAfter)
            
        default:
            break
        }
    }

    @IBAction func selectedFaceProfile(_ btn: UIButton)
    {
        profileBtn = btn.restorationIdentifier ?? "no id"
        print(profileBtn)
        switch timePhoto {
        case .before:
            showPictureTaken(profile: profileBtn, dict: imgsDictBefore)
        case .after:
            showPictureTaken(profile: profileBtn, dict: imgsDictAfter)
        }
    }
    
    func showPictureTaken(profile:String,dict:[String:UIImage])
    {
        guard let imgPhoto = dict[profile] else {
            self.imgViewClient.cargarImgDesdeURL(urlImg: service.cliente.foto ?? service.cliente.foto_url) {
            }
            return
        }
        self.imgViewClient.image = imgPhoto
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goDesign"
        {
            let designVC = segue.destination as! DesignVC
            designVC.nameServiceStr = service.author_tag
            designVC.caseService = .finish
        }
        
        if segue.identifier == "goInbox"
        {
            let messageVC = segue.destination as! MessagesVC
            messageVC.idReceptor = self.service.cliente.id
           
            let urlPhotoClient = service.cliente.foto ?? service.cliente.foto_url
            
            let imgView = UIImageView()
            imgView.cargarImgDesdeURL(urlImg: urlPhotoClient) {
                 messageVC.imgClient = imgView.image
            }
        }
    }
}
