//
//  FormulationsVC.swift
//  InBeauty
//
//  Created by ADMIN on 28/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FormulationsVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{

    @IBOutlet weak var nombreFormulacion: UILabel!
    @IBOutlet weak var formulationTV: UITableView!
    @IBOutlet weak var processTxt: UITextField!
    @IBOutlet weak var productoImg: UIImageView!
    @IBOutlet weak var nombreProducto: UILabel!
    @IBOutlet weak var nombreCategoria: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var medida: UILabel!
    @IBOutlet weak var heightTV: NSLayoutConstraint!
    
    var productos = [ProductFormula]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //nombreFormulacion.text = NameFormulationVC.formula.name
        productos = NameFormulationVC.formula.productos
        productos.reverse()
        formulationTV.dataSource = self
        formulationTV.delegate = self
        processTxt.attributedPlaceholder = NSAttributedString(string:"declare your process", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        
        heightTV.constant = 250 * CGFloat(productos.count)
        
        processTxt.attributedPlaceholder = NSAttributedString(string:"declare your process", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
        
        processTxt.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "formulationCell", for: indexPath) as! FormulaTVCell
        
        let producto = productos[indexPath.row]
        cell.buildProduct(product: producto)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteTxt = UILabel()
        deleteTxt.font = UIFont(name:"Futura Bold",size:17.0)
        deleteTxt.font = UIFont.boldSystemFont(ofSize: 17)
        deleteTxt.text = "Delete ✖︎"
        let rowAction = UITableViewRowAction(style: .destructive, title: deleteTxt.text) { (tbrow, index) in
            //print("Row e Index respectivamente : ",tbrow," ",index)
            self.productos.remove(at: index.row)
            NameFormulationVC.formula.productos.remove(at: index.row)
            self.heightTV.constant = 250 * CGFloat(self.productos.count)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        rowAction.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        
        return [rowAction]
       
    }
    
    @IBAction func saveFormulation(_ sender: UIButton)
    {
        NameFormulationVC.formula.descripcion_proceso = processTxt.text!.isEmpty ? "No description" : processTxt.text!
        self.performSegue(withIdentifier: "goServiceName", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goServiceName"
        {
            let serviceVC = segue.destination as! ServiceNameVC
            
            if let service = serviceVC.service
            {
                print("Si existes ")
                service.formula = NameFormulationVC.formula
            }else
            {
                print("no existes")
                ServiceNameVC.serviceClient.formula = NameFormulationVC.formula
            }
            
            NameFormulationVC.formula = Formula()
        }
    }
}


