//
//  FormulaDaysVC.swift
//  InBeauty
//
//  Created by ADMIN on 28/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FormulaDaysVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var stackType: UIStackView!
    @IBOutlet weak var stackNumberDays: UIStackView!
    @IBOutlet weak var numberTxt: UITextField!
    @IBOutlet weak var numberMeasured: UITextField!
    @IBOutlet weak var collectionDate: UICollectionView!
    @IBOutlet weak var collectionNumberDays: UICollectionView!
    @IBOutlet weak var pickerViewNumbers: UIPickerView!
    @IBOutlet weak var nombreProducto: UILabel!
    @IBOutlet weak var nombreCategoria: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var numberTV: UITableView!
    @IBOutlet weak var swicthAmount: UISwitch!
    @IBOutlet weak var grams: UILabel!
    @IBOutlet weak var ounces: UILabel!
    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var activityNumbers: UIActivityIndicatorView!
    @IBOutlet weak var descriptionProduct: UILabel!
    @IBOutlet weak var numberProduct: UILabel!
    
    var numbers : [NumeroProducto] = []
    var allDays : [Int] = []
    let allAmount = ["grams","ounces"]
    var categoria: Categoria!
    var nameBrand :String = ""
    var nombreCategoriaStr : String = ""
    var nameServiceStr = ""
    var producto : Producto!
    var imgProduct : UIImage?
    var numberProductStr: String = ""

    enum Search
    {
        case number
        case quantity
    }
    
    var mySearch : Search = .number
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
        self.listNumbersProduct(num: "")
    }

    func setupViews()
    {
        nombreCategoria.text = nombreCategoriaStr
        nombreProducto.text = producto.nombre
        nameService.text = nameServiceStr
        descriptionProduct.text = producto.descripcion
        
        imgViewProduct.image = imgProduct
        print(nameBrand)
        
        for num in 1...31
        {
            allDays.append(num)
        }
        
        numberTV.dataSource = self
        numberTV.delegate = self
        numberTV.alwaysBounceVertical = true
        
        collectionDate.dataSource = self
        
        let layoutDateCell = collectionDate.collectionViewLayout as! UICollectionViewFlowLayout
        layoutDateCell.itemSize = CGSize(width: collectionDate.bounds.width, height: collectionDate.bounds.height)
        
        numberMeasured.delegate = self
        numberTxt.delegate = self
    }
    
    func listNumbersProduct(num:String)
    {
        activityNumbers.startAnimating()
        inBiServices.instancia.listNumberProducts(token: AppDelegate.user?.access_token ?? "", idCategoria: producto.categoria_id, nombre: producto.nombre, numero: num) { (respuesta) in
            
            if let numeros = respuesta as? [NumeroProducto],numeros.count > 0
            {
                print("Entramos",numeros.count)
                self.btnSave.setTitle("Next", for: .normal)
                self.numbers = numeros
                self.numberTV.reloadData()
                self.numberMeasured.resignFirstResponder()
            }else
            {
                self.btnSave.setTitle("Next", for: .normal)
            }
            
            self.activityNumbers.stopAnimating()
        }
    }
    
    func setupCollectionView(collection : UICollectionView,cellScale:CGFloat,cellHeight:CGFloat)
    {
        collection.dataSource = self
        
        let sizeCell = collection.bounds.width * cellScale
        
        let insetCell = (collection.bounds.width - sizeCell) / 2.0
        let layoutCell = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layoutCell.itemSize = CGSize(width: sizeCell, height: sizeCell * cellHeight)
        
        collection.contentInset = UIEdgeInsets(top: insetCell, left: insetCell, bottom: insetCell, right: insetCell)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
        
        UIView.animate(withDuration: 0.25) {
            let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomSaveProduct"})!
            bottomMessage.constant = (isShowNtf) ? keyboardRect.height : 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func typeNumberAction(_ sender: UIButton)
    {
        stackType.isHidden = true
        stackNumberDays.isHidden = false
        numberTxt.becomeFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if self.mySearch == .number
        {
            self.mySearch = .number
            self.btnSave.setTitle("Next", for: .normal)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        let layout = collectionNumberDays.collectionViewLayout as! UICollectionViewFlowLayout
//        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
//
//        var offset = targetContentOffset.pointee
//        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
//        let roundedIndex = round(index)
//
//        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
//        targetContentOffset.pointee = offset
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case collectionDate:
            return allAmount.count
        case collectionNumberDays:
            return allDays.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let isNumber = (collectionView == collectionNumberDays)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath)
        
        let numberLabel = cell.viewWithTag(100) as! UILabel
        numberLabel.text = allAmount[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "numberCell", for: indexPath)
        let number = numbers[indexPath.row]
        cell.textLabel?.text = number.numero
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("Te selecciono")
        let cell = tableView.cellForRow(at: indexPath)!
        numberProductStr = cell.textLabel?.text ?? ""
        numberProduct.text = "Number: " + numberProductStr
        showUnitQuantity()
    }
    
    func showUnitQuantity()
    {
        mySearch = .quantity
        btnSave.setTitle("Next", for: .normal)
        stackType.isHidden = true
        numberTV.isHidden = true
        stackNumberDays.isHidden = false
        swicthAmount.isHidden = false
        grams.isHidden = false
        ounces.isHidden = false
        numberTxt.becomeFirstResponder()
    }
    
    @IBAction func saveProductAction(_ btn: UIButton) {
        switch mySearch {
        case .number:
            self.listNumbersProduct(num: numberMeasured.text!)
        case .quantity:
            guard let num = numberTxt.text,!num.isEmpty else {return}
            let medida = swicthAmount.isOn ? "ounces" : "grams"
            producto.categoria = self.categoria
            let productFormula = ProductFormula(id:"idProductFormula",producto_id: producto.id,numeroProducto: numberProductStr ,nombreMarca: nameBrand,cantidad: Int(num)!, medida: medida, producto: producto)
            NameFormulationVC.formula.productos.append(productFormula)
            self.performSegue(withIdentifier: "goFormulas", sender: self)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//numberTxt.delegate = self


//self.activarOcultamientoTeclado()


//


//        collectionNumberDays.dataSource = self
//        collectionNumberDays.delegate = self
//
//        let sizeNumberCell = collectionNumberDays.bounds.width * 0.5
//
//        let insetNumberCell = (collectionNumberDays.bounds.width - sizeNumberCell) / 2.0
//        let layoutNumberCell = collectionNumberDays.collectionViewLayout as! UICollectionViewFlowLayout
//        layoutNumberCell.itemSize = CGSize(width: sizeNumberCell, height: sizeNumberCell)
//
//        collectionNumberDays.contentInset = UIEdgeInsets(top: insetNumberCell, left: insetNumberCell, bottom: insetNumberCell, right: insetNumberCell)

