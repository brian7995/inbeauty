//
//  FormulationVC.swift
//  InBeauty
//
//  Created by ADMIN on 13/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FormulationBrandVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
 
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var viewFormula: UIView!
    @IBOutlet weak var productTxt: UITextField!
    @IBOutlet weak var marcasTV: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnMarca: UIButton!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var noResults: UILabel!
    
    var heightImgProduct : NSLayoutConstraint!
    var widthImgProduct : NSLayoutConstraint!
    var leadingImg : NSLayoutConstraint!
    var topProduct : NSLayoutConstraint!
    var heightLine : NSLayoutConstraint!
    var bottomTable : NSLayoutConstraint!
    
    var namesProducts = ["Lancome","Loreal"]
    var lines = ["Line 1","Line 2"]
    var nameServiceStr =  ""
    
    enum Search {
        case brand
        case category
    }
    
    var marcas = [Marca]()
    var categorias = [Categoria]()
    var switchTableView = false
    var idMarca = 0
    var mySearch : Search = Search.brand
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.activarOcultamientoTeclado()
        productLabel.adjustsFontSizeToFitWidth = true
        marcasTV.alwaysBounceVertical = true
        
        nameService.text = nameServiceStr
        productTxt.delegate = self
        
        heightImgProduct = imgProduct.constraints.first(where: {$0.identifier == "heightImgProduct"})!
        widthImgProduct = imgProduct.constraints.first(where: {$0.identifier == "widthImgProduct"})!
        
        leadingImg = viewFormula.constraints.first(where: {$0.identifier == "leadingImg"})!
        topProduct = viewFormula.constraints.first(where: {$0.identifier == "topProduct"})!
        
        bottomTable = viewFormula.constraints.first(where: {$0.identifier == "bottomTable"})!
        
        heightLine = lineLabel.constraints.first(where: {$0.identifier == "heightLine"})!
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnMarca.isHidden = categorias.isEmpty
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let txt = textField.text,txt.count == 2
        {
            if mySearch == .brand
            {
                buscarMarcas(str: txt)
            }else
            {
                buscarCategorias(str: txt)
            }
            //self.noResults.isHidden = !marcas.isEmpty && !categorias.isEmpty
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        noResults.isHidden = true
        
        if mySearch == .brand
        {
            buscarMarcas(str: textField.text!)
        }else
        {
            buscarCategorias(str: textField.text!)
        }
        
        let esSE : CGFloat = (view.bounds.height == 568) ? 0.8 : 0.6
        
        bottomTable.constant = marcasTV.bounds.height * esSE
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if mySearch == .brand
        {
            buscarMarcas(str: textField.text!)
        }else
        {
            buscarCategorias(str: textField.text!)
        }
        
        bottomTable.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        //self.noResults.isHidden = !marcas.isEmpty || !categorias.isEmpty
        return textField.resignFirstResponder()
    }
    
    func buscarMarcas(str:String)
    {
        activityIndicator.startAnimating()
        inBiServices.instancia.buscarMarca(token: AppDelegate.user?.access_token ?? "", iniciales: str) { (respuesta) in
            self.marcas = respuesta as? [Marca] ?? []
            self.activityIndicator.stopAnimating()
            self.noResults.isHidden = !self.marcas.isEmpty
            self.marcasTV.reloadData()
        }
    }
    
    func buscarCategorias(str:String)
    {
        activityIndicator.startAnimating()
        inBiServices.instancia.buscarCategoria(token: AppDelegate.user?.access_token ?? "", idMarca: idMarca, iniciales: str) { (respuesta) in
            self.categorias = respuesta as? [Categoria] ?? []
            self.activityIndicator.stopAnimating()
            self.noResults.isHidden =  !self.categorias.isEmpty
            self.marcasTV.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (mySearch == .brand) ? marcas.count : categorias.count
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "marcaCell", for: indexPath)

        let imgCell = cell.viewWithTag(100) as! UIImageView
        let cellLabel = cell.viewWithTag(101) as! UILabel
        let widthImg = imgCell.constraints.first(where: {$0.identifier == "widthCell"})!
        
        if mySearch == .brand
        {
            UIView.animate(withDuration: 0.15) {
                widthImg.constant = 60//(self.mySearch == .brand) ? 60 : 0
                tableView.layoutIfNeeded()
            }
            
            cellLabel.text = marcas[indexPath.row].nombre
            imgCell.cargarImgDesdeURL(urlImg: marcas[indexPath.row].url_foto ?? "") {
            }
        }else
        {
            widthImg.constant = 0
            cellLabel.text = categorias[indexPath.row].nombre
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)!
        let imgView = cell.viewWithTag(100) as! UIImageView
        
        if mySearch == .brand
        {
            self.imgProduct.image = imgView.image
            self.updateTable(row: indexPath.row)
        }else
        {
            let categoria = categorias[indexPath.row]
            let list : [Any] = [productLabel.text ?? "¿?",categoria]
            self.performSegue(withIdentifier: "goProductos", sender: list)
        }
    }
    
    func updateTable(row:Int)
    {
        switchTableView = !switchTableView
        mySearch = .category
        btnMarca.isHidden = false
        
        idMarca = marcas[row].id
        productLabel.text = marcas[row].nombre
        productTxt.text = ""
        productTxt.placeholder = "Type the line"
        
        //self.marcas.removeAll()
        marcasTV.reloadData()
        
        UIView.animate(withDuration: 0.15) {
            
            self.heightImgProduct.constant = 60//(self.switchTableView) ? 60 : 0
            self.widthImgProduct.constant = 60//(self.switchTableView) ? 60 : 0
            
            self.leadingImg.constant = 16//(self.switchTableView) ? 16 : 0
            self.topProduct.constant =  24//(self.switchTableView) ? 24 : 20
            
            self.heightLine.constant =  20//(self.switchTableView) ? 20 : 0
            self.viewFormula.layoutIfNeeded()
        }
    }
    
    @IBAction func backToMarks(_ sender: UIButton)
    {
        switchTableView = false
        mySearch = .brand
        categorias.removeAll()
        btnMarca.isHidden = true
        noResults.isHidden = true
        
        productLabel.text = "Product Brand"
        productTxt.text = nil
        productTxt.placeholder = "type the product brand"
        
        buscarMarcas(str: "")
        
        UIView.animate(withDuration: 0.15) {
            self.heightImgProduct.constant = 0
            self.widthImgProduct.constant = 0
            
            self.leadingImg.constant = 0
            self.topProduct.constant = 20
            
            self.heightLine.constant = 0
            self.viewFormula.layoutIfNeeded()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goProductos"
        {
            let formulacionVC = segue.destination as! FormulationProductVC
            let lista = sender as! [Any]
            
            formulacionVC.nombreMarcaStr = lista.first as! String
            formulacionVC.categoria = lista.last as? Categoria
            formulacionVC.imgBrand = imgProduct.image
            formulacionVC.nameServiceStr = nameServiceStr
        }
    }
    
}


//        if switchTableView && categorias.count > 0
//        {
//
//        } else
//        {
//            if marcas.count > 0
//            {
//
//            }
//        }
