//
//  NameFormulationVC.swift
//  InBeauty
//
//  Created by ADMIN on 13/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class NameFormulationVC: UIViewController {

    @IBOutlet weak var addProducTxt: UITextField!
    @IBOutlet weak var forAuthor: UILabel!
    
    static var formula = Formula()
    var authorTag = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    
        addProducTxt.attributedPlaceholder = NSAttributedString(string:"for [AuthorsTag]", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])

        addProducTxt.text = authorTag
        NameFormulationVC.formula = Formula()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addProducTxt.becomeFirstResponder()
        addProducTxt.tintColor = #colorLiteral(red: 0.612482667, green: 0.5961741805, blue: 1, alpha: 1)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }

    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
        
        //self.view.frame.origin.y = (isShowNtf) ? -keyboardRect.height : 0
        let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomAddProduct"})!
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.25) {
            bottomMessage.constant = (isShowNtf) ? keyboardRect.height : 32
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func agregarNuevaFormula(_ sender: UIButton)
    {
        guard let nameFormula = addProducTxt.text,!nameFormula.isEmpty else {return}
    
        //NameFormulationVC.formula.name = nameFormula
        //ServiceNameVC.serviceClient.formula.name = nameFormula
        self.performSegue(withIdentifier: "goMarcas", sender: self)
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
