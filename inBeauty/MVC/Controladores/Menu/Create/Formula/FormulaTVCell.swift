//
//  FormulaTVCell.swift
//  InBeauty
//
//  Created by ADMIN on 14/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FormulaTVCell: UITableViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var nombreProducto: UILabel!
    @IBOutlet weak var nombreCategoria: UILabel!
    @IBOutlet weak var nombreAutor: UILabel!
    @IBOutlet weak var descriptionProduct: UILabel!
    @IBOutlet weak var cantidadProducto: UILabel!
    @IBOutlet weak var medidaProducto: UILabel!
    @IBOutlet weak var numberProduct: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        numberProduct.adjustsFontSizeToFitWidth = true
        cantidadProducto.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func buildProduct(product:ProductFormula)
    {
        nombreProducto.text = product.producto.nombre
        nombreCategoria.text = product.producto.categoria?.nombre
        descriptionProduct.text = product.producto.descripcion
        nombreAutor.text = product.nombreMarca ?? product.producto.categoria?.marca?.nombre
        numberProduct.text = product.numeroProducto ?? product.producto.numero
        
        cantidadProducto.text = String(product.cantidad)
        medidaProducto.text = product.medida
        
        imgProduct.image = #imageLiteral(resourceName: "LorealProducto")
        imgProduct.cargarImgDesdeURL(urlImg: product.producto.url_foto) {
        }
    }

}
