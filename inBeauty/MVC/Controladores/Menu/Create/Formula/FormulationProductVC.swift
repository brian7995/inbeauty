//
//  FormulationProductVC.swift
//  InBeauty
//
//  Created by ADMIN on 17/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FormulationProductVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var productTV: UITableView!
    @IBOutlet weak var producTxt: UITextField!
    @IBOutlet weak var viewFormulation: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var noResults: UILabel!
    @IBOutlet weak var imgViewBrand: UIImageView!
    @IBOutlet weak var nombreMarca: UILabel!
    @IBOutlet weak var nombreCategoria: UILabel!
    @IBOutlet weak var nameService: UILabel!
    
    var categoria : Categoria!
    var nameServiceStr : String = ""
    var nombreMarcaStr = ""
    
    var bottomTable : NSLayoutConstraint!
    var productos : [Producto] = []
    var imgBrand:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        productTV.dataSource = self
        productTV.delegate = self
        productTV.alwaysBounceVertical = true
        
        producTxt.delegate = self
        
        nombreMarca.text = nombreMarcaStr
        nombreCategoria.text = categoria.nombre
        nameService.text = nameServiceStr
        
        imgViewBrand.image = imgBrand
        
        bottomTable = viewFormulation.constraints.first(where: {return $0.identifier == "bottomTable"})!
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let txt = textField.text,txt.count == 2
        {
            buscarProducto(txt: txt)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        buscarProducto(txt: textField.text ?? "")
        bottomTable.constant = productTV.bounds.width * 0.9
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        buscarProducto(txt: textField.text ?? "")
        bottomTable.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
        return textField.resignFirstResponder()
    }
    
    func buscarProducto(txt:String)
    {
        self.activityIndicator.startAnimating()
        inBiServices.instancia.buscarProducto(token: AppDelegate.user?.access_token ?? "", idCategoria: categoria.id, iniciales: txt) { (respuesta) in
            
            self.productos = respuesta as? [Producto] ?? []
            self.noResults.isHidden = !self.productos.isEmpty
            self.activityIndicator.stopAnimating()
            self.productTV.reloadData()
        }
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "formulaProductCell", for: indexPath)
        let imgCell = cell.viewWithTag(100) as! UIImageView
        let cellLabel = cell.viewWithTag(101) as! UILabel
        
        cellLabel.text = productos[indexPath.row].nombre
        imgCell.cargarImgDesdeURL(urlImg: productos[indexPath.row].url_foto) {
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)!
        let imgCell = cell.viewWithTag(100) as! UIImageView
        let producto = productos[indexPath.row]
        let lista : [Any] = [nombreCategoria.text ?? "¿?",producto,categoria,imgCell.image ?? UIImage()]
        self.performSegue(withIdentifier: "goAmountProduct", sender: lista)
    }
    
    @IBAction func addProductAction(_ segue :UIStoryboardSegue)
    {
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goAmountProduct"
        {
            let formulaVC = segue.destination as! FormulaDaysVC
            let lista = sender as! [Any]
            formulaVC.nombreCategoriaStr = lista.first as! String
            formulaVC.producto = lista[1] as? Producto
            formulaVC.nameServiceStr = nameServiceStr
            formulaVC.nameBrand = nombreMarcaStr
            formulaVC.categoria = lista[2] as? Categoria
            formulaVC.imgProduct = lista.last as? UIImage
        }
    }
}
