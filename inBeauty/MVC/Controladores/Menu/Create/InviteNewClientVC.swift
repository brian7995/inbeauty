//
//  InviteNewClientVC.swift
//  InBeauty
//
//  Created by ADMIN on 16/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class InviteNewClientVC: UIViewController {

    @IBOutlet weak var viewFields: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var userTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        userTxt.delegate = self
        emailTxt.delegate = self
        phoneTxt.delegate = self
        
        emailTxt.becomeFirstResponder()
        
        self.activarOcultamientoTeclado()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
        
        //self.view.frame.origin.y = (isShowNtf) ? -keyboardRect.height : 0
        let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomSendInvitation"})!
        
        UIView.animate(withDuration: 0.25) {
            bottomMessage.constant = (isShowNtf) ? keyboardRect.height : 0
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendInvitationAction(_ btn: UIButton)
    {
        guard let email = emailTxt.text,email.count > 0,email.contains("@") else {return}
        
        btn.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        
        inBiServices.instancia.sendInvitation(token: AppDelegate.user?.access_token ?? "", email: email) { (respuesta) in
            
            btn.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
            
            if let body = respuesta as? [String:Any]
            {
                let status = body["status"] as? String ?? "error"
                
                if status == "success"
                {
                    self.mostrarAlerta(titulo: "inBi", msj: "The invitation was send", acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    })], bloqueCompletacion: nil)
                }else
                {
                    self.mostrarAlerta(titulo: "inBi", msj: "There was an error. Send the invitation again.", acciones:[UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                }
                
            }else
            {
                self.mostrarAlerta(titulo: "inBi", msj: "There was an error. Send the invitation again.", acciones:[UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InviteNewClientVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        if textField == userTxt
//        {
//            return emailTxt.becomeFirstResponder()
//        }
//
//        if textField == emailTxt
//        {
//            return phoneTxt.becomeFirstResponder()
//        }
//
//        if textField == phoneTxt
//        {
//            return phoneTxt.resignFirstResponder()
//        }
        
        return textField.resignFirstResponder()
    }
    
}
