//
//  PictureVC.swift
//  InBeauty
//
//  Created by ADMIN on 14/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import AVFoundation
import Cloudinary

class PictureVC: UIViewController,AVCapturePhotoCaptureDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var viewSelectFace: UIView!
    @IBOutlet weak var btnMultiTab: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnTakePhoto: UIButton!
    @IBOutlet weak var stackHeads: UIStackView!
    @IBOutlet weak var viewCamara: UIView!
    @IBOutlet weak var pictureImg: UIImageView!
    @IBOutlet weak var btnTrash: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet var btnsPhotos: [UIButton]!
    @IBOutlet weak var centerBtnCamera: NSLayoutConstraint!
    
    var camera = AVCaptureVideoPreviewLayer()
    let sesion = AVCaptureSession()
    var photoOutput : AVCapturePhotoOutput?
    var delegatePhoto : StatePhoto?
    //var nextBtnTag = 0
    //var tagBtn = 0
    var idBtn : String = "Before-left"
    
    var horizontalSelectedFace : NSLayoutConstraint!
    
    //var photosTakeBefore : [UIImage] = []
    //var photosTakeAfter : [UIImage] = []
    //var selectedIndexPhotos = 1
    
    //var completePhotosBefore = false
    //var completePhotosAfter = false
    var dictPhotosUpdated = [String:Bool]()
    
    //var dictPhotosBefore = [Int:UIImage]()
    //var dictPhotosAfter = [Int:UIImage]()
    
    let idsBefore = ["Before-left","Before-front","Before-right","Before-top","Before-back"]
    let idsAfter = ["After-left","After-front","After-right","After-top","After-back"]
    
    enum TimePhotos
    {
        case before
        case after
    }
    
    enum TakePhotos
    {
        case follow
        case done
    }
    
    enum StateService
    {
        case create
        case edit
    }
    
    var timePhotos : TimePhotos = .before
    var takePhotos : TakePhotos = .follow
    var stateService : StateService = .create
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        sesion.sessionPreset = AVCaptureSession.Preset.photo
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("No Camara")
            return
        }
        
        print("Accedemos a la camara")
        
        do
        {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            sesion.addInput(input)
        }
        catch
        {
            print("Error")
        }
        
        self.activeCamera()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        viewSelectFace.layer.cornerRadius = viewSelectFace.bounds.midY
       
        btnTakePhoto.setGradientBackground(colorOne: #colorLiteral(red: 0.847683065, green: 0.8679608861, blue: 1, alpha: 1), colorTwo: UIColor.white, colorTree: UIColor.white, y1: 0.005, y2: 0.65, indice: 0, opacity: 1)
        //updateBtnTakePhoto(dictPhotos: photosTakeBefore)
        
        btnTakePhoto.setTitle(nil, for: .normal)
        btnTakePhoto.layer.cornerRadius = btnTakePhoto.bounds.midY
        btnTakePhoto.layer.borderWidth = 6
        btnTakePhoto.layer.borderColor = UIColor.white.cgColor
        
        btnCamera.layer.cornerRadius = btnCamera.bounds.midY
        btnCamera.layer.borderWidth = 0.5
        btnCamera.layer.borderColor = UIColor.lightGray.cgColor
        
        btnMultiTab.layer.cornerRadius = btnMultiTab.bounds.midY
        btnMultiTab.layer.borderWidth = 0.5
        btnMultiTab.layer.borderColor = UIColor.lightGray.cgColor
        
        horizontalSelectedFace = self.view.constraints.first(where: {$0.identifier == "horizontalSelectedFace"})!
        
        let centerX = stackHeads.subviews.first!.bounds.midX - viewSelectFace.bounds.midX
        horizontalSelectedFace.constant = stackHeads.subviews.first!.frame.origin.x + centerX
        
//        for b in 0...stackHeads.subviews.count - 1
//        {
//            dictPhotosUpdated[b] = false
//        }
        
        //completePhotosBefore = (ServiceNameVC.photosBefore.count >= stackHeads.subviews.count)
        //completePhotosAfter = (ServiceNameVC.photosAfter.count >= stackHeads.subviews.count)
    
        //btnsPhotos.first!.isUserInteractionEnabled = completePhotosBefore
        //btnsPhotos.last!.isUserInteractionEnabled = stateService == .edit
    
        /*if completePhotosBefore
        {
            self.choosePhotosAction(btnsPhotos.last!)
        }*/
        self.showPicturesTaken(profile: "Before-left", dict: ServiceNameVC.photosBefore)
        self.choosePhotosAction(btnsPhotos.first!)
    }
    
    func activeCamera()
    {
        if sesion.outputs.isEmpty
        {
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecType.jpeg])], completionHandler: nil)
            
            sesion.addOutput(photoOutput!)
        
            self.viewCamara.layoutIfNeeded()
            
            camera = AVCaptureVideoPreviewLayer(session: sesion)
            camera.frame = viewCamara.layer.bounds
            camera.videoGravity = AVLayerVideoGravity.resizeAspectFill
            camera.connection?.videoOrientation = .portrait
    
            self.viewCamara.layer.insertSublayer(camera, at: 0)
            sesion.startRunning()
        }
    }
    
    @IBAction func backAction(_ btn: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func choosePhotosAction(_ btn: UIButton)
    {
        btnsPhotos.forEach { (btnLoop) in
            btnLoop.alpha = (btnLoop.tag == btn.tag) ? 1 : 0.5
        }
        
        //selectedIndexPhotos = btn.tag
        
        switch btn.tag {
        case 1:
            //stackHeads.subviews.forEach({$0.isUserInteractionEnabled = ($0.tag > 0) ? completePhotosBefore : true})
            timePhotos = .before
            for (i,btn) in stackHeads.subviews.enumerated()
            {
                btn.restorationIdentifier = idsBefore[i]
            }
            
            if let rangeBefore = idBtn.range(of: "After")
            {
                idBtn.replaceSubrange(rangeBefore, with: "Before")
            }
           
            showPicturesTaken(profile: idBtn, dict: ServiceNameVC.photosBefore)
            //btnTakePhoto.isHidden = !showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosBefore).tope
            //btnTrash.isHidden = btnTakePhoto.isHidden
        case 2:
            timePhotos = .after
            for (i,btn) in stackHeads.subviews.enumerated()
            {
                btn.restorationIdentifier = idsAfter[i]
            }
            
            if let rangeBefore = idBtn.range(of: "Before")
            {
                idBtn.replaceSubrange(rangeBefore, with: "After")
            }
            
            showPicturesTaken(profile: idBtn, dict: ServiceNameVC.photosAfter)
            
//            if completePhotosAfter
//            {
//                viewCamara.isHidden = !showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosAfter).tope
//                btnTakePhoto.isHidden = viewCamara.isHidden
//                btnTrash.isHidden = viewCamara.isHidden
//                //stackHeads.subviews.forEach ({$0.isUserInteractionEnabled = completePhotosAfter})
//            }else
//            {
//                let indexBtn = (nextBtnTag >= stackHeads.subviews.count) ? tagBtn : nextBtnTag
//                let btn = stackHeads.subviews[indexBtn] as! UIButton
//                selectedFaceAction(btn)
//
//                guard !photosTakeAfter.isEmpty else {
//                    stackHeads.subviews.forEach({$0.isUserInteractionEnabled = ($0.tag > 0) ? completePhotosAfter : true})
//                    return
//                }
//
//                stackHeads.subviews.forEach ({$0.isUserInteractionEnabled = ($0.tag <= photosTakeAfter.count)})
//            }
        default:
            break
        }
        
        print(idBtn)
    }
    
    func photosSelected(photos:[UIImage])
    {
//        btnTakePhoto.isHidden = self.showPicturesTaken(tag: tagBtn, photos: photos).tope
//        btnTrash.isHidden = self.showPicturesTaken(tag: tagBtn, photos: photos).updated
    }
    
    @IBAction func selectedFaceAction(_ btn: UIButton)
    {
        //tagBtn = btn.tag
        idBtn = btn.restorationIdentifier ?? "no id"
        print(btn.restorationIdentifier ?? "no id")
        //let updatedPhotos = dictPhotosUpdated.values.reduce(false, {return $0 || $1})
        
        translateViewSelectedFace(btn: btn)
    
        switch timePhotos {
        case .before:
            showPicturesTaken(profile: idBtn, dict: ServiceNameVC.photosBefore)
            //let tope = showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosBefore).tope
            //let updated = showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosBefore).updated
            
            /*if completePhotosBefore
            {
                btnTakePhoto.isHidden = true
                btnTrash.isHidden = true
            }else
            {
                btnTakePhoto.isHidden = (updatedPhotos) ? !updated : tope
            }*/
          
        case .after:
            showPicturesTaken(profile: idBtn, dict: ServiceNameVC.photosAfter)
            //let tope = showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosAfter).tope
            //let updated = showPicturesTaken(tag: tagBtn, photos: ServiceNameVC.photosAfter).updated
            
            /*if completePhotosAfter
            {
                btnTakePhoto.isHidden = true
                btnTrash.isHidden = true
            }else
            {
                btnTakePhoto.isHidden = (updatedPhotos) ? !updated : tope
                updateBtnTakePhoto(photos: photosTakeAfter)
            }*/
        }
    }
    
    func translateViewSelectedFace(btn:UIButton)
    {
        let centerX = btn.bounds.midX - viewSelectFace.bounds.midX
        self.horizontalSelectedFace.constant = btn.frame.origin.x + centerX
        
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
        }
    }
    
    func showPicturesTaken(profile:String,dict:[String:UIImage]) //-> (tope : Bool, updated : Bool)
    {
        guard let imgPhoto = dict[profile] else {
            self.btnTrash.isHidden = true
            self.viewCamara.isHidden = false
            self.pictureImg.image = nil
            return //(false,true)
        }
        
        //let updatedPhoto = dictPhotosUpdated[profile]!
        
        self.btnTrash.isHidden = false
        self.viewCamara.isHidden = true
        self.pictureImg.isHidden = false

        //let img = photos[tag]
        self.pictureImg.image = imgPhoto
        //return (photos.count != stackHeads.subviews.count,updatedPhoto)
    }
    
    @IBAction func restartPhotosAction(_ btn: UIButton)
    {
        //self.photosTake.removeAll()
//        self.nextBtnTag = 0
//        self.btnTakePhoto.setTitle(nil, for: .normal)
//        let nextBtn = stackHeads.subviews[nextBtnTag] as! UIButton
//        self.selectedFaceAction(nextBtn)
    }
    
    @IBAction func takePhotoAction(_ btn: UIButton)
    {
        takePhoto(dictPhotos: (timePhotos == .before) ? ServiceNameVC.photosBefore : ServiceNameVC.photosAfter)
    }
    
    func takePhoto(dictPhotos:[String:UIImage])
    {
//        let updatedPhotos = dictPhotosUpdated.values.reduce(false, {return $0 || $1})
//
//        if photos.count == stackHeads.subviews.count ,!updatedPhotos
//        {
//            print("Ya tomamos todas las fotos")
//            switch timePhotos
//            {
//            case .before:
//                ServiceNameVC.photosBefore = self.photosTakeBefore
//            case .after:
//                ServiceNameVC.photosAfter = self.photosTakeAfter
//            }
//
//
//        }else
//        {
//            self.viewCamara.isHidden = false
//            let settings = AVCapturePhotoSettings()
//            photoOutput?.capturePhoto(with: settings, delegate: self)
//        }
        
        if self.takePhotos == .done
        {
            self.navigationController?.popViewController(animated: true)
        }else
        {
            self.viewCamara.isHidden = false
            let settings = AVCapturePhotoSettings()
            photoOutput?.capturePhoto(with: settings, delegate: self)
        }
    }
    
    @IBAction func deletePhoto(_ btn: UIButton)
    {
        for btn in stackHeads.subviews
        {
            let profileId = btn.restorationIdentifier ?? "no id"
            switch idBtn
            {
            case profileId:
                
                switch timePhotos
                {
                case .before:
                    print("be")
                    self.updatePhoto(profile: profileId, timePhoto: "Before", dict: &ServiceNameVC.photosBefore)
                case .after:
                    print("af")
                    self.updatePhoto(profile: profileId, timePhoto: "After", dict: &ServiceNameVC.photosAfter)
                }
                break
            default:
                break
            }
        }
    }
    
    func updatePhoto(profile:String,timePhoto:String,dict: inout [String:UIImage])
    {
        //dictPhotosUpdated[indice] = true
        //let imgTemporary = UIImage()
        if let indice = dict.index(forKey: profile)
        {
            dict.remove(at: indice)
            self.btnTrash.isHidden = true
            self.viewCamara.isHidden = false
            self.delegatePhoto?.deletePhoto(id: profile, timePhoto: timePhoto)
        }
    
        //btnTakePhoto.isHidden = !showPicturesTaken(tag: indice, photos: photos).updated
        //updateBtnTakePhoto(photos: photos)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let img = photo.fileDataRepresentation()//,(nextBtnTag < stackHeads.subviews.count || updatedPhoto)
        {
            let newPhoto = UIImage(data: img)!
   
            switch timePhotos
            {
            case .before:
                self.savedPhoto(newPhoto: newPhoto,dict: &ServiceNameVC.photosBefore)
            case .after:
                self.savedPhoto(newPhoto: newPhoto,dict: &ServiceNameVC.photosAfter)
            }
        }
    }
    
    func savedPhoto(newPhoto:UIImage,dict: inout [String:UIImage])
    {
        /*
        if let updatedPhoto = dictPhotosUpdated[idBtn],updatedPhoto
        {
            print("Actualizamos la foto")
            dictPhotosUpdated[tagBtn] = false
            photos.remove(at: tagBtn)
            photos.insert(newPhoto, at: tagBtn)
            dict[tagBtn] = newPhoto
            if photos.count >= stackHeads.subviews.count
            {
                _ = showPicturesTaken(tag: tagBtn, photos: photos)
            }else
            {
                let nextBtn = stackHeads.subviews[nextBtnTag] as! UIButton
                self.translateViewSelectedFace(btn: nextBtn)
            }
        }else
        {
            photos.append(newPhoto)
         
            nextBtnTag = photos.count

            switch nextBtnTag
            {
            case 1,2,3,4:
                let nextBtn = stackHeads.subviews[nextBtnTag] as! UIButton
                nextBtn.isUserInteractionEnabled = true
                self.translateViewSelectedFace(btn: nextBtn)
            case 5:
                let nextBtn = stackHeads.subviews[stackHeads.subviews.count - 1] as! UIButton
                self.viewCamara.isHidden = !showPicturesTaken(tag: nextBtn.tag, photos: photos).tope
            default:
                break
            }
        }
        */
        takePhotos = .done
        
        if stateService == .edit
        {
            delegatePhoto?.newPhoto(id: idBtn)
        }
        
        btnTakePhoto.layer.borderWidth = 0
        btnTakePhoto.setTitle("Done", for: .normal)
        btnTakePhoto.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        if let subLayers = btnTakePhoto.layer.sublayers,let gradientLayer = subLayers.first {
            gradientLayer.isHidden = true
        }
        
        dict[idBtn] = newPhoto
        
        if dict.count < stackHeads.subviews.count
        {
            btnContinue.isHidden = false
            centerBtnCamera.constant = view.bounds.midX / 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
        
        showPicturesTaken(profile: idBtn, dict: dict)
        //updateBtnTakePhoto(photos: photos)
    }
    
    @IBAction func continueTakePhotosAction(_ btn: UIButton)
    {
        takePhotos = .follow
        btnTakePhoto.setTitle(nil, for: .normal)
        btnTakePhoto.layer.cornerRadius = btnTakePhoto.bounds.midY
        btnTakePhoto.layer.borderWidth = 6
        btnTakePhoto.layer.borderColor = UIColor.white.cgColor
        
        if let subLayers = btnTakePhoto.layer.sublayers,let gradientLayer = subLayers.first {
              gradientLayer.isHidden = false
        }
      
        btnContinue.isHidden = true
        centerBtnCamera.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func chooseMainPhotoAction(_ btn: UIButton)
    {
        if let _ = pictureImg.image
        {
            print("Elegimos como foto principal a \(idBtn)")
            delegatePhoto?.mainPhoto(id: idBtn)
        }
    }
    
    @IBAction func openLibraryPhotosAction(_ sender: UIButton)
    {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.delegate = self
        self.showDetailViewController(picker, sender: nil)
    }
    
    //MARK: UIIMAGE PICKER CONTROLLER DELEGATE
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imgPicker = info[.originalImage] as? UIImage else{return}
        
        print("Img Picker Controller")
        switch timePhotos {
        case .before:
            print("be")
            self.savedPhoto(newPhoto: imgPicker,dict: &ServiceNameVC.photosBefore)
        case .after:
            print("af")
            self.savedPhoto(newPhoto: imgPicker,dict: &ServiceNameVC.photosAfter)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateBtnTakePhoto(dictPhotos : [Int:UIImage])
    {
        let updatedPhotos = dictPhotosUpdated.values.reduce(false, {return $0 || $1})
        
        if dictPhotos.count == stackHeads.subviews.count && !updatedPhotos
        {
            btnTakePhoto.layer.borderWidth = 0
            btnTakePhoto.setTitle("Done", for: .normal)
            btnTakePhoto.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            
            guard let subLayers = btnTakePhoto.layer.sublayers,let gradientLayer = subLayers.first else {
                return
            }
            gradientLayer.isHidden = true
        }else
        {
            btnTakePhoto.setTitle(nil, for: .normal)
            btnTakePhoto.layer.cornerRadius = btnTakePhoto.bounds.midY
            btnTakePhoto.layer.borderWidth = 6
            btnTakePhoto.layer.borderColor = UIColor.white.cgColor

            guard let subLayers = btnTakePhoto.layer.sublayers,let gradientLayer = subLayers.first else {
                return
            }
            gradientLayer.isHidden = false
        }
    }    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*
    func codigodesechado()
    {
        let centerX = stackHeads.subviews[0].bounds.midX - viewSelectFace.bounds.midX
        switch horizontalSelectedFace.constant {
        case let constant where constant <= stackHeads.subviews[0].frame.origin.x + centerX:
            
            switch selectedIndexPhotos
            {
            case 1:
                self.updatePhoto(indice: 0, photos: &photosTakeBefore)
            case 2:
                self.updatePhoto(indice: 0, photos: &photosTakeAfter)
            default:
                break
            }
            
        case let constant where constant > stackHeads.subviews[0].frame.origin.x + centerX && constant <= stackHeads.subviews[1].frame.origin.x + centerX:
            
            switch selectedIndexPhotos
            {
            case 1:
                self.updatePhoto(indice: 1, photos: &photosTakeBefore)
            case 2:
                self.updatePhoto(indice: 1, photos: &photosTakeAfter)
            default:
                break
            }
            
        case let constant where constant > stackHeads.subviews[1].frame.origin.x + centerX && constant <= stackHeads.subviews[2].frame.origin.x + centerX:
            
            switch selectedIndexPhotos
            {
            case 1:
                self.updatePhoto(indice: 2, photos: &photosTakeBefore)
            case 2:
                self.updatePhoto(indice: 2, photos: &photosTakeAfter)
            default:
                break
            }
            
        case let constant where constant > stackHeads.subviews[2].frame.origin.x + centerX && constant <= stackHeads.subviews[3].frame.origin.x + centerX:
            
            switch selectedIndexPhotos
            {
            case 1:
                self.updatePhoto(indice: 3, photos: &photosTakeBefore)
            case 2:
                self.updatePhoto(indice: 3, photos: &photosTakeAfter)
            default:
                break
            }
            
        case let constant where constant > stackHeads.subviews[3].frame.origin.x + centerX && constant <= stackHeads.subviews[4].frame.origin.x + centerX:
            
            switch selectedIndexPhotos
            {
            case 1:
                self.updatePhoto(indice: 4, photos: &photosTakeBefore)
            case 2:
                self.updatePhoto(indice: 4, photos: &photosTakeAfter)
            default:
                break
            }
            
        default:
            break
        }
    }
    */
    /*
     switch tagBtn {
     case 0:
     
     switch selectedIndexPhotos
     {
     case 1:
     self.updatePhoto(indice: 0, photos: &photosTakeBefore)
     case 2:
     self.updatePhoto(indice: 0, photos: &photosTakeAfter)
     default:
     break
     }
     
     case 1:
     
     switch selectedIndexPhotos
     {
     case 1:
     self.updatePhoto(indice: 1, photos: &photosTakeBefore)
     case 2:
     self.updatePhoto(indice: 1, photos: &photosTakeAfter)
     default:
     break
     }
     case 2:
     
     switch selectedIndexPhotos
     {
     case 1:
     self.updatePhoto(indice: 2, photos: &photosTakeBefore)
     case 2:
     self.updatePhoto(indice: 2, photos: &photosTakeAfter)
     default:
     break
     }
     
     case 3:
     
     switch selectedIndexPhotos
     {
     case 1:
     self.updatePhoto(indice: 3, photos: &photosTakeBefore)
     case 2:
     self.updatePhoto(indice: 3, photos: &photosTakeAfter)
     default:
     break
     }
     
     case 4:
     
     switch selectedIndexPhotos
     {
     case 1:
     self.updatePhoto(indice: 4, photos: &photosTakeBefore)
     case 2:
     self.updatePhoto(indice: 4, photos: &photosTakeAfter)
     default:
     break
     }
     
     default:
     break
     }
     */
}


