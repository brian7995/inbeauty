//
//  ServiceNameVC.swift
//  InBeauty
//
//  Created by Brian Nuñez Rios on 5/11/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Cloudinary
import Alamofire

@objc protocol StatePhoto
{
    func deletePhoto(id:String,timePhoto:String)
    func newPhoto(id:String)
    func mainPhoto(id:String)
}

class ServiceNameVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CLUploaderDelegate,StatePhoto {
    
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var scrollViewService: UIScrollView!
    @IBOutlet weak var viewAddPictures: UIView!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var btnPicture: UIButton!
    @IBOutlet weak var btnDesign: UIButton!
    @IBOutlet weak var btnFormula: UIButton!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnReminder: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var heightViewTxt: NSLayoutConstraint!
    @IBOutlet weak var heightViewUsername: NSLayoutConstraint!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var recommendationTV: UITableView!
    @IBOutlet weak var formulationTV: UITableView!
    @IBOutlet weak var useTV: UITableView!
    @IBOutlet weak var reminderTV: UITableView!
    @IBOutlet weak var heightRecommendationTV: NSLayoutConstraint!
    @IBOutlet weak var heightFormulationTV: NSLayoutConstraint!
    @IBOutlet weak var heightUseTV: NSLayoutConstraint!
    @IBOutlet weak var heightReminderTV: NSLayoutConstraint!
    @IBOutlet weak var switchOnRecord: UISwitch!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var dateService: UILabel!
    @IBOutlet weak var widthBtnSave: NSLayoutConstraint!
    @IBOutlet weak var saveMsj: UILabel!
    @IBOutlet weak var viewSatisfaction: UIView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var forUsername: UILabel!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var imgViewPhotoPrincipal: UIImageView!
    @IBOutlet weak var imgClientView: UIImageView!
    @IBOutlet weak var style: UILabel!
    @IBOutlet weak var addPictures: UILabel!
    
    static var serviceClient = ServiceClient()
    static var photosBefore : [String:UIImage] = [:]
    static var photosAfter : [String:UIImage] = [:]
    static var designsService: [String:UIImage] = [:]
    
    var countTotalPhotos = 0
    var countUploaderPhotos = 0
    var service : OnService?
    var category : Category?
    var client : Client?
    var imgClient : UIImage?
    
    var totalMessage : String = "Type a message here..."
    var readMore = false
    
    var nameCategoryStr : String = ""
    
    var formulas = [ProductFormula]()
    var doDont = [DoDont]()
    var uses = [Use]()
    var reminders = [Reminder]()
    
    var uploaderCloud : CLUploader?
    var imgViews = [UIImageView]()
    
    enum UploadPhotos
    {
        case uploadingBefore
        case completeBefore
        case uploadingAfter
        case completeAfter
        case uploadingDesign
        case completeDesign
    }
    
    enum EditPhoto
    {
        case none
        case delete
        case new
        case onRecord
    }
    
    var uploadPhotos : UploadPhotos = .uploadingBefore
    var editPhoto : EditPhoto = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploaderCloud = CLUploader(AppDelegate.cloud, delegate: self)
        self.setupViews()
    
//        for i in 200...204
//        {
//            let imgView = view.viewWithTag(i) as! UIImageView
//            imgViews.append(imgView)
//        }
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        btnPicture.layer.borderWidth = 1.5
        btnPicture.layer.borderColor = #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1).self
        
        btnDesign.layer.borderWidth = 1
        btnDesign.layer.borderColor = UIColor.lightGray.cgColor
        
        btnFormula.layer.borderWidth = 1
        btnFormula.layer.borderColor = UIColor.lightGray.cgColor
        
        btnTask.layer.borderWidth = 1
        btnTask.layer.borderColor = UIColor.lightGray.cgColor
        
        btnReminder.layer.borderWidth = 1
        btnReminder.layer.borderColor = UIColor.lightGray.cgColor
        
        btnSave.layer.cornerRadius = btnSave.bounds.midY
        btnSave.titleLabel?.adjustsFontSizeToFitWidth = true
        
        imgViewPhotoPrincipal.setGradientBackground(colorOne: UIColor.black, colorTwo: UIColor.clear, colorTree: UIColor.black, y1: 0.0, y2: 1.0, indice: 0, opacity: 0.75)
        
        recommendationTV.dataSource = self
        formulationTV.dataSource = self
        useTV.dataSource = self
        reminderTV.dataSource = self
        
        recommendationTV.delegate = self
        formulationTV.delegate = self
        useTV.delegate = self
        reminderTV.delegate = self
        
        dateService.adjustsFontSizeToFitWidth = true
        
        viewSatisfaction.layer.cornerRadius = viewSatisfaction.bounds.midY
        viewSatisfaction.layer.shadowRadius = 5
        viewSatisfaction.layer.shadowOpacity = 0.5
        viewSatisfaction.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewSatisfaction.transform = CGAffineTransform(scaleX: 0, y: 0)
        viewSatisfaction.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(goWriteMessage(gesture:)))
        messageLabel.addGestureRecognizer(tap)
        
        if let category = self.category
        {
            btnFormula.isUserInteractionEnabled = (category.formulacion == 1)
            btnDesign.isUserInteractionEnabled = (category.disenio == 1)
            
            btnFormula.alpha = (category.formulacion == 1) ? 1 : 0.5
            btnDesign.alpha = (category.disenio == 1) ? 1 : 0.5
            
            nameCategory.text = category.nombre
        }
        
        if let service = self.service
        {
            //btnBack.isHidden = false
            nameService.text = service.author_tag
            dateService.text = service.fecha_recordatorio
            style.text = service.style
            
            let dictSatisfaction : [Int:String] = [1:"😅",2:"🤨",3:"😃",4:"😕",5:"😓"]
            btnSelect.setTitle(dictSatisfaction[service.satisfaccion], for: .normal)
            btnSelect.tag = service.satisfaccion
            switchOnRecord.isOn = (service.status == 1)
            
//            print(service.categoria.formulacion)
//            print(service.categoria.disenio)
            
            btnFormula.isUserInteractionEnabled = (service.categoria.formulacion == 1)
            btnDesign.isUserInteractionEnabled = (service.categoria.disenio == 1)
            
            btnFormula.alpha = (service.categoria.formulacion == 1) ? 1 : 0.5
            btnDesign.alpha = (service.categoria.disenio == 1) ? 1 : 0.5
            
            messageLabel.text = service.mensaje
            forUsername.text = "for " + service.cliente.name
            nameCategory.text = service.categoria.nombre
            
            for (i,reminder) in service.reminder.enumerated()
            {
                service.reminder[i].numero = reminder.numero_restante
            }
            
            imgClientView.cargarImgDesdeURL(urlImg: service.cliente.foto_url) {
            }
            
            imgViewPhotoPrincipal.image = imgClient
            //let perfiles = ["left":0,"front":1,"right":2,"top":3,"back":4]
        }else
        {
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US")
            dateFormatter.dateStyle = .medium
            dateService.text = dateFormatter.string(from: date)
            
            forUsername.text = "for " + (client?.name ?? "no name !?")
            nameService.text = ServiceNameVC.serviceClient.author_tag
            style.text = ServiceNameVC.serviceClient.style
            nameCategory.text = nameCategoryStr
            imgClientView.image = imgClient
            //btnBack.isHidden = true
        }
        
        let tapProfile = UITapGestureRecognizer(target: self, action: #selector(goToProfile))
        imgClientView.addGestureRecognizer(tapProfile)
    }
    
    @objc func goWriteMessage(gesture:UIGestureRecognizer)
    {
        print("Tap Go")
        self.performSegue(withIdentifier: "writeMsjSegue", sender: nil)
    }
    
    @objc func goToProfile()
    {
        let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let profileVC = profileStoryboard.instantiateViewController(withIdentifier: "ProfessionalID") as! ProfileVC
        profileVC.myProfile = .client
        profileVC.client = self.client ?? self.service?.cliente
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let service = service
        {
            formulas = service.formula?.productos ?? [ProductFormula]()
            doDont = service.recomendaciones?.dodont ?? [DoDont]()
            uses = service.recomendaciones?.use ?? [Use]()
            reminders = service.reminder
            
            self.countTotalPhotos = 0
            
            let photosBefore = service.fotos.filter({return $0.perfil.contains("Before")})
            self.setupPhotosService(photos: photosBefore, before: true)
            self.setupDesignPhotos(service: service)
        }else
        {
            formulas = ServiceNameVC.serviceClient.formula?.productos ?? [ProductFormula]()
            doDont = ServiceNameVC.serviceClient.recomendaciones?.dodont ?? [DoDont]()
            uses = ServiceNameVC.serviceClient.recomendaciones?.use ?? [Use]()
            reminders = ServiceNameVC.serviceClient.reminder
        }
       
        formulas.reverse()
        doDont.reverse()
        uses.reverse()
        reminders.reverse()
    
        heightFormulationTV.constant = 250 * CGFloat(formulas.count)
        heightRecommendationTV.constant = 75 * CGFloat(doDont.count)
        heightUseTV.constant = 120 * CGFloat(uses.count)
        heightReminderTV.constant = 100 * CGFloat(reminders.count)
        
        recommendationTV.layoutIfNeeded()
        useTV.layoutIfNeeded()
        reminderTV.layoutIfNeeded()
        formulationTV.layoutIfNeeded()
        
        recommendationTV.reloadData()
        formulationTV.reloadData()
        useTV.reloadData()
        reminderTV.reloadData()
        
        heightViewUsername.constant = 800 + (heightFormulationTV.constant + heightRecommendationTV.constant + heightUseTV.constant + heightReminderTV.constant)
       
        self.totalMessage = service?.mensaje ?? ServiceNameVC.serviceClient.mensaje
        self.messageLabel.text = self.totalMessage
        
        if switchOnRecord.isOn && ServiceNameVC.photosAfter.isEmpty
        {
            switchOnRecord.isOn = false
        }
        
        self.countUploaderPhotos = 0
        self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count + ServiceNameVC.designsService.count
        print("Hay un total de \(countTotalPhotos) fotos para subir")
    }
    
    func setupPhotosService(photos:[Picture],before:Bool)
    {
        guard photos.count > 0 else {
            addPictures.text = "add pictures"
            btnPicture.alpha = 1
            btnPicture.isUserInteractionEnabled = true
            return
        }
        
        addPictures.text = "loading photos..."
        btnPicture.alpha = 0.5
        btnPicture.isUserInteractionEnabled = false
        
        if !before && photos.isEmpty
        {
            addPictures.text = "add pictures"
            btnPicture.alpha = 1
            btnPicture.isUserInteractionEnabled = true
//            self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count
//            print("Hay un total de \(countTotalPhotos) fotos para subir")
        }
        
        for (indice,photo) in photos.enumerated()
        {
            if let imgFromCache = imgCache.object(forKey: photo.url_fotos as NSString)
            {
                if before
                {
                    print("Obtenemos una foto del before : \(photo.perfil)")
                    ServiceNameVC.photosBefore[photo.perfil] = imgFromCache
                    
                    if indice == photos.count - 1
                    {
                        let photosAfter = self.service!.fotos.filter({return $0.perfil.contains("After")})
                        self.setupPhotosService(photos: photosAfter, before: false)
                    }
                    
                }else
                {
                    print("Obtenemos una foto del after : \(photo.perfil)")
                    ServiceNameVC.photosAfter[photo.perfil] = imgFromCache
                    
                    if indice == photos.count - 1
                    {
                        addPictures.text = "add pictures"
                        btnPicture.isUserInteractionEnabled = true
                        btnPicture.alpha = 1
                        //self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count
                        //print("Hay un total de \(countTotalPhotos) fotos para subir")
                    }
                }
                continue
            }
            
            Alamofire.request(photo.url_fotos).responseData { (respuesta) in
                
                guard let data = respuesta.data else
                {
                    print("No tenemos  data 😱")
                    return
                }
                
                guard let img = UIImage(data: data) else {return}
                
                if before
                {
                    print("Obtenemos una foto del before : \(photo.perfil)")
                    ServiceNameVC.photosBefore[photo.perfil] = img
                    
                    if indice == photos.count - 1
                    {
                        let photosAfter = self.service!.fotos.filter({return $0.perfil.contains("After")})
                        self.setupPhotosService(photos: photosAfter, before: false)
                    }
                }else
                {
                    print("Obtenemos una foto del after : \(photo.perfil)")
                    ServiceNameVC.photosAfter[photo.perfil] = img
                    
                    if indice == photos.count - 1
                    {
                        self.addPictures.text = "add pictures"
                        self.btnPicture.alpha = 1
                        self.btnPicture.isUserInteractionEnabled = true
                        //self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count
                        //print("Hay un total de \(self.countTotalPhotos) fotos para subir")
                    }
                }
            
                imgCache.setObject(img, forKey: photo.url_fotos as NSString)
            }
        }
    }
    
    func setupDesignPhotos(service:OnService)
    {
        guard service.componentes.count > 0 else {return}
        btnDesign.setTitle("Loading Designs...", for: .normal)
        btnDesign.isUserInteractionEnabled = false
        
        for (indice,design) in service.componentes.enumerated()
        {
            if let imgFromCache = imgCache.object(forKey: design.url_componente as NSString)
            {
                ServiceNameVC.designsService[design.perfil] = imgFromCache
                if indice == service.componentes.count - 1
                {
                    btnDesign.setTitle("+ Add Design", for: .normal)
                    btnDesign.isUserInteractionEnabled = true
                }
                continue
            }
            
            request(design.url_componente).responseData { (respuesta) in
                
                guard let data = respuesta.data else
                {
                    print("No tenemos  data 😱")
                    return
                }
                
                guard let img = UIImage(data: data) else {return}
                ServiceNameVC.designsService[design.perfil] = img
                imgCache.setObject(img, forKey: design.url_componente as NSString)
                
                if indice == service.componentes.count - 1
                {
                    self.btnDesign.setTitle("+ Add Design", for: .normal)
                    self.btnDesign.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if ServiceNameVC.designsService.count > 0
        {
            //imgViews.first?.image = ServiceNameVC.designsService["Face-Left"]
            /*for (profile,photo) in ServiceNameVC.designsService
            {
                let imgProfile = imgViews.first(where: {return $0.restorationIdentifier == profile})
                imgProfile?.image = before
            }*/
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    @IBAction func readMoreAction(_ btn: UIButton)
    {
        self.readMore = !readMore
        btn.setTitle((readMore) ? "Read less..." : "Read more..."  , for: .normal)
        
        let newHeightViewTxt = (CGFloat(totalMessage.count) * 0.5) + 100
        let newHeightViewUsername = (CGFloat(totalMessage.count) * 0.5) + heightViewUsername.constant
        
        UIView.animate(withDuration: 0.25) {
            
            self.heightViewTxt.constant = (self.readMore && self.totalMessage != "Type a message here...") ? newHeightViewTxt : 100
            self.heightViewUsername.constant = (self.readMore && self.totalMessage != "Type a message here...") ? newHeightViewUsername : self.heightViewUsername.constant
            
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func openViewSatisfaction(_ btn: UIButton)
    {
        viewSatisfaction.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .init(), animations: {
            self.viewSatisfaction.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (bool) in
            
        }
    }
    
    @IBAction func chooseSatisfaction(_ btn: UIButton)
    {
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .init(), animations: {
            self.viewSatisfaction.transform = CGAffineTransform(scaleX: 0, y: 0)
            self.view.layoutIfNeeded()
        }) { (bool) in
            self.btnSelect.setTitle(btn.titleLabel?.text, for: .normal)
            self.btnSelect.tag = btn.tag
            self.viewSatisfaction.isHidden = true
        }
    }
    
    //MARK: CHANGE STATUS ON RECORD
    @IBAction func changeStatusService(_ sender: UISwitch)
    {
        print("change status")
        if sender.isOn && ServiceNameVC.photosAfter.isEmpty
        {
            self.mostrarAlerta(titulo: "inBi", msj: "You must have at least one photo of the after to change the status of the service.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                sender.isOn = false
            })], bloqueCompletacion: nil)
        }
    }
    
    //MARK: Table View Edit
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case recommendationTV:
            return doDont.count
        case formulationTV:
            return formulas.count
        case useTV:
            return uses.count
        case reminderTV:
            return reminders.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case recommendationTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "recommendationCell", for: indexPath)
            let viewBG = cell.viewWithTag(100)!
            let doDontLabel = cell.viewWithTag(101) as! UILabel
            let task = cell.viewWithTag(102) as! UILabel
            
            var tipo = doDont[indexPath.row].tipo
            tipo.removeFirst()
            tipo.insert("D", at: tipo.startIndex)
            doDontLabel.text = tipo
            
            viewBG.backgroundColor = (tipo == "Do") ? #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1) : #colorLiteral(red: 0.2232428193, green: 0.2219871581, blue: 0.2965724468, alpha: 1)
            doDontLabel.clipsToBounds = true
            doDontLabel.layer.cornerRadius = doDontLabel.bounds.midX
            doDontLabel.adjustsFontSizeToFitWidth = true
         
            task.text = doDont[indexPath.row].tarea
            return cell
        case formulationTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "formulationCell", for: indexPath) as! FormulaTVCell
            let formula = formulas[indexPath.row]
            cell.buildProduct(product: formula)
            return cell
        case useTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "useCell", for: indexPath)
            let use = cell.viewWithTag(101) as! UILabel
            let frecuency = cell.viewWithTag(102) as! UILabel
            let name = cell.viewWithTag(103) as! UILabel
            let imgView = cell.viewWithTag(104) as! UIImageView
            
            imgView.layer.cornerRadius = imgView.bounds.midX * 0.25
            
            use.layer.cornerRadius = use.bounds.midX
            use.text = "Use"
            
            imgView.cargarImgDesdeURL(urlImg: uses[indexPath.row].producto.url_foto) {
            }
            
            frecuency.text = uses[indexPath.row].frecuencia
            name.adjustsFontSizeToFitWidth = true
            name.text = uses[indexPath.row].producto.nombre + "\n" + (uses[indexPath.row].producto.descripcion ?? "")
            
            return cell
        case reminderTV:
            let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath)
            
            let imgView = cell.viewWithTag(101) as! UIImageView
            imgView.layer.cornerRadius = imgView.bounds.midX
            imgView.image = self.imgClientView.image
            
            let msj = cell.viewWithTag(102) as! UILabel
            msj.text = reminders[indexPath.row].mensaje
            
            let time = cell.viewWithTag(104) as! UILabel
            time.adjustsFontSizeToFitWidth = true
        
            //let timeRest = reminders[indexPath.row].tiempo_restante
            time.text = "IN " + reminders[indexPath.row].tiempo.uppercased()
//            if timeRest == "dias" || timeRest == "semanas" || timeRest == "meses"
//            {
//                let timeStr = "In \()"
//
//            }else
//            {
//                time.text = timeRest.uppercased()
//            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteTxt = UILabel()
        deleteTxt.font = UIFont(name:"Futura Bold",size:17.0)
        deleteTxt.font = UIFont.boldSystemFont(ofSize: 17)
        deleteTxt.text = "Delete ✖︎"
        let rowAction = UITableViewRowAction(style: .destructive, title: deleteTxt.text) { (tbrow, index) in
            //print("Row e Index respectivamente : ",tbrow," ",index)
            switch tableView {
            case self.recommendationTV:
                self.doDont.remove(at: indexPath.row)
                self.service?.recomendaciones?.dodont.remove(at: indexPath.row)
                
                if !(ServiceNameVC.serviceClient.recomendaciones?.dodont.isEmpty ?? true)
                {
                    ServiceNameVC.serviceClient.recomendaciones?.dodont.remove(at: indexPath.row)
                }
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.heightRecommendationTV.constant = 75 * CGFloat(self.doDont.count)
                    self.view.layoutIfNeeded()
                })
                
            case self.formulationTV:
                self.formulas.remove(at: indexPath.row)
                self.service?.formula?.productos.remove(at: indexPath.row)
                
                if !(ServiceNameVC.serviceClient.formula?.productos.isEmpty ?? true)
                {
                    ServiceNameVC.serviceClient.formula?.productos.remove(at: indexPath.row)
                }
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.heightFormulationTV.constant = 250 * CGFloat(self.formulas.count)
                    self.view.layoutIfNeeded()
                })
            case self.useTV:
                self.uses.remove(at: indexPath.row)
                self.service?.recomendaciones?.use.remove(at: indexPath.row)
                
                if !(ServiceNameVC.serviceClient.recomendaciones?.use.isEmpty ?? false)
                {
                    ServiceNameVC.serviceClient.recomendaciones?.use.remove(at: indexPath.row)
                }
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.heightUseTV.constant = 120 * CGFloat(self.uses.count)
                    self.view.layoutIfNeeded()
                })
            case self.reminderTV:
                self.reminders.remove(at: indexPath.row)
                self.service?.reminder.remove(at: indexPath.row)
                
                if !ServiceNameVC.serviceClient.reminder.isEmpty
                {
                    ServiceNameVC.serviceClient.reminder.remove(at: indexPath.row)
                }
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.heightReminderTV.constant = 100 * CGFloat(self.reminders.count)
                    self.view.layoutIfNeeded()
                })
            default:
                break
            }
            
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        rowAction.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        
        return [rowAction]
        
    }
    
    //MARK: State Photo
    
    func newPhoto(id:String) {
     
        guard let editService = self.service else {return}

        print("Tomamos una nueva foto")
        self.editPhoto = .new
        
        if let indexPhoto = editService.fotos.firstIndex(where: {return $0.perfil == id})
        {
            editService.fotos.remove(at: indexPhoto)
        }
        
        if let indexDesign = editService.componentes.firstIndex(where: {return $0.perfil == id})
        {
            editService.componentes.remove(at: indexDesign)
        }
    }
    
    func deletePhoto(id: String, timePhoto: String) {
        guard let editService = self.service else {return}
     
        if let indexPhoto = editService.fotos.firstIndex(where: {return $0.perfil == id})
        {
            print("Borramos una foto del perfil : \(id)")
            print("Indice de la foto que vamos a borrar :\(indexPhoto)")
            editService.fotos.remove(at: indexPhoto)
        }
        
        if let indexDesign = editService.componentes.firstIndex(where: {return $0.perfil == id})
        {
            print("Borramos el diseño : \(id)")
            print("Indice del diseño que vamos a borrar :\(indexDesign)")
            editService.componentes.remove(at: indexDesign)
        }
    }
    
    func mainPhoto(id: String)
    {
        if let editService = self.service
        {
            editService.foto_principal = id
        }else
        {
            ServiceNameVC.serviceClient.foto_principal = id
        }
        
        imgViewPhotoPrincipal.image = ServiceNameVC.photosBefore[id] ?? ServiceNameVC.photosAfter[id] ?? ServiceNameVC.photosBefore["Before-left"]
    }
    
    //MARK: CLUploader Delegate
    func uploaderSuccess(_ result: [AnyHashable : Any]!, context: Any!) {
        let publicId = result["public_id"] as? String ?? "noid"
        let version = result["version"] as? Int ?? -12
        let urlPhoto = AppDelegate.cloud?.url("v" + String(version) + "/" + publicId) ?? "nada"
        let perfil = urlPhoto.components(separatedBy: "_").last ?? ""

        self.countUploaderPhotos += 1
        
        //print("Upload Photos : \(countUploaderPhotos)/\(countTotalPhotos)")
        //print("Exito en Uploader \(perfil)",urlPhoto)
        
        if let editService = self.service
        {
            switch self.uploadPhotos {
            case .uploadingAfter,.uploadingBefore:
               
                let newPicture = Picture(perfil: perfil, url_fotos: urlPhoto)
                if let indexUpdated = editService.fotos.firstIndex(where: {return $0.perfil == perfil})
                {
                    print("Actualizamos foto")
                    editService.fotos[indexUpdated].url_fotos = urlPhoto
                }else
                {
                    print("Añadimos foto")
                    editService.fotos.append(newPicture)
                }
            case .uploadingDesign:
                //
                let newComponent = Component(perfil: perfil, url_componente: urlPhoto)
                if let indexUpdated = editService.componentes.firstIndex(where: {return $0.perfil == perfil})
                {
                    print("Actualizamos diseño")
                    editService.componentes[indexUpdated].url_componente = urlPhoto
                }else
                {
                    print("Añadimos diseño")
                    editService.componentes.append(newComponent)
                }
            default:
                break
            }
            
            if editService.foto_principal == perfil
            {
                print("la foto principal es la del perfil : \(mainPhoto)")
                editService.foto_principal = urlPhoto
            }
            
            if self.countUploaderPhotos == self.countTotalPhotos
            {
                print("Editar servicio : ya tenemos todas las fotos para subirlas")
                updatedService(service: editService)
            }
           
        }else
        {
            switch self.uploadPhotos {
            case .uploadingBefore,.uploadingAfter:
                let newPicture = Picture(perfil: perfil, url_fotos: urlPhoto)
                ServiceNameVC.serviceClient.fotos.append(newPicture)
            case .uploadingDesign:
                let newComponent = Component(perfil: perfil, url_componente: urlPhoto)
                ServiceNameVC.serviceClient.componentes.append(newComponent)
            default:
                break
            }
            
            let mainPhoto = ServiceNameVC.serviceClient.foto_principal
            
            if mainPhoto.count > 0 && mainPhoto == perfil
            {
                print("la foto principal es la del perfil : \(mainPhoto)")
                ServiceNameVC.serviceClient.foto_principal = urlPhoto
            }else if perfil == "Before-left"
            {
                print("No se escogio foto principal asi que sera la del Before-left")
                ServiceNameVC.serviceClient.foto_principal = urlPhoto
            }
            
            //ServiceNameVC.serviceClient.componentes.append(newComponent)
            if self.countUploaderPhotos == self.countTotalPhotos
            {
                print("Crear servicio : ya tenemos todas las fotos para subirlas")
                saveServiceWS()
            }
        }
    
        switch self.countUploaderPhotos
        {
        case ServiceNameVC.photosBefore.count:
            if ServiceNameVC.photosAfter.count > 0
            {
                print("Subimos la foto del After")
                self.uploadPhotos = .uploadingAfter
                getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.photosAfter, type: "After")
            }
            else if ServiceNameVC.designsService.count > 0
            {
                print("Subimos la foto del diseño porque no hay fotos de after")
                self.uploadPhotos = .uploadingDesign
                getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.designsService, type: "Design")
            }
        case ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count:
            if self.uploadPhotos == .uploadingBefore || self.uploadPhotos == .uploadingAfter
            {
                print("Subimos la foto del diseño porque ya acabamos con before y after")
                self.uploadPhotos = .uploadingDesign
                getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.designsService, type: "Design")
            }
        default:
            break
        }
    }
    
    func uploaderError(_ result: String!, code: Int, context: Any!) {
        print("Error uploaded",result,code)
        self.saveMsj.text = "Save"
        self.btnSave.backgroundColor = #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1)
        self.btnSave.isUserInteractionEnabled = true
        self.viewAddPictures.isUserInteractionEnabled = true
        self.viewUsername.isUserInteractionEnabled = true
        self.mostrarAlerta(titulo: "Inbi", msj:"Failed to upload photo. Try again.", acciones: [UIAlertAction(title: "Ok", style: .cancel, handler: nil)], bloqueCompletacion: nil)
    }
    
    func uploaderProgress(_ bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int, context: Any!) {
        //let pro : CLongDouble = CLongDouble(totalBytesWritten/totalBytesExpectedToWrite)
        //print("Uploader Progress",bytesWritten,totalBytesWritten,totalBytesExpectedToWrite)
    }
    
    //MARK: SAVE AND UPDATED CLIENT SERVICE SAVE
    
    @IBAction func saveServiceAction(_ btn: UIButton)
    {
        guard self.btnSelect.tag > 0 else {
            self.btnSelect.transform = CGAffineTransform(scaleX: 0, y: 0)
            self.scrollViewService.scrollRectToVisible(self.btnSelect.frame, animated: true)
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .init(), animations: {
                self.btnSelect.transform = CGAffineTransform(scaleX: 1, y: 1)
            }) { (bool) in
            }
            return
        }
        
        btn.isUserInteractionEnabled = false
        viewAddPictures.isUserInteractionEnabled = false
        viewUsername.isUserInteractionEnabled = false
        
        saveMsj.text = "Saving"
        
        self.startAnimationSavingService(btn: btn)

        if self.service != nil
        {
            self.service!.satisfaccion = self.btnSelect.tag
            self.service!.mensaje = self.messageLabel.text!
            self.service!.status = (switchOnRecord.isOn) ? 1 : 0
            
            for (i,_) in self.service!.reminder.enumerated()
            {
                let tmpRest = self.service!.reminder[i].tiempo_restante
                
                if tmpRest == "dias" || tmpRest == "semanas" || tmpRest == "meses"
                {
                    self.service!.reminder[i].tiempo = tmpRest
                }
            }
            
            print("editamos el servicio")
    
            if self.editPhoto == .new
            {
                let photosBefore = self.service!.fotos.filter({return $0.perfil.contains("Before")})
                let newPhotosBefore = ServiceNameVC.photosBefore.filter { (profile,_) -> Bool in
                    return !photosBefore.contains(where: {return $0.perfil == profile})
                }
                //print(newPhotosBefore)
                
                let photosAfter = self.service!.fotos.filter({return $0.perfil.contains("After")})
                let newPhotosAfter = ServiceNameVC.photosAfter.filter { (profile,_) -> Bool in
                    return !photosAfter.contains(where: {return $0.perfil == profile})
                }
                //print(newPhotosAfter)
                
                let newPhotosDesign = ServiceNameVC.designsService.filter { (profile,_) -> Bool in
                    return !(service!.componentes.contains(where: {return $0.perfil == profile}))
                }
                
                //print(newPhotosDesign)
                
                ServiceNameVC.photosBefore = newPhotosBefore
                ServiceNameVC.photosAfter = newPhotosAfter
                ServiceNameVC.designsService = newPhotosDesign
                
                self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count + ServiceNameVC.designsService.count
                print("Editar servicio : hay un total de \(countTotalPhotos) fotos para subir")
                
                if ServiceNameVC.photosBefore.count > 0
                {
                    getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.photosBefore, type: "Before")
                }else if ServiceNameVC.photosAfter.count > 0
                {
                    getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.photosAfter, type: "After")
                }else if ServiceNameVC.designsService.count > 0
                {
                    getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.photosAfter, type: "Design")
                }
            }else
            {
                if let mainPhoto = self.service?.foto_principal
                {
                    if let picture = self.service!.fotos.first(where: {return $0.perfil == mainPhoto})
                    {
                        print("Cambiamos la foto a principal a la del perfil : \(picture.perfil)")
                        self.service!.foto_principal = picture.url_fotos
                    }
                }
                
                self.updatedService(service: self.service!)
            }
        }else
        {
            self.countTotalPhotos = ServiceNameVC.photosBefore.count + ServiceNameVC.photosAfter.count + ServiceNameVC.designsService.count
            print("Crear servicio : hay un total de \(countTotalPhotos) fotos para subir")
            self.saveService()
        }
    }
    
    func updatedService(service:OnService)
    {
        guard let data = try? JSONEncoder.init().encode(service) else {
            print("No se pudo encodear la clase 😱")
            return
        }
        
        inBiServices.instancia.updateCustomersService(id: service.id, token: AppDelegate.user?.access_token ?? "", update: data) { (respuesta) in
            
            guard let status = respuesta as? String,status == "success" else {
                self.saveMsj.text = "Save"
                self.btnSave.backgroundColor = #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1)
                self.btnSave.isUserInteractionEnabled = true
                self.viewAddPictures.isUserInteractionEnabled = true
                self.viewUsername.isUserInteractionEnabled = true
                self.mostrarAlerta(titulo: "inBi", msj: "An error occurred while saving the service. Try again.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                return
            }
            
            ServiceNameVC.photosBefore.removeAll()
            ServiceNameVC.photosAfter.removeAll()
            ServiceNameVC.designsService.removeAll()
            self.btnSave.isUserInteractionEnabled = true
            self.viewAddPictures.isUserInteractionEnabled = true
            self.viewUsername.isUserInteractionEnabled = true
            self.performSegue(withIdentifier: "goBoard", sender: true)
        }
    }
    
    func saveService()
    {
        ServiceNameVC.serviceClient.satisfaccion = self.btnSelect.tag
        ServiceNameVC.serviceClient.categoria_id = category?.id ?? -1
        let countMessage = self.messageLabel.text?.count ?? 0
        ServiceNameVC.serviceClient.mensaje = (countMessage > 0) ? self.messageLabel.text! : "Type a message here..."
        ServiceNameVC.serviceClient.cliente_id = self.client?.id ?? "no id client"
        ServiceNameVC.serviceClient.status = switchOnRecord.isOn
        
        //print("Guardando por primera vez",ServiceNameVC.serviceClient.imprimirData())
        //self.saveServiceWS()
        if ServiceNameVC.photosBefore.count > 0
        {
            getUrlPhotosOfCloudinary(dictPhotos: ServiceNameVC.photosBefore, type: "Before")
        }else
        {
            self.mostrarAlerta(titulo: "inBi", msj: "You must take at least one photo of the before to save the service.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.saveMsj.text = "Save"
                self.btnSave.backgroundColor = #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1)
                self.btnSave.isUserInteractionEnabled = true
                self.viewAddPictures.isUserInteractionEnabled = true
                self.viewUsername.isUserInteractionEnabled = true
            })], bloqueCompletacion: nil)
        }
    }
    
    //MARK: GET URL PHOTOS ,UPLOAD CLOUDINARY
    func getUrlPhotosOfCloudinary(dictPhotos:[String:UIImage],type:String)
    {
        //let perfiles = [0:"left",1:"front",2:"right",3:"top",4:"back"]
        let clientPhoto = self.client ?? self.service?.cliente
        
        for (profile,photo) in dictPhotos
        {
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "es_PE")
            dateFormatter.dateFormat = "E,d_MMM_yyyy_HH:mm:ss"
            let dateStr = dateFormatter.string(from: date)
            
            switch type
            {
            case "Before","After":
                if let jpgPhoto = photo.jpegData(compressionQuality: 0.25),let client = clientPhoto
                {
                    //print("subimos las fotos al cloudinary_\(profile)")
                    //let idProfile = time + "-" + profile
                    let idPhoto = dateStr + "_" + profile
                    self.uploaderCloud?.upload(jpgPhoto, options: ["folder":"CustomerPhotos/\(client.email)/\(type)/","public_id":idPhoto])
                }
            case "Design":
                if let jpgPhoto = photo.pngData(),let client = clientPhoto
                {
                    //print("subimos las fotos al cloudinary_\(profile)")
                    //let idProfile = time + "-" + profile
                    let idPhoto = dateStr + "_" + profile
                    self.uploaderCloud?.upload(jpgPhoto, options: ["folder":"CustomerPhotos/\(client.email)/\(type)/","public_id":idPhoto])
                }
            default:
                break
            }
        }
    }
    
    func saveServiceWS()
    {
        guard let data = try? JSONEncoder.init().encode(ServiceNameVC.serviceClient) else {
            print("No se pudo encodear la clase 😱")
            return
        }
        
        inBiServices.instancia.saveCustomerService(token: AppDelegate.user?.access_token ?? "", service: data) { (respuesta) in
            guard let status = respuesta as? String,status == "success" else {
                self.saveMsj.text = "Save"
                self.btnSave.backgroundColor = #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1)
                self.btnSave.isUserInteractionEnabled = true
                self.viewAddPictures.isUserInteractionEnabled = true
                self.viewUsername.isUserInteractionEnabled = true
                self.mostrarAlerta(titulo: "inBi", msj: "An error occurred while saving the service. Try again.", acciones: [UIAlertAction(title: "Ok", style: .default, handler: nil)], bloqueCompletacion: nil)
                return
            }
            
            AppDelegate.user?.servicios += 1
            ServiceNameVC.serviceClient = ServiceClient()
            
            ServiceNameVC.photosBefore.removeAll()
            ServiceNameVC.photosAfter.removeAll()
            ServiceNameVC.designsService.removeAll()
            
            self.btnSave.isUserInteractionEnabled = true
            self.viewAddPictures.isUserInteractionEnabled = true
            self.viewUsername.isUserInteractionEnabled = true
            self.performSegue(withIdentifier: "goBoard", sender: true)
        }
    }
    
    func startAnimationSavingService(btn:UIButton)
    {
        UIView.animate(withDuration: 0.25) {
            //self.widthBtnSave.constant = 10
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.saveMsj.text?.append(".")
            let color = (btn.backgroundColor == #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1))
            btn.backgroundColor = (color) ? #colorLiteral(red: 0.8049458234, green: 0.7885595235, blue: 0.9686274529, alpha: 1) : #colorLiteral(red: 0.6101320386, green: 0.5976423621, blue: 0.9998722672, alpha: 1)
        }) { (bool) in
            
            if !btn.isUserInteractionEnabled
            {
                self.startAnimationSavingService(btn: btn)
            }else
            {
                //self.widthBtnSave.constant = 0
                //self.view.layoutIfNeeded()
            }
            
            if self.saveMsj.text == "Saving...."
            {
                self.saveMsj.text = "Saving"
            }
        }
    }
    
    @IBAction func goBackAction(_ btn: UIButton)
    {
        if let _ = self.service
        {
            self.performSegue(withIdentifier: "goBoard", sender: true)
        }else
        {
            self.performSegue(withIdentifier: "goCreateService", sender: self)
            //self.navigationController?.popViewController(animated: true)
        }
        
        ServiceNameVC.photosBefore.removeAll()
        ServiceNameVC.photosAfter.removeAll()
        ServiceNameVC.designsService.removeAll()
    }

    @IBAction func volverServiceName(_ segue:UIStoryboardSegue)
    {
        Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false) { (timer) in
            
            switch segue.source {
            case is FormulationsVC:
                self.scrollViewService.scrollRectToVisible(self.formulationTV.frame, animated: true)
            case is DoRecommendationVC:
                self.scrollViewService.scrollRectToVisible(self.recommendationTV.frame, animated: true)
            case is UseDaysVC:
                self.scrollViewService.scrollRectToVisible(self.useTV.frame, animated: true)
            case is DueOnVC:
                self.scrollViewService.scrollRectToVisible(self.reminderTV.frame, animated: true)
            default:
                break
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goBoard"
        {
            let boardVC = segue.destination as! BoardVC
            boardVC.change = sender as! Bool
        }
        
        if segue.identifier == "goFormula"
        {
            let nameVC = segue.destination as! FormulationBrandVC
            
            if let service = self.service
            {
                nameVC.nameServiceStr = service.author_tag
            }else
            {
                nameVC.nameServiceStr = ServiceNameVC.serviceClient.author_tag
            }
        }
        
        if segue.identifier == "goDesign"
        {
            let designVC = segue.destination as! DesignVC
            
            if let service = self.service
            {
                designVC.nameServiceStr = service.author_tag
                designVC.delegateDesign = self
            }else
            {
                designVC.nameServiceStr = ServiceNameVC.serviceClient.author_tag
            }
        }
        
        if segue.identifier == "goPictures"
        {
            let pictureVC = segue.destination as! PictureVC
            pictureVC.delegatePhoto = self
            
            if let _ = self.service
            {
                pictureVC.stateService = .edit
            }else
            {
                pictureVC.stateService = .create
            }
        }
        
        if segue.identifier == "goReminder"
        {
            let reminderVC = segue.destination as! DueOnVC
            reminderVC.imgClient = imgClientView.image
            
            if let service = self.service
            {
                reminderVC.nameClient = service.cliente.name
            }else
            {
                reminderVC.nameClient = client?.name ?? "!?"
            }
        }
        
        if segue.identifier == "writeMsjSegue"
        {
            let writeVC = segue.destination as! WriteMsgeVC
            writeVC.message = self.totalMessage
        }
    }
    

}
