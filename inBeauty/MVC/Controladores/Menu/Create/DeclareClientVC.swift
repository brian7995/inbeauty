//
//  DeclareClientVC.swift
//  InBeauty
//
//  Created by ADMIN on 10/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class DeclareClientVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
 
    @IBOutlet weak var searchUsers: UISearchBar!
    @IBOutlet weak var tableSearch: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var customers : [Client] = []
    
    enum Search
    {
        case list
        case filter
    }
    
    var searchUser : Search = .list
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.activarOcultamientoTeclado()
        searchUsers.delegate = self
        tableSearch.delegate = self
        
        setupKeyBoard()
        
        let refreshControl = UIRefreshControl(frame: tableSearch.frame)
        refreshControl.addTarget(self, action: #selector(listCustomersWS), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        tableSearch.refreshControl = refreshControl
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listCustomersWS()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @objc func listCustomersWS()
    {
        self.customers.removeAll()
        self.tableSearch.reloadData()
        self.tableSearch.refreshControl?.beginRefreshing()
    
        switch self.searchUser {
        case .list:
            inBiServices.instancia.listCustomers(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
                
                if let customers = respuesta as? [Client]  {
                    self.customers = customers
                    self.tableSearch.reloadData()
                }
                
                self.tableSearch.refreshControl?.endRefreshing()
            }
        case .filter:
            searchUsersWS(user: self.searchUsers.text ?? "")
        }
    }
    
    func setupKeyBoard()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
        
        //self.view.frame.origin.y = (isShowNtf) ? -keyboardRect.height : 0
        let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomStackBtns"})!
        
        UIView.animate(withDuration: 0.25) {
            bottomMessage.constant = (isShowNtf) ? (keyboardRect.height - 39) : 10
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func backAction(_ btn: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchUser = .filter
        //let currentSearchText = searchText.replacingOccurrences(of: " ", with: "%20")
        //print(currentSearchText)
        searchUsersWS(user: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let user = searchBar.text,user.count > 0
        {
            self.searchUser = .filter
            searchUsersWS(user: user)
        }else
        {
            self.searchUser = .list
            listCustomersWS()
        }
        
        searchBar.resignFirstResponder()
    }

    func searchUsersWS(user:String)
    {
        self.customers.removeAll()
        self.tableSearch.refreshControl?.beginRefreshing()
        self.tableSearch.reloadData()
        
        inBiServices.instancia.searchUser(token: AppDelegate.user?.access_token ?? "", user: user, chat: 0) { (respuesta) in
            
            if let users = respuesta as? [Client]
            {
                self.customers = users
                self.tableSearch.reloadData()
            }
            
            self.tableSearch.refreshControl?.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! ClientTVCell
        cell.tag = indexPath.row
     
        let client = customers[indexPath.row]
        cell.buildClient(client: client)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    @IBAction func backCreateService(_ segue:UIStoryboardSegue)
    {
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goDefineService"
        {
            let defineServiceVC = segue.destination as! DefinirServicioVC
            let cell = sender as! UITableViewCell
            let imgView = cell.viewWithTag(100) as! UIImageView
            
            defineServiceVC.client = customers[cell.tag]
            defineServiceVC.imgClient = imgView.image
        }
        
    }
}
