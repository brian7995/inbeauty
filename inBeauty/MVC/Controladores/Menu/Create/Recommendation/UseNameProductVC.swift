//
//  UseNameProductVC.swift
//  InBeauty
//
//  Created by ADMIN on 29/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseNameProductVC: UIViewController,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var productTV: UITableView!
    @IBOutlet weak var producTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        productTV.dataSource = self
        producTxt.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userNameProductCell", for: indexPath)
        
        let nameProduct = cell.viewWithTag(100) as! UILabel
        nameProduct.text = "Line \(indexPath.row)"
        
        return cell
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
