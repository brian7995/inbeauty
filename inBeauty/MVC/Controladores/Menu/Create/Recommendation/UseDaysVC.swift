//
//  UseDaysVC.swift
//  InBeauty
//
//  Created by ADMIN on 29/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseDaysVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate{

    @IBOutlet weak var stackType: UIStackView!
    @IBOutlet weak var stackNumbers: UIStackView!
    @IBOutlet weak var numberTxt: UITextField!
    @IBOutlet weak var collectionNumbersDay: UICollectionView!
    @IBOutlet weak var collectionDate: UICollectionView!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var viewFrecuency: UIView!
    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var descriptionProduct: UILabel!
    
    var allDays : [Int] = []
    let allDate = ["Daily","Weekly","Monthly"]
    var nombreCategoriaStr : String = ""
    var producto : Producto!
    var imgProduct:UIImage?
    
    var addReco : AddRecommend = .service
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews()
    {
        self.view.layoutIfNeeded()
        
        nameProduct.text = producto.nombre
        nameCategory.text = nombreCategoriaStr
        imgViewProduct.image = imgProduct
        descriptionProduct.text = producto.descripcion
        
        for num in 1...31
        {
            allDays.append(num)
        }
        
        setupCollectionView(collection: collectionNumbersDay, cellScale: 0.5, cellHeight: 1)
        setupCollectionView(collection: collectionDate, cellScale: 0.5, cellHeight: 1)
    }
    
    func setupCollectionView(collection : UICollectionView,cellScale:CGFloat,cellHeight:CGFloat)
    {
        collection.delegate = self
        collection.dataSource = self
        
        let sizeCell = collection.bounds.width * cellScale
        
        let insetCell = (collection.bounds.width - sizeCell) / 2.0
        let layoutCell = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layoutCell.itemSize = CGSize(width: sizeCell, height: collection.bounds.height)
        
        collection.contentInset = UIEdgeInsets(top: 0, left: insetCell, bottom: 0, right: insetCell)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func typeNumberAction(_ sender: UIButton)
    {
        stackType.isHidden = true
        stackNumbers.isHidden = false
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if collectionDate == scrollView
        {
            self.centerCell(collectionView: collectionDate, target: targetContentOffset, scrollView: scrollView)
        }else
        {
            self.centerCell(collectionView: collectionNumbersDay, target: targetContentOffset, scrollView: scrollView)
        }
    }

    func centerCell(collectionView:UICollectionView,target:UnsafeMutablePointer<CGPoint>,scrollView:UIScrollView)
    {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = target.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        target.pointee = offset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("End Decelerating")
        //collectionDate.reloadData()
        //collectionNumbersDay.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case collectionDate:
            return allDate.count
        case collectionNumbersDay:
            return allDays.count
        default:
            return 0
        }
    }
    
    //func scroll
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let isNumber = (collectionView == collectionNumbersDay)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: (isNumber) ? "numberDayCell" : "dateCell", for: indexPath)
        
        let numberLabel = cell.viewWithTag(100) as! UILabel
        numberLabel.text = (isNumber) ? "\(allDays[indexPath.row])" : "\(allDate[indexPath.row])"
        
        //let center = (cell.frame.midX == viewFrecuency.frame.midX || cell.frame.origin.x == 0)
        //numberLabel.alpha = (center) ? 1 : 0.5
        
        return cell
    }
    
    @IBAction func addRecommendationAction(_ sender: UIButton)
    {
        let sortedVisibleCells = collectionNumbersDay.visibleCells.sorted(by: {return $0.frame.origin.x > $1.frame.origin.x})
        let cell = sortedVisibleCells[1]
        let numberLabel = cell.viewWithTag(100) as! UILabel
        
        var diaNum = Int(numberLabel.text!)!
        diaNum += (sortedVisibleCells.count == 2 && diaNum == 30) ? 1 : 0
        
        var dateVisibleCells = collectionDate.visibleCells.sorted(by: {return $0.frame.origin.x < $1.frame.origin.x})
    
        var dateCell = dateVisibleCells[1]
        
        if dateVisibleCells.count == 2 && dateVisibleCells.first?.frame.origin.x == 0
        {
            dateCell = dateVisibleCells[0]
        }
        
        let dateTxt = dateCell.viewWithTag(100) as! UILabel
        let time = dateTxt.text!
        
        let frecuencia = String(diaNum) + ", " + time
        let use = Use(id: "useId" ,producto_id: producto.id, cantidad: 0, medida: "-",frecuencia: frecuencia,producto: producto)
        var identifier : String = ""
        
        switch self.addReco {
        case .service:
            identifier = "goServiceName"
        case .inbox:
            identifier = "goInbox"
        }
        
        self.performSegue(withIdentifier: identifier, sender: use)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case "goServiceName":
            let serviceVC = segue.destination as! ServiceNameVC
            if let service = serviceVC.service
            {
                if service.recomendaciones == nil
                {
                    service.recomendaciones = Recomendacion()
                    service.recomendaciones?.use.append(sender as! Use)
                }else
                {
                    service.recomendaciones?.use.append(sender as! Use)
                }
            }else
            {
                if ServiceNameVC.serviceClient.recomendaciones == nil
                {
                    ServiceNameVC.serviceClient.recomendaciones = Recomendacion()
                    ServiceNameVC.serviceClient.recomendaciones?.use.append(sender as! Use)
                }else
                {
                    ServiceNameVC.serviceClient.recomendaciones?.use.append(sender as! Use)
                }
            }
        case "goInbox":
            let messageVC = segue.destination as! MessagesVC
            let use = sender as! Use
            let newMessage = Message()
            newMessage.tipo_mensaje = "useCell"
            newMessage.mensaje = "\(use.frecuencia ?? "");\(use.producto.nombre);\(use.producto_id);\(use.producto.url_foto)"
            newMessage.de_user_id = AppDelegate.user?.id ?? ""
            newMessage.para_user_id = messageVC.chatUser?.para_user_id ?? messageVC.idReceptor
            messageVC.messages.append(newMessage)
            
            if let service = messageVC.lastService
            {
                if service.recomendaciones == nil
                {
                    service.recomendaciones = Recomendacion()
                    service.recomendaciones?.use.append(use)
                }else
                {
                    service.recomendaciones?.use.append(use)
                }
            }
        default:
            break
        }
    }
}

//numberTxt.delegate = self
//
//NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
//NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
//NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
//
//deinit {
//    NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
//    NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
//    NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
//}
//
//@objc func keyboardWillChange(ntf:Notification)
//{
//    guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
//
//    let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
//
//    UIView.animate(withDuration: 0.25) {
//        let bottomMessage = self.view.constraints.first(where: {$0.identifier == "bottomUseProduct"})!
//        bottomMessage.constant = (isShowNtf) ? keyboardRect.height : 0
//        self.view.layoutIfNeeded()
//    }
//
//}
