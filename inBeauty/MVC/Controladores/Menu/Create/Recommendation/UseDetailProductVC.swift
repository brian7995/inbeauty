//
//  UseDetailProductVC.swift
//  InBeauty
//
//  Created by ADMIN on 29/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseDetailProductVC: UIViewController,UITableViewDataSource,UITextFieldDelegate,UITableViewDelegate {

    @IBOutlet weak var productTV: UITableView!
    @IBOutlet weak var producTxt: UITextField!
    @IBOutlet weak var nombreMarca: UILabel!
    @IBOutlet weak var nombreCategoria: UILabel!
    @IBOutlet weak var imgViewBrand: UIImageView!
    
    var imgBrand: UIImage?
    var categoria : Categoria!
    var nombreMarcaStr = ""
    
    var addReco : AddRecommend = .service
    var productos : [Producto] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        
        productTV.dataSource = self
        productTV.delegate = self
        producTxt.delegate = self
        
        nombreMarca.text = nombreMarcaStr
        nombreCategoria.text = categoria.nombre
        
        imgViewBrand.layer.cornerRadius = imgViewBrand.bounds.midX
        imgViewBrand.image = imgBrand
        
        productTV.alwaysBounceVertical = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let txt = textField.text,txt.count == 3
        {
            buscarProducto(txt: txt)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        buscarProducto(txt: textField.text ?? "")
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        buscarProducto(txt: textField.text ?? "")
    }
    
    func buscarProducto(txt:String)
    {
        inBiServices.instancia.buscarProducto(token: AppDelegate.user?.access_token ?? "", idCategoria: categoria.id, iniciales: txt) { (respuesta) in
            guard let productos = respuesta as? [Producto] else {
                self.productTV.isHidden = true
                return
            }
            
            self.productos = productos
            self.productTV.isHidden = productos.isEmpty
            self.productTV.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailProductCell", for: indexPath)
        
        let imgView = cell.viewWithTag(100) as! UIImageView
        let cellLabel = cell.viewWithTag(101) as! UILabel
        
        cellLabel.text = productos[indexPath.row].nombre
        imgView.cargarImgDesdeURL(urlImg: productos[indexPath.row].url_foto) {
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        let imgView = cell.viewWithTag(100) as! UIImageView
        let producto = productos[indexPath.row]
        let lista : [Any] = [categoria.nombre,producto,imgView.image ?? UIImage()]
        self.performSegue(withIdentifier: "goAmountUse", sender: lista)
    }
    

    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goAmountUse"
        {
            let formulaVC = segue.destination as! UseDaysVC
            let lista = sender as! [Any]
            formulaVC.nombreCategoriaStr = lista.first as! String
            formulaVC.producto = lista[1] as? Producto
            formulaVC.imgProduct = lista.last as? UIImage
            formulaVC.addReco = self.addReco
        }
    }
    

}
