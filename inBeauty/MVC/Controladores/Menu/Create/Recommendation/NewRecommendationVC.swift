//
//  NewRecommendationVC.swift
//  InBeauty
//
//  Created by ADMIN on 13/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class NewRecommendationVC: UIViewController {

    @IBOutlet weak var stackBtn: UIStackView!

    var addReco : AddRecommend = .service
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stackBtn.layoutIfNeeded()
        
        let lastBtn = stackBtn.subviews.last!
        lastBtn.layer.borderWidth = 1
        lastBtn.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1).self
        
        stackBtn.subviews.forEach { (subView) in
            let btn = subView as! UIButton
            btn.layer.cornerRadius = btn.bounds.midY
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func goDoDontAction(_ btn: UIButton)
    {
        self.performSegue(withIdentifier: "goDoDont", sender: btn.tag)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goDoDont"
        {
            let doDontVC = segue.destination as! DoRecommendationVC
            doDontVC.idBtn = sender as! Int
            doDontVC.addReco = self.addReco
        }
        
        if segue.identifier == "goUseProduct"
        {
            let useVC = segue.destination as! UseProductVC
            useVC.addReco = self.addReco
        }
    }
}
