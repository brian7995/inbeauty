//
//  UseRecommendationVC.swift
//  InBeauty
//
//  Created by ADMIN on 13/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseRecommendationVC: UIViewController {

    
    @IBOutlet weak var useLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        useLabel.layoutIfNeeded()
        useLabel.layer.borderWidth = 1
        useLabel.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1).self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
