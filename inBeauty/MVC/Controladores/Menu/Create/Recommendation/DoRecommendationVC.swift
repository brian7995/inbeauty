//
//  DoRecommendationVC.swift
//  InBeauty
//
//  Created by ADMIN on 13/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class DoRecommendationVC: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {
 
    @IBOutlet weak var taskTxt: UITextField!
    @IBOutlet weak var DoDonTask: UILabel!
    @IBOutlet weak var viewDo: UIView!
    @IBOutlet weak var taskTV: UITableView!
    
    var idBtn = 0
    var tasks = [DoDont]()
    var filter = ""
    
    var addReco : AddRecommend = .service
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    func setupViews()
    {
        taskTxt.delegate = self
        viewDo.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(ntf:)), name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
        
        let backGroundColor = (idBtn == 1) ?  #colorLiteral(red: 0.6146504879, green: 0.5941453576, blue: 0.9999912381, alpha: 1) : #colorLiteral(red: 0.2506617904, green: 0.2492632866, blue: 0.3321357369, alpha: 1)
        let txt = (idBtn == 1) ? "Do" : "Don't"
        filter = (idBtn == 1) ? "do" : "dont"
        
        DoDonTask.backgroundColor = backGroundColor
        DoDonTask.text = txt
        
        self.taskTV.dataSource = self
        self.taskTV.delegate = self
        
        self.listTaskWS()
    }
    
    func listTaskWS()
    {
        inBiServices.instancia.listDoDont(token: AppDelegate.user?.access_token ?? "") { (respuesta) in
            
            guard let statusDoDont = respuesta as? StatusDoDont,statusDoDont.status == "success" else{
                return
            }

            self.tasks = statusDoDont.data.filter({return $0.tipo == self.filter})
            self.taskTV.reloadData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    
    deinit {
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    
    @objc func keyboardWillChange(ntf:Notification)
    {
        guard let keyboardRect = (ntf.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        
        let isShowNtf = (ntf.name == UIResponder.keyboardWillShowNotification || ntf.name == UIResponder.keyboardWillChangeFrameNotification)
        
        //self.view.frame.origin.y = (isShowNtf) ? -keyboardRect.height : 0
        let topViewDoDont = self.view.constraints.first(where: {$0.identifier == "topViewDoDont"})!
        
        let bottomMessage = self.viewDo.constraints.first(where: {$0.identifier == "bottomRecommendation"})!
        bottomMessage.constant = (isShowNtf) ? keyboardRect.height : 0
        
        switch UIScreen.main.bounds.height  {
        case 568:
            topViewDoDont.constant = (isShowNtf) ? -70 : 15
        case 667:
            topViewDoDont.constant = (isShowNtf) ? -5 : 15
        default:
            break
        }
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //taskTxt.becomeFirstResponder()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dodontCell", for: indexPath)
        
        let task = cell.viewWithTag(100) as! UILabel
        task.text = tasks[indexPath.row].tarea
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        taskTxt.text = tasks[indexPath.row].tarea
    }
    
    @IBAction func addRecommendation(_ btn: UIButton)
    {
        guard let txt = taskTxt.text,!txt.isEmpty else {return}
        
        let doDont = DoDont(id: "dodontId",tipo: (idBtn == 1) ? "Do" : "Don't", tarea: txt)
        
        var identifier : String = ""
        
        switch self.addReco {
        case .service:
            identifier = "goServiceName"
        case .inbox:
            identifier = "goInbox"
        }
    
        //print("Do Dont : \(doDont)")
        self.performSegue(withIdentifier: identifier, sender: doDont)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case "goServiceName":
            let serviceVC = segue.destination as! ServiceNameVC
            if let service = serviceVC.service
            {
                if service.recomendaciones == nil
                {
                    service.recomendaciones = Recomendacion()
                    service.recomendaciones?.dodont.append(sender as! DoDont)
                }else
                {
                    service.recomendaciones?.dodont.append(sender as! DoDont)
                }
                
            }else
            {
                if ServiceNameVC.serviceClient.recomendaciones == nil
                {
                    ServiceNameVC.serviceClient.recomendaciones = Recomendacion()
                    ServiceNameVC.serviceClient.recomendaciones?.dodont.append(sender as! DoDont)
                }else
                {
                    ServiceNameVC.serviceClient.recomendaciones?.dodont.append(sender as! DoDont)
                }
            }
        case "goInbox":
            print("inbox")
            let messageVC = segue.destination as! MessagesVC
            
            let newDoDont = sender as! DoDont
            let newMessage = Message()
            newMessage.tipo_mensaje = "dodontCell"
            newMessage.mensaje = "\(newDoDont.tipo);\(newDoDont.tarea)"
            newMessage.de_user_id = AppDelegate.user?.id ?? ""
            newMessage.para_user_id = messageVC.chatUser?.chatcon.id ?? messageVC.idReceptor
            messageVC.messages.append(newMessage)
            //print("Message \(newMessage)")
            
            if let service = messageVC.lastService
            {
                if service.recomendaciones == nil
                {
                    service.recomendaciones = Recomendacion()
                    service.recomendaciones?.dodont.append(newDoDont)
                }else
                {
                    service.recomendaciones?.dodont.append(newDoDont)
                }
            }
            
        default:
            break
        }
    }
}
