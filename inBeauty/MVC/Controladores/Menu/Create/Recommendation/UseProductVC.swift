//
//  UseProductVC.swift
//  InBeauty
//
//  Created by ADMIN on 17/05/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class UseProductVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var viewUse: UIView!
    @IBOutlet weak var productTxt: UITextField!
    @IBOutlet weak var nameMark: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var useLabel: UILabel!
    @IBOutlet weak var marcasTV: UITableView!
    @IBOutlet weak var btnMark: UIButton!
    @IBOutlet weak var imgViewBrand: UIImageView!
    
    var centerXUse : NSLayoutConstraint!
    var topUse : NSLayoutConstraint!
    var widthUse : NSLayoutConstraint!
    
    var namesProducts = ["Lancome","Loreal"]
    var lines = ["Line 1","Line 2"]
    
    var marcas = [Marca]()
    var categorias = [Categoria]()
    
    var switchTableView = false
    var idCategoria = 0
    
    var addReco : AddRecommend = .service
    
    enum Search {
        case brand
        case category
    }
    
    var mySearch : Search = Search.brand
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mySearch = .brand
        
        centerXUse = viewUse.constraints.first(where: {$0.identifier == "centerUseX"})!
        
        widthUse = useLabel.constraints.first(where: {$0.identifier == "widthUse"})
        
        topUse = viewUse.constraints.first(where: {$0.identifier == "topUse"})!
        
        productTxt.delegate = self
        
        useLabel.layoutIfNeeded()
        useLabel.layer.borderWidth = 1
        useLabel.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1).self
        
        marcasTV.alwaysBounceVertical = true
        imgViewBrand.layer.cornerRadius = imgViewBrand.bounds.midX
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameMark.isHidden = categorias.isEmpty
        btnMark.isHidden = categorias.isEmpty
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let txt = textField.text,txt.count == 3,mySearch == .brand
        {
            buscarMarcas(str: txt)
        }else if let txt = textField.text,txt.count == 3
        {
            buscarCategorias(str: txt)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if mySearch == .brand
        {
            buscarMarcas(str: textField.text ?? "")
        }else
        {
            buscarCategorias(str: textField.text ?? "")
        }
    }
    
    func buscarCategorias(str:String)
    {
        //activityIndicator.startAnimating()
        
        inBiServices.instancia.buscarCategoria(token: AppDelegate.user?.access_token ?? "", idMarca: idCategoria, iniciales: str) { (respuesta) in
            
            guard let categorias = respuesta as? [Categoria] else
            {
                self.marcasTV.isHidden = true
                return
            }
            
            //self.activityIndicator.stopAnimating()
            self.categorias = categorias
            self.marcasTV.isHidden = categorias.isEmpty
            self.marcasTV.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if mySearch == .brand
        {
            buscarMarcas(str: textField.text ?? "")
        }else
        {
            buscarCategorias(str: textField.text ?? "")
        }
        return textField.resignFirstResponder()
    }
    
    func buscarMarcas(str:String)
    {
        //activityIndicator.startAnimating()
        inBiServices.instancia.buscarMarca(token: AppDelegate.user?.access_token ?? "", iniciales: str) { (respuesta) in
            
            guard let marcas = respuesta as? [Marca] else
            {
                self.marcasTV.isHidden = true
                return
            }
            
            //self.activityIndicator.stopAnimating()
            self.marcas = marcas
            self.marcasTV.isHidden = marcas.isEmpty
            self.marcasTV.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (mySearch == .brand) ? marcas.count : categorias.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "useCell", for: indexPath)
        
        let imgCell = cell.viewWithTag(100) as! UIImageView
        let cellLabel = cell.viewWithTag(101) as! UILabel
        
        let widthImg = imgCell.constraints.first(where: {$0.identifier == "widthCell"})!
        
        if mySearch == .brand
        {
            UIView.animate(withDuration: 0.15) {
                widthImg.constant = 60
                tableView.layoutIfNeeded()
            }
            cellLabel.text = marcas[indexPath.row].nombre
            imgCell.cargarImgDesdeURL(urlImg: marcas[indexPath.row].url_foto ?? "") {
            }
        } else
        {
            widthImg.constant = 0
            cellLabel.text = categorias[indexPath.row].nombre
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)!
        let imgView = cell.viewWithTag(100) as! UIImageView
        
        if mySearch == .brand
        {
            self.useLabel.layer.borderWidth = 0
            self.imgViewBrand.image = imgView.image
            self.updateTable(row: indexPath.row)
        }else
        {
            let categoria = categorias[indexPath.row]
            let list : [Any] = [productLabel.text ?? "¿?",categoria]
            self.performSegue(withIdentifier: "goUseProductos", sender: list)
        }
    }
    
    func updateTable(row:Int)
    {
        //switchTableView = !switchTableView
        btnMark.isHidden = false
        mySearch = .category
        
        idCategoria = marcas[row].id
        nameMark.isHidden = false
        nameMark.text = marcas[row].nombre
        useLabel.text = ""
        productLabel.text = "Line"
        productTxt.text = ""
        productTxt.placeholder = "Type the line"
    
        marcasTV.reloadData()
        
        UIView.animate(withDuration: 0.15) {
            
            self.centerXUse.constant = -(UIScreen.main.bounds.width / 3) //(self.switchTableView) ? -(UIScreen.main.bounds.width / 3) : 0
            self.topUse.constant = 16//(self.switchTableView) ? 16 : 32
            self.useLabel.layer.cornerRadius = 30//(self.switchTableView) ? 30 : 40
            self.widthUse.constant = 60//(self.switchTableView) ? 60 : 80
            
            self.viewUse.layoutIfNeeded()
        }
    }
    
    
    @IBAction func backMarks(_ sender: UIButton)
    {
        //switchTableView = false
        categorias.removeAll()
        mySearch = .brand
        btnMark.isHidden = true
        nameMark.isHidden = true
        
        imgViewBrand.image = nil
        
        useLabel.layer.borderWidth = 1
        useLabel.text = "Use"
        productLabel.text = "Product Brand"
        productTxt.placeholder = "type the product brand"
        
        marcasTV.reloadData()
        
        UIView.animate(withDuration: 0.15) {
            
            self.centerXUse.constant =  0
            self.topUse.constant = 32
            self.useLabel.layer.cornerRadius = 40
            self.widthUse.constant = 80
            
            self.viewUse.layoutIfNeeded()
        }
    }
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goUseProductos"
        {
            let formulacionVC = segue.destination as! UseDetailProductVC
            let lista = sender as! [Any]
            
            formulacionVC.nombreMarcaStr = lista.first as! String
            formulacionVC.categoria = lista.last as? Categoria
            formulacionVC.imgBrand = self.imgViewBrand.image
            formulacionVC.addReco = self.addReco
        }
    }
    

}
