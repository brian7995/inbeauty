//
//  UserManager.swift
//  Snoop
//
//  Created by Javier  Siancas on 9/02/17.
//  Copyright © 2017 Fernando Daniel Mateo Ramos. All rights reserved.
//



/*************************************** VERSIONAMIENTO ***************************************/
//  Versión 1.0.0:
//  1. Se creó el archivo.
//  2. Se crearon los siguientes métodos:
//  - saveUserLogged:additionalImplementation:
//  - userLogged:
//  - deleteUser:additionalImplementation:
/**********************************************************************************************/



import UIKit

/// El nombre de la llave para guardar el usuario usando `NSUserDefaults`.
private let UserDataKey = "UserDataKey"
private let NotiDataKey = "NotiDataKey"

/**
 Clase que maneja la persistencia de los usuarios de la aplicación.
 Esta clase expone métodos que permiten guardar al usuario que ha iniciado sesión, obtenerlo en el momento que la aplicación lo necesite y eliminarlo cuando se cierre la sesión.
 */
internal class UserManager: NSObject {

    // MARK: - UserManager's methods
    
    /**
     Método para guardar al usuario que ha iniciado sesión.
     - Parameters:
        - user: El usuario que ha iniciado sesión y que desea guardar en la aplicación.
        - additionalImplementation: Bloque de código opcional para ejecutar después de guardar al usuario.
     - Returns: Valor que permite saber si el usuario fue guardado exitosamente o no.
     */
    internal class func saveUserLogged(_ user: Any?, _ additionalImplementation: (() -> Void)?) -> Bool {
        var userDataSaved = false
//        print("Guardando Usuario")
        if user != nil {
//            print("Guardo Usuario")
            // Eliminar un usuario que ha iniciado sesión previamente.
            let userDefaults = UserDefaults.standard
            userDefaults.removeObject(forKey: UserDataKey)
            
            // Guardar al nuevo usuario.
            //let userData = NSKeyedArchiver.archivedData(withRootObject: user!)
            do
            {
                let userData = try NSKeyedArchiver.archivedData(withRootObject: user!, requiringSecureCoding: false)
                userDefaults.set(userData, forKey: UserDataKey)
                userDataSaved = userDefaults.synchronize()
            }catch let error
            {
                print("Error Saved User",error.localizedDescription)
            }
  
            
            // Ejecutar bloque de código adicional después de guardar al usuario.
            additionalImplementation?()
        }
        
        return userDataSaved
    }
    
    internal class func saveNotificacion(_ noti: Any?, _ additionalImplementation: (() -> Void)?) -> Bool {
        var notiDataSaved = false
        //        print("Guardando Usuario")
        if noti != nil {
            // print("Guardo Usuario")
            let userDefaults = UserDefaults.standard
            
            // Guardar al nuevo usuario.
            //let userData = NSKeyedArchiver.archivedData(withRootObject: user!)
            do
            {
                let userData = try NSKeyedArchiver.archivedData(withRootObject: noti!, requiringSecureCoding: false)
                userDefaults.set(userData, forKey: NotiDataKey)
                notiDataSaved = userDefaults.synchronize()
            }catch let error
            {
                print("Error Saved User",error.localizedDescription)
            }
            
            // Ejecutar bloque de código adicional después de guardar al usuario.
            additionalImplementation?()
        }
        
        return notiDataSaved
    }
    
    
    /**
     Método para retornar el usuario que ha iniciado sesión.
     - Returns: El usuario que ha iniciado sesión o `nil` si no existe.
     */
    internal class func userLogged() -> Any? {
        var userLogged: Any?
        
        let userDefaults = UserDefaults.standard
        let data = userDefaults.object(forKey: UserDataKey) as? Data
    
        if data != nil { /* Si hay un `NSData` que represente al usuario, entonces... */
            //userLogged = NSKeyedUnarchiver.unarchiveObject(with: data!)
            do
            {
                userLogged = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data!)
            }catch let error
            {
                print("Error User Logged",error.localizedDescription)
            }
            
        }
        
        return userLogged
    }
    
    internal class func notiArray() -> Any? {
        var notiLogged: Any?
        
        let userDefaults = UserDefaults.standard
        let data = userDefaults.object(forKey: NotiDataKey) as? Data
        
        if data != nil { /* Si hay un `NSData` que represente al usuario, entonces... */
            //userLogged = NSKeyedUnarchiver.unarchiveObject(with: data!)
            do
            {
                notiLogged = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data!)
            }catch let error
            {
                print("Error Noti Logged",error.localizedDescription)
            }
            
        }
        
        return notiLogged
    }
    
    /**
    
     Método para eliminar al usuario que ha iniciado sesión.
     - Parameter additionalImplementation: Bloque de código opcional para ejecutar después de eliminar al usuario.
     - Returns: Valor que permite saber si el usuario fue eliminado exitosamente o no.
     */
    
    internal class func deleteUserLogged(_ additionalImplementation: (() -> Void)?) -> Bool {
        var userDeleted = false
        
        // Eliminar al usuario que ha iniciado sesión.
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: UserDataKey)
        userDeleted = userDefaults.synchronize()
        
        // Ejecutar bloque de código adicional después de eliminar al usuario.
        additionalImplementation?()
        
        return userDeleted
    }
    
    internal class func deleteNtfcaciones(_ additionalImplementation: (() -> Void)?) -> Bool {
        var notiDeleted = false
        
        // Eliminar al usuario que ha iniciado sesión.
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: NotiDataKey)
        notiDeleted = userDefaults.synchronize()
        
        // Ejecutar bloque de código adicional después de eliminar al usuario.
        additionalImplementation?()
        
        return notiDeleted
    }
}
