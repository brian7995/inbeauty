//
//  AppDelegate.swift
//  InBeauty
//
//  Created by ADMIN on 11/03/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import Cloudinary
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    static var user: User?
    static var cloud : CLCloudinary?
    //static var badge : Int = 0
    //static var messageVC : Bool = false
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Client ID Antiguo : 112996042677-1f2rgdn566qh8c472kri83pm91ies700.apps.googleusercontent.com
        GIDSignIn.sharedInstance().clientID = "1087986172804-5eellvao69i08d0vpfoid6ie1vdq0puu.apps.googleusercontent.com"
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        AppDelegate.cloud = CLCloudinary.init(url: "cloudinary://413228667999944:rKdUzceH7bhQxLfM2q4nRc5MqPA@inbi")
        
        if let gai = GAI.sharedInstance()
        {
            gai.tracker(withTrackingId: "Tracking_ID")
            gai.trackUncaughtExceptions = true
            //gai.logger.logLevel = .verbose
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            //UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.badge, .alert, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
            }
            
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
        
        return true
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourc, annotation: <#T##Any!#>)
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("notificacion center willpresent")
        
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        if let estatus = userInfo[AnyHashable("tipo_notificacion")] as? String,estatus == "inbox"
        {
            //AppDelegate.badge += 1
            //UIApplication.shared.applicationIconBadgeNumber += 1
            NotificationCenter.default.post(name: Notification.Name(rawValue: "NewMessage"), object: nil, userInfo: userInfo)
        }
   
        let navigationVC = window?.rootViewController as? UINavigationController
        //print(navigationVC?.visibleViewController)
        
        var optionsNotification : UNNotificationPresentationOptions = []
        
        if let _ = navigationVC?.visibleViewController as? MessagesVC
        {
            optionsNotification = [.sound,.badge]
        }else
        {
            optionsNotification = [.alert,.sound,.badge]
        }
        
        completionHandler(optionsNotification)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("notificacion center open settings")
        //print(notification?.request.content.userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("notificacion center response")
      
        //let userInfo = response.notification.request.content.userInfo
        //let aps = userInfo["aps"] as? [String:Any] ?? [:]
        
//        if let estatus = userInfo[AnyHashable("tipo_notificacion")] as? String,estatus == "inbox"
//        {
//            AppDelegate.badge += 1
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "NewMessage"), object: nil, userInfo: userInfo)
//        }
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "EstatusPedido"), object: nil, userInfo: userInfo)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "CodigoActivacion"), object: nil, userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Recibimos una notificacion desde el didReceiveRemoteNotificacion!")
        print(userInfo)
        
        if let estatus = userInfo[AnyHashable("tipo_notificacion")] as? String,estatus == "inbox"
        {
            if UIApplication.shared.applicationState == .background
            {
                //AppDelegate.badge += 1
                //UIApplication.shared.applicationIconBadgeNumber += 1
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NewMessage"), object: nil, userInfo: userInfo)
            }
        }
//
//        if let estatus = userInfo[AnyHashable("estatus")] as? String,estatus == "cancelado"
//        {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "PedidoCancelado"), object: nil, userInfo: userInfo)
//        }
//
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "EstatusPedido"), object: nil, userInfo: userInfo)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "CodigoActivacion"), object: nil, userInfo: userInfo)
        
        completionHandler(.newData)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("Will Enter Foreground")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "NewMessage"), object: nil, userInfo: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options [UIApplication.OpenURLOptionsKey.annotation])
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        return true
    }
}

