//
//  inBiServices.swift
//  InBeauty
//
//  Created by ADMIN on 13/06/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit
import Alamofire

class inBiServices: NSObject {

    static let instancia = inBiServices()
    let urlBase = "https://inbiapp.com/api/"
    
    func buscarMarca(token:String,iniciales:String,completacion: @escaping(_ marca:Any) -> Void)
    {
        let urlMarcas = urlBase + "productos/marcas/buscar?string=\(iniciales)"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        Alamofire.request(urlMarcas, method: .post, parameters: nil, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            do
            {
                let tiendas = try JSONDecoder().decode([Marca].self, from: data)
                completacion(tiendas)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    func buscarCategoria(token:String,idMarca:Int,iniciales:String,completacion: @escaping(_ categoria:Any) -> Void)
    {
        let urlCategorias = urlBase + "productos/marcas/\(idMarca)/categorias/buscar?string=\(iniciales)"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        Alamofire.request(urlCategorias, method: .post, parameters: nil, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            do
            {
                let tiendas = try JSONDecoder().decode([Categoria].self, from: data)
                completacion(tiendas)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    func buscarProducto(token:String,idCategoria:Int,iniciales:String,completacion : @escaping(_ producto:Any) -> Void)
    {
        let urlProducto = urlBase + "productos/marcas/categorias/\(idCategoria)/producto/buscar"
        let cabecera : HTTPHeaders = ["Authorization" : "Bearer \(token)"]
        let parametros : Parameters = ["string":iniciales]
        
        Alamofire.request(urlProducto, method: .post, parameters: parametros, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            do
            {
                let productos = try JSONDecoder().decode([Producto].self, from: data)
                completacion(productos)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
    }
    
    //MARK: REGISTER
    
    func register(username:String,email:String,phone:String,password:String,confirm:String,pais:String,urlPhoto:String,tokenFbase:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlRegister = urlBase + "signup"
        let parameters : Parameters = ["tipo":ChooseVC.tipo ?? "cliente","name":username,"email":email,"phone":phone,"password":password,"password_confirmation":confirm,"pais":pais,"foto":urlPhoto,"fcm_token":tokenFbase]

        Alamofire.request(urlRegister, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseJSON { (respuesta) in

            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }

            print(respuesta.value ?? "nada")

            let body = respuesta.value as? [String:Any] ?? [:]
            //let exito = estado?["status"] as? String ?? "¿?"
            completacion(body)
        }
    }
    
    //MARK: SAVE PHOTO PROFILE
    func savePhotoProfile(id:String,urlPhoto:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlSavePhoto = urlBase + "usuario/editar/foto/\(id)"
        let parameters : Parameters = ["foto":urlPhoto]
        
        request(urlSavePhoto, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion("fracaso")
                return
            }
            
            print(respuesta.value ?? "nadasdas")
            let body = respuesta.value as? [String:Any] ?? [:]
            completacion(body)
        }
    }
    
    func login(email:String,password:String,tokenFbase:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlLog = urlBase + "login"
        let parameters : Parameters = ["email":email,"password":password,"fcm_token":tokenFbase]
        
        Alamofire.request(urlLog, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            //print(respuesta.value)
            
            do
            {
                let status = try JSONDecoder().decode(StatusUser.self, from: data)
                completacion(status)
            }catch
            {
                completacion(respuesta.value as? [String:Any] ?? [:])
            }
            
        }
    }
    
    func listCategories(token:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlTitles = urlBase + "categorias"
        var request = URLRequest(url: URL(string: urlTitles)!)
    
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        Alamofire.request(request).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            do
            {
                let status = try JSONDecoder().decode(StatusCategory.self, from: data)
                completacion(status)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
    }
    
    func saveCategories(token:String,title:String,categories:[Int],completacion : @escaping(_ status:Any) -> Void)
    {
        let urlCategories = urlBase + "profesional/postlogin"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
       
        let parameters : Parameters = ["titulo":title,"categorias":categories]
        
        Alamofire.request(urlCategories, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            print(respuesta.value ?? "dasdsa")
            let estado = respuesta.value as? [String:Any]
            let exito = estado?["status"] as? String ?? "¿?"
            completacion(exito)
        }
    }

    func listCustomers(token:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlCustomers = urlBase + "clientes/mostrar"
        
        guard let url = URL(string: urlCustomers) else {return}
        
        var customersQuest = URLRequest(url: url)
        customersQuest.httpMethod = HTTPMethod.get.rawValue
        customersQuest.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
        
        Alamofire.request(customersQuest).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            do
            {
                let customers = try JSONDecoder().decode([Client].self, from: data)
                completacion(customers)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
        
    }
    
    //MARK: UPDATE PROFILE
    func updateProfile(token:String,name:String,pass:String,confirmPass:String,phone:String,pais:String,foto:String,completacion: @escaping(_ status:Any) -> Void)
    {
        let urlUpdate = urlBase + "usuario/editar"
        let headers : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        let parametros : Parameters = ["name":name,"password":pass,"password_confirmation":confirmPass,"phone":phone,"pais":pais,"foto":foto]
        
        request(urlUpdate, method: .put, parameters: parametros, encoding: URLEncoding.default, headers: headers).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                print("FRACASO")
                completacion("Fracaso")
                return
            }
            print(respuesta.value ?? "nada de valor")
            let body = respuesta.value as? [String:Any] ?? [:]
            completacion(body)
        }
    }
    
    //MARK: SEND CUSTOMER SERVICE
    func saveCustomerService(token:String,service:Data,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlService = urlBase + "serviciocliente"
        
        guard let url = URL(string: urlService) else {return}
        
        var serviceQuest = URLRequest(url: url)
        serviceQuest.httpMethod = HTTPMethod.post.rawValue
        serviceQuest.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
        serviceQuest.addValue("application/json", forHTTPHeaderField: "Content-Type") //; charset=UTF-8"
        serviceQuest.httpBody = service
        
        Alamofire.request(serviceQuest).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else{
                print("FRACASO BREIANS")
                completacion("Error")
                return
            }
            
            print(respuesta.value ?? "")
            let status = respuesta.value as? [String:Any]
            let exito = status?["status"] as? String
            completacion(exito ?? "Error")
        }
    }
    
    //MARK: UPDATE CUSTOMER SERVICE
    func updateCustomersService(id:String,token:String,update:Data,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlUpdate = urlBase + "serviciocliente/\(id)"
        
        guard let url = URL(string: urlUpdate) else {return}
        
        var updateQuest = URLRequest(url: url)
        updateQuest.httpMethod = HTTPMethod.put.rawValue
        updateQuest.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
        updateQuest.addValue("application/json", forHTTPHeaderField: "Content-Type") //; charset=UTF-8"
        updateQuest.httpBody = update
        
        Alamofire.request(updateQuest)
            .downloadProgress(closure: { (progress) in
                print(progress.fractionCompleted)
            })
            .responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else{
                print("FRACASO BREIANS")
                completacion("Error")
                return
            }
            
            print(respuesta.value ?? "")
            let status = respuesta.value as? [String:Any]
            let exito = status?["status"] as? String
            completacion(exito ?? "Error")
        }
    }
    
    //MARK: LIST SERVICES
    func listServices(token:String,id:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlTitles = urlBase + "serviciocliente/\(id)"
        
        guard let url = URL(string: urlTitles) else {return}
        var serviceRequest = URLRequest(url: url)
        
        serviceRequest.httpMethod = HTTPMethod.get.rawValue
        serviceRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        Alamofire.request(serviceRequest).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            //print(respuesta.value)
            do
            {
                let status = try JSONDecoder().decode([OnService].self, from: data)
                completacion(status)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    //MARK: LIST DO DONT
    func listDoDont(token:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlDoDont = urlBase + "dodont"
        
        guard let url = URL(string: urlDoDont) else {return}
        var dodontRequest = URLRequest(url: url)
        
        dodontRequest.httpMethod = HTTPMethod.get.rawValue
        dodontRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        dodontRequest.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(dodontRequest).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            //print(respuesta.value)
            do
            {
                let status = try JSONDecoder().decode(StatusDoDont.self, from: data)
                completacion(status)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    //MARK: LIST DISCOVERY
    func listDiscovery(token:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlDiscovey = urlBase + "discovery"
        
        guard let url = URL(string: urlDiscovey) else {return}
        var discoveryRequest = URLRequest(url: url)
        
        discoveryRequest.httpMethod = HTTPMethod.get.rawValue
        discoveryRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        discoveryRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(discoveryRequest).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            //print(respuesta.value)
            do
            {
                let status = try JSONDecoder().decode([Discovery].self, from: data)
                completacion(status)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    //MARK: FILTER SEARCH DISCOVERY
    
    func searchDiscovery(token:String,search:String,filter:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlSearch = urlBase + "discovery/busqueda"
        
        let parametros : Parameters = ["string":search,"searchby":filter]
        
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        Alamofire.request(urlSearch, method: .get, parameters: parametros, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            
            switch filter
            {
            case "service":
                do
                {
                    let discovery = try JSONDecoder().decode(StatusDiscovery.self, from: data)
                    completacion(discovery)
                }catch let jsonErr
                {
                    completacion(jsonErr.localizedDescription)
                }
            case "professional","location":
                do
                {
                    let professional = try JSONDecoder().decode(StatusProfesional.self, from: data)
                    completacion(professional)
                }catch let jsonErr
                {
                    completacion(jsonErr.localizedDescription)
                }
            default:
                break
            }
            
        }
    }
    
    //MARK: FOLLOW UP
    func followUp(token:String,id:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlFollow = urlBase + "followup/seguir/\(id)"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        Alamofire.request(urlFollow, method: .post, parameters: nil, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
    
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            print(respuesta.value ?? "Nda nada")
            let estado = respuesta.value as? [String:Any]
            let exito = estado?["status"] as? String ?? "¿?"
            completacion(exito)
        }
    }
    
    //MARK: FOLLOW DOWN
    func followDown(token:String,id:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlFollow = urlBase + "followup/dejarseguir/\(id)"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        Alamofire.request(urlFollow, method: .put, parameters: nil, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print(" FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            let estado = respuesta.value as? [String:Any]
            let exito = estado?["status"] as? String ?? "¿?"
            completacion(exito)
        }
    }
    
    func listNumberProducts(token:String,idCategoria:Int,nombre:String,numero:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlFollow = urlBase + "productos/marcas/categorias/\(idCategoria)/producto/buscarnumero"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        let parametros : Parameters = ["nombreProducto":nombre,"numero":numero]
        
        Alamofire.request(urlFollow, method: .post, parameters: parametros, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print(" FRACASO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion(respuesta.value ?? "Ni valor tiene 😱")
                return
            }
            
            print()
            
            do
            {
                let numero = try JSONDecoder().decode([NumeroProducto].self, from: data)
                completacion(numero)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
    }
    
    //MARK: INBOX
    
    func getChatsActives(token:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlChats = urlBase + "chats/activos"
        
        guard let url = URL(string: urlChats) else {return}
        
        var chatsQuest = URLRequest(url: url)
        chatsQuest.httpMethod = HTTPMethod.get.rawValue
        chatsQuest.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
        
        request(chatsQuest).responseData { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO: Get Active Chats")
                completacion("")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion("")
                return
            }
            
            do
            {
                let chats = try JSONDecoder().decode([Chat].self, from: data)
                completacion(chats)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
        
    }
    
    //MARK: SEARCH USER
    func searchUser(token:String,user:String,chat:Int,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlSearch = urlBase + "clientes/busqueda"
        let header : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        let parameters : Parameters = ["string":user,"chat":chat]
        
        request(urlSearch, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                completacion(nil)
                return
            }
            
            guard let data = respuesta.data else {return}
            
            do
            {
                let messages = try JSONDecoder().decode([Client].self, from: data)
                completacion(messages)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
    }
    
    //MARK: CHAT
    func getChatOfUser(token:String,id:String,completacion : @escaping(_ status:Any) -> Void)
    {
        let urlChats = urlBase + "chats/usuario/\(id)"
        
        guard let url = URL(string: urlChats) else {return}
        
        var chatsQuest = URLRequest(url: url)
        chatsQuest.httpMethod = HTTPMethod.get.rawValue
        chatsQuest.setValue("Bearer \(token)", forHTTPHeaderField:"Authorization")
        
        request(chatsQuest).responseData { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO:")
                completacion("")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion("")
                return
            }
            
            do
            {
                let messages = try JSONDecoder().decode([Message].self, from: data)
                completacion(messages)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
            
        }
    }
    
    //MARK: SEND MESSAGE CHAT
    func sendMessageChat(token:String,id:String,tipo:String,mensaje:String,identificador:String,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlMessage = urlBase + "chats/usuario/\(id)"
        let header : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        let parameters : Parameters = ["mensaje":mensaje,"tipo_mensaje":tipo,"identificador":identificador]
        
        request(urlMessage, method: .post, parameters: parameters, encoding: URLEncoding.default, headers:header).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                completacion(respuesta.value ?? "nada")
                return
            }
            
            let body = respuesta.value as? [String:Any]
            completacion(body)
        }
    }
    
    //MARK: UPDATED CHAT WITH USER
    func updatedChatWithUser(token:String,id:String,lastDate:String,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlUpdatedChat = urlBase + "chats/usuario/ultimomensaje/\(id)"
        let header : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        let parameters : Parameters = ["fecha_ultimo_mensaje":lastDate]
        
        request(urlUpdatedChat, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: header).responseData { (respuesta) in
            
            guard respuesta.result.isSuccess else
            {
                print("FRACASO")
                completacion("nada")
                return
            }
            
            guard let data = respuesta.data else {
                print("EXITOSO PERO:")
                completacion("nada")
                return
            }
            
            do
            {
                let newMessages = try JSONDecoder().decode([Message].self, from: data)
                completacion(newMessages)
            }catch let jsonErr
            {
                completacion(jsonErr.localizedDescription)
            }
        }
    }
    
    //MARK: SEND INVITATION
    func sendInvitation(token:String,email:String,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlEmail = urlBase + "invitar/cliente"
        let cabecera : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        let parameters : Parameters = ["email":email]
            
        request(urlEmail, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: cabecera).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                completacion(nil)
                return
            }
            
            let body = respuesta.value as? [String:Any] ?? [:]
            print(body)
            completacion(body)
        }
    }
    
    //MARK: REMINDERS DEL INBOX
    func reminderInbox(token:String,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlReminder = urlBase + "chats/obtener/reminders"
        let header : HTTPHeaders = ["Authorization":"Bearer \(token)"]
        
        request(urlReminder, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                completacion("Error")
                return
            }
            
            guard let reminderData = respuesta.data else {
                completacion(respuesta.value as? [String:Any] ?? [:])
                return
            }
    
            do
            {
                let reminders = try JSONDecoder.init().decode([Reminder].self, from: reminderData)
                completacion(reminders)
            }catch
            {
                print("Error Json")
                completacion("Error")
            }
        }
    }
    
    //MARK: LOG OUT
    func logOut(token:String,completacion : @escaping(_ status:Any?) -> Void)
    {
        let urlLogOut = urlBase + "logout"
        let headers : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Bearer \(token)"]
        
        request(urlLogOut, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                print("Fracaso")
                completacion("Error")
                return
            }
            
            guard let value = respuesta.value as? [String:Any] else {
                print("No hay valor")
                completacion("Error")
                return
            }
            
            completacion(value)
        }
    }
    
    //MARK: OBTENER VERSION DE LA APP
    
    func versionDeInbi(completacion: @escaping(_ estado:Any?) -> Void)
    {
        let urlVersion = urlBase + "version/control/ios"
        
        request(urlVersion).responseJSON { (respuesta) in
            
            guard respuesta.result.isSuccess else {
                completacion(-1)
                return
            }
            
            //print(respuesta.value)
            let body = respuesta.value as? [String:Any]
            let data = body?["data"] as? [String:Any]
            let version = data?["numero"] as? String
            completacion(version)
        }
    }
}
