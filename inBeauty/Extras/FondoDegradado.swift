//
//  FondoDegradado.swift
//  InBeauty
//
//  Created by ADMIN on 15/04/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

import UIKit

class FondoDegradado: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //setGradientBackground(colorOne: #colorLiteral(red: 0.4887146354, green: 0.5628440976, blue: 0.6232366562, alpha: 1), colorTwo: #colorLiteral(red: 0.2466025949, green: 0.2463320792, blue: 0.3336096406, alpha: 1), y1: 0.025, y2:1, indice: 0)
    }
}

extension UIView {
    
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor,colorTree:UIColor,y1:CGFloat,y2:CGFloat,indice:Int,opacity:Float) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor,colorTwo.cgColor,colorTree.cgColor]
        //gradientLayer.locations = [1.0, 0.0]
        //para el fondo de pantalla : [y1:0.05,y2:1]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: y1)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: y2)
    
        //gradientLayer.locations = [0.0,0.5,1.0]
        
        layer.insertSublayer(gradientLayer, at: UInt32(indice))
        layer.sublayers?.first?.bounds.size = CGSize(width: bounds.width * 1.5, height: bounds.height * 1)
        layer.sublayers?.first?.layoutIfNeeded()
        layer.sublayers?.first?.opacity = opacity
    }
}
