//
//  GAIHeader.h
//  inBeauty
//
//  Created by ADMIN on 15/08/19.
//  Copyright © 2019 Area51 Training Center. All rights reserved.
//

#ifndef GAIHeader_h
#define GAIHeader_h
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>
#endif /* GAIHeader_h */
